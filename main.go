package main

import (
	_ "gitlab.com/catharinejm/gojure/.gojure_imports"
	"gitlab.com/catharinejm/gojure/lang"
)

func main() {
	lang.REPL()
}
