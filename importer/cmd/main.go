package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/catharinejm/gojure/importer"
	"gitlab.com/catharinejm/gojure/lang"
)

func noerror(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

const (
	importDir    = ".gojure_imports"
	importConfig = ".gojure_imports.clj"
)

func main() {
	noerror(os.RemoveAll(importDir))
	noerror(os.MkdirAll(importDir, 0777))
	fmt.Fprintln(os.Stderr, "=== Generating imports...")
	config, err := os.Open(importConfig)
	noerror(err)
	imports := lang.NewLispReader(nil, config, importConfig).Read()
	for iseq := lang.RT.Seq(imports); iseq != nil; iseq = iseq.Next() {
		importpath := iseq.First().(string)

		filename := filepath.Join(importDir, lang.Munge(importpath)+".go")
		fmt.Fprintf(os.Stderr, "--- %s => %s...", importpath, filename)
		file, err := os.Create(filename)
		noerror(err)
		noerror(importer.GenerateImports(importpath, file))
		file.Sync()
		file.Close()
		fmt.Fprintln(os.Stderr, "Done.")
	}
	fmt.Fprintln(os.Stderr, "=== Done.")
}
