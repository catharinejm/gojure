package lang

import "fmt"

type Reduced struct {
	value interface{}
}

func NewReduced(val interface{}) *Reduced {
	return &Reduced{val}
}

func (r *Reduced) Deref(vm *VM) interface{} {
	return r.value
}

func (r *Reduced) String() string {
	return fmt.Sprintf("#Reduced[%v]", r.value)
}
