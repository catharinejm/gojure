package lang

type PersistentSetBuilderType struct{}

var PersistentSetBuilder *PersistentSetBuilderType = &PersistentSetBuilderType{}

func (_ *PersistentSetBuilderType) FromSeq(elems Seq) PersistentSet {
	s := EMPTY_SET.AsTransientS() // TODO: use transient
	for ; elems != nil; elems = elems.Next() {
		s.ConjS(elems.First())
	}
	return s.PersistentS()
}

func (_ *PersistentSetBuilderType) PrintNative() string {
	return "lang.PersistentSetBuilder"
}
