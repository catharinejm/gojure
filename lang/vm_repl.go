package lang

import (
	"bufio"
	"fmt"
	"io"
	"math/big"
	"os"
	"runtime"
	"sort"
	"strings"
	"time"
)

func REPL() {
	InitEnv(false)
	reportTimerOverhead()
	vm := PushDefaultBindings(nil)
	rdr := NewLispReader(vm, os.Stdin, "<stdin>")
	for vm.readEvalPrint(rdr) {
	}
}

var kDEBUG *Keyword = ParseKeyword("debug")

func (vm *VM) readEvalPrint(rdr *LispReader) (cont bool) {
	cont = true
	defer func() {
		if e := recover(); e != nil {
			vm.printError(e)
			if _, ok := e.(*readerError); ok {
				rdr.consumeLine()
			}
		}
	}()
	fmt.Printf("%v=> ", vm.CurrentNS().name)
	form := rdr.Read()
	debug := false
	if form == kDEBUG {
		debug = true
		fmt.Println("-- DEBUG --")
		form = rdr.Read()
	}
	if form == io.EOF {
		fmt.Println("Goodbye...")
		return false
	}
	start := time.Now()
	fn := Compile(vm, form)
	compileTime := time.Since(start)
	start = time.Now()
	var result interface{}
	if debug {
		code := fn.(*Fn).bodies[0]
		evaler := newEvaler(vm)
		evaler.code = code
		evaler.params.push(fn)
		result, cont = vm.doDebug(evaler)
		if !cont {
			return false
		}
	} else {
		result = fn.Invoke(vm, nil)
	}
	evalTime := time.Since(start)
	fmt.Println(RT.Print(result))
	fmt.Printf("\ttime: %s (%s compile, %s eval)\n", compileTime+evalTime, compileTime, evalTime)
	return
}

func (vm *VM) doDebug(evaler *Evaler) (interface{}, bool) {
	stdin := bufio.NewReader(os.Stdin)

	printcode := func() {
		stix := evaler.pc - 5
		if stix < 0 {
			stix = 0
		}
		endix := evaler.pc + 5
		if endix > len(evaler.code) {
			endix = len(evaler.code)
		}
		for i := stix; i < endix; i++ {
			if i == evaler.pc {
				fmt.Printf("\t*% 3d: %v\n", i, evaler.code[i])
			} else {
				fmt.Printf("\t % 3d: %v\n", i, evaler.code[i])
			}
		}
	}

	for {
		printcode()
	inputloop:
		for {
			fmt.Print("-DEBUG-> ")
			input, err := stdin.ReadString('\n')
			if err == io.EOF {
				fmt.Println("Goodbye...")
				return nil, false
			}
			if err != nil {
				panic(err)
			}
			switch strings.TrimSpace(input) {
			case "", "s", "step", "n", "next":
				break inputloop
			case "locals", "params", "l", "p":
				fmt.Println("Locals:")
				for i, local := range evaler.params.view() {
					fmt.Printf("\t% 3d: %v\n", i, local)
				}
			case "returns", "r":
				fmt.Println("Returns:")
				for i, ret := range evaler.returns.view() {
					fmt.Printf("\t% 3d: %v\n", i, ret)
				}
			case "c", "code":
				printcode()
			default:
				fmt.Println("unrecognized command")
			}
		}
		if evaler.pc < len(evaler.code) {
			if inv, isinv := evaler.code[evaler.pc].(*bc_Invoke); isinv {
				params := evaler.returns.peekN(inv.count)
				if fn, isfn := params[0].(*Fn); isfn {
					subev := evaler.makeSubEvaler(fn, params[1:])
					result, cont := vm.doDebug(subev)
					if !cont {
						return nil, false
					}
					evaler.pc++
					// params and returns are swapped in sub-fn
					evaler.params.popFrame(subev.returns)
					evaler.returns.popFrame(subev.params)
					evaler.returns.push(result)
				}
			}
		}
		if !evaler.step() {
			break
		}
	}
	// fmt.Println("returns:")
	// for i, ret := range evaler.returns {
	// 	fmt.Printf("\t% 3d: %v\n", i, ret)
	// }
	return evaler.returns.pop(), true
}

func (vm *VM) printError(e interface{}) {
	const maxBacktrace = 25

	errIO := vERR.Deref(vm).(io.StringWriter)

	skip := 4

	if inverr, ok := e.(*invokeError); ok {
		errIO.WriteString(inverr.err.Error())
		errIO.WriteString("\n\n")
		pcs := make([]uintptr, maxBacktrace*2)
		n := runtime.Callers(skip, pcs)
		if n == 0 {
			return
		}
		frames := runtime.CallersFrames(pcs[:n])

		cljbtrace := inverr.backtrace

		const (
			gojure        = "gitlab.com/catharinejm/gojure/"
			gojureLang    = gojure + "lang."
			gojureImports = gojure + "%2egojure_imports.init."
		)
		more := true
		var frame runtime.Frame
		for written := 0; written < maxBacktrace && more; {
			frame, more = frames.Next()
			writeClj := func() {
				if cljbtrace[0].file != "" {
					errIO.WriteString(cljbtrace[0].FnName())
					errIO.WriteString("\n\t")
					errIO.WriteString(cljbtrace[0].String())
					errIO.WriteString("\n")
					written++
				}
				cljbtrace = cljbtrace[1:]
			}
			writeDefault := func() {
				errIO.WriteString(fmt.Sprintln(frame.Function))
				errIO.WriteString(fmt.Sprintf("\t%s:%d\n", frame.File, frame.Line))
				written++
			}
			switch {
			case strings.HasPrefix(frame.Function, "runtime."):
				//noop
			case strings.HasPrefix(frame.Function, gojureLang):
				tail := frame.Function[len(gojureLang):]
				switch {
				case strings.HasPrefix(tail, "(*Evaler)"):
					if strings.HasSuffix(tail, ".run") {
						writeClj()
					}
					// writeDefault()
				default:
					writeDefault()
				}
			default:
				writeDefault()
			}
		}
		return
	}

	errIO.WriteString(fmt.Sprintf("%v\n", e))

	pc := make([]uintptr, 25)
	// fmt.Println("skipping", skip, "frames...")
	n := runtime.Callers(skip, pc)
	if n == 0 {
		return
	}
	errIO.WriteString("\n")
	frames := runtime.CallersFrames(pc[:n])
	for {
		frame, more := frames.Next()
		if strings.HasPrefix(frame.Function, "runtime.") {
			if more {
				continue
			}
			break
		}
		errIO.WriteString(fmt.Sprintln(frame.Function))
		errIO.WriteString(fmt.Sprintf("\t%s:%d\n", frame.File, frame.Line))
		if !more {
			break
		}
	}
}

func reportTimerOverhead() {
	times := make([]int, 1000)
	var sum big.Int
	for i := range times {
		t := time.Now()
		d := time.Since(t)
		times[i] = int(d)
		bigd := big.NewInt(int64(d))
		sum.Add(&sum, bigd)
	}
	sort.IntSlice(times).Sort()
	min := time.Duration(times[0])
	max := time.Duration(times[len(times)-1])
	median := time.Duration(times[len(times)/2])
	counts := make(map[int]int)
	for _, n := range times {
		counts[n] = counts[n] + 1
	}
	mode := int(min)
	modeCount := counts[mode]
	for n, c := range counts {
		if c > modeCount {
			mode = n
			modeCount = c
		}
	}
	var mean big.Float
	mean.SetInt(&sum)
	mean.Quo(&mean, big.NewFloat(float64(len(times))))
	fmt.Printf("Timing overhead:\n\tmin: %v\n\tmax: %v\n\tmedian: %v\n\tmode: %v (%v times, %.1f%%)\n\tmean: %sns\n\n",
		min, max, median, time.Duration(mode), modeCount, float64(modeCount)/float64(len(times))*100, mean.String())
}
