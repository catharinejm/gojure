package lang

import (
	"fmt"
	"strings"
)

type PersistentSet interface {
	PersistentCollection
	FastCount() int
	ConjS(key interface{}) PersistentSet
	Disjoin(key interface{}) PersistentSet
	Contains(key interface{}) bool
	Get(key interface{}) interface{}
	AsTransientS() TransientSet
}

type persistentSet struct {
	m PersistentMap
	metadata
	unorderedHashEqCache
}

var (
	EMPTY_SET = &persistentSet{m: EMPTY_MAP}
)

func NewPersistentSet(keys ...interface{}) *persistentSet {
	if len(keys) == 0 {
		return EMPTY_SET
	}
	for i, k0 := range keys {
		for _, k1 := range keys[i+1:] {
			if RT.Equiv(k0, k1) {
				panic(fmt.Errorf("NewPersistentSet called with duplicate key: %v", k0))
			}
		}
	}
	dupkeys := make([]interface{}, len(keys))
	copy(dupkeys, keys)
	return &persistentSet{m: &persistentSliceMap{keys: dupkeys, values: dupkeys}}
}

func (ps *persistentSet) ConjS(x interface{}) PersistentSet {
	if ps.Contains(x) {
		return ps
	}
	return &persistentSet{m: ps.m.AssocM(x, x), metadata: ps.metadata}
}

func (ps *persistentSet) Conj(x interface{}) PersistentCollection {
	return ps.ConjS(x)
}

func (ps *persistentSet) Count() int {
	return ps.m.Count()
}

func (ps *persistentSet) FastCount() int {
	return ps.m.FastCount()
}

func (ps *persistentSet) Contains(x interface{}) bool {
	return ps.m.ContainsKey(x)
}

func (ps *persistentSet) Get(x interface{}) interface{} {
	return ps.m.ValAt(x, nil)
}

func (ps *persistentSet) Disjoin(x interface{}) PersistentSet {
	if ps.Contains(x) {
		return &persistentSet{m: ps.m.Dissoc(x), metadata: ps.metadata}
	}
	return ps
}

func (ps *persistentSet) Seq() Seq {
	if ps.Count() == 0 {
		return nil
	}
	return ps.m.Keys()
}

func (ps *persistentSet) WithMeta(newMeta PersistentMap) IObj {
	if ps.meta == newMeta {
		return ps
	}
	newPS := *ps
	newPS.metadata = metadata{newMeta}
	return &newPS
}

func (ps *persistentSet) Equiv(other interface{}) bool {
	switch other := other.(type) {
	case PersistentSet:
		if ps.Count() != other.Count() {
			return false
		}
		for seq := ps.Seq(); seq != nil; seq = seq.Next() {
			if !other.Contains(seq.First()) {
				return false
			}
		}
		return true
	default:
		return false
	}
}

func (ps *persistentSet) String() string {
	var sb strings.Builder
	sb.WriteString("#{")
	seq := ps.m.Keys()
	for ; seq != nil; seq = seq.Next() {
		sb.WriteString(RT.Print(seq.First()))
		if seq.Next() != nil {
			sb.WriteString(" ")
		}
	}
	sb.WriteString("}")
	return sb.String()
}

//// IFn ////

func (ps *persistentSet) Invoke(vm *VM, args Seq) interface{} {
	switch RT.Count(args) {
	case 1:
		return ps.Get(args.First())
	case 2:
		if ps.Contains(args.First()) {
			return ps.Get(args.First())
		}
		return RT.Second(args)
	default:
		panic(&arityError{RT.Count(args), "1,2"})
	}
}

func (ps *persistentSet) VM(vm *VM) *VM {
	return vm
}

func (ps *persistentSet) AsTransient() TransientCollection {
	return ps.AsTransientS()
}

func (ps *persistentSet) AsTransientS() TransientSet {
	return newTransientSet(ps)
}

func (ps *persistentSet) HashEq() uint32 {
	return ps.unorderedHashEq(ps)
}

var (
	_ PersistentSet        = &persistentSet{}
	_ PersistentCollection = &persistentSet{}
	_ FastCounter          = &persistentSet{}
	_ IMeta                = &persistentSet{}
	_ IObj                 = &persistentSet{}
	_ IFn                  = &persistentSet{}
	_ EditableCollection   = &persistentSet{}
)
