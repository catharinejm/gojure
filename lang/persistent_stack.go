package lang

type PersistentStack interface {
	Peek() interface{}
	Pop() PersistentStack
}
