package lang

import (
	"fmt"
	"os"
	"reflect"
	"sync"
	"sync/atomic"
	"unsafe"
)

var nsmap sync.Map

type Namespace struct {
	name     *Symbol
	mappings unsafe.Pointer //*PersistentMap
	imports  unsafe.Pointer //*PersistentMap

	resettableMetadata
}

type internable interface {
	BoundNS() *Namespace
}

func FindOrCreateNamespace(name *Symbol) *Namespace {
	ns := loadNs(name)
	if ns != nil {
		return ns
	}
	return loadOrStoreNs(name)
}

func FindNamespace(name *Symbol) *Namespace {
	return loadNs(name)
}

func newNamespace(name *Symbol) *Namespace {
	mappings := PersistentMap(EMPTY_MAP)
	imports := PersistentMap(NewPersistentMap(ParseSymbol("builtin"), FindGoPackage("builtin")))
	return &Namespace{
		name,
		unsafe.Pointer(&mappings),
		unsafe.Pointer(&imports),
		resettableMetadata{meta: name.Meta()},
	}
}

func loadNs(name *Symbol) *Namespace {
	if name == nil {
		return nil
	}
	nsiface, _ := nsmap.Load(name.symbolParts)
	ns, _ := nsiface.(*Namespace)
	return ns
}

func loadOrStoreNs(name *Symbol) *Namespace {
	if name == nil {
		return nil
	}
	ns, _ := nsmap.LoadOrStore(name.symbolParts, newNamespace(name))
	return ns.(*Namespace)
}

func AllNamespaces() Seq {
	v := EMPTY_VECTOR.AsTransientV()
	nsmap.Range(func(sym, ns interface{}) bool {
		v.Conj(ns)
		return true
	})
	return v.Persistent().Seq()
}

func ResolveName(ns *Namespace, name *Symbol) interface{} {
	if nssym := name.NamespaceSym(); nssym != nil {
		if ns != nil {
			if gopkg := ns.FindImport(nssym); gopkg != nil {
				if imp := gopkg.Lookup(name.NameSym()); imp != nil {
					return imp
				}
				return nil
			}
		}
		ns = FindNamespace(nssym)
	}
	if ns == nil {
		return nil
	}
	return ns.Resolve(name.NameSym())
}

func (ns *Namespace) Resolve(name *Symbol) interface{} {
	return ns.GetMappings().ValAt(name, nil)
}

func (ns *Namespace) FindInternedVar(name *Symbol) *Var {
	v, _ := ns.Resolve(name).(*Var)
	return v
}

func (ns *Namespace) FindImport(name *Symbol) *GoPackage {
	return ns.GetImports().ValAt(name, (*GoPackage)(nil)).(*GoPackage)
}

func (ns *Namespace) Intern(sym *Symbol) *Var {
	return ns.internObject(sym, nil).(*Var)
}

func (ns *Namespace) Refer(sym *Symbol, v *Var) *Var {
	return ns.internObject(sym, v).(*Var)
}

func (ns *Namespace) Import(sym *Symbol, gopkg *GoPackage) *GoPackage {
	if sym == nil {
		sym = gopkg.defaultName
	}
	if sym.Namespace() != "" {
		panic(fmt.Errorf("import can't be namespace-qualified symbol: %v", sym))
	}
	sym = sym.NameSym() // clear meta and gensym tag
	importsPtr := (*PersistentMap)(atomic.LoadPointer(&ns.imports))
	pkg := (*importsPtr).ValAt(sym, (*GoPackage)(nil)).(*GoPackage)
	for pkg == nil {
		newImports := (*importsPtr).AssocM(sym, gopkg)
		atomic.CompareAndSwapPointer(&ns.imports, unsafe.Pointer(importsPtr), unsafe.Pointer(&newImports))
		importsPtr = (*PersistentMap)(atomic.LoadPointer(&ns.imports))
		pkg = (*importsPtr).ValAt(sym, (*GoPackage)(nil)).(*GoPackage)
	}
	if pkg == gopkg {
		return pkg
	}
	panic(fmt.Errorf("%v already refers to package at path %q", sym, pkg.path))
}

func (ns *Namespace) internObject(sym *Symbol, x internable) internable {
	if sym.Namespace() != "" {
		panic(fmt.Errorf("can't intern namespace-qualified symbol: %v", sym))
	}
	sym = sym.NameSym() // clear meta and gensym tag
	mappingsPtr := ns.loadMappingsPointer()
	o, _ := (*mappingsPtr).ValAt(sym, nil).(internable)
	for o == nil {
		if x == nil {
			x = newVar(ns, sym)
		}
		newMap := (*mappingsPtr).AssocM(sym, x)
		ns.compareAndSwapMappings(mappingsPtr, &newMap)
		mappingsPtr = ns.loadMappingsPointer()
		o, _ = (*mappingsPtr).ValAt(sym, nil).(internable)
	}
	if x == nil || sameIntern(x, o) {
		return o
	}
	ns.replaceWarnOrFail(sym, x, o)
	for {
		newMappings := (*mappingsPtr).AssocM(sym, x)
		if ns.compareAndSwapMappings(mappingsPtr, &newMappings) {
			break
		}
		mappingsPtr = ns.loadMappingsPointer()
	}
	return x
}

func sameIntern(x, y internable) bool {
	return reflect.TypeOf(x) == reflect.TypeOf(y) && x.BoundNS() == y.BoundNS()
}

func (ns *Namespace) replaceWarnOrFail(sym *Symbol, x, o internable) {
	if x == nil || o == nil {
		return
	}
	if reflect.TypeOf(x) != reflect.TypeOf(o) {
		panic(fmt.Errorf("%v already refers to: %v in namespace: %v", sym, o, ns.name))
	}
	if x.BoundNS() == o.BoundNS() || o.BoundNS() == coreNS {
		return
	}
	fmt.Fprintf(os.Stderr,
		"WARNING: %v already refers to: %v in namespace: %v, being replaced by: %v",
		sym, o, ns.name, x,
	)
}

func (ns *Namespace) GetMappings() PersistentMap {
	return *ns.loadMappingsPointer()
}

func (ns *Namespace) GetImports() PersistentMap {
	return *(*PersistentMap)(atomic.LoadPointer(&ns.imports))
}

func (ns *Namespace) loadMappingsPointer() *PersistentMap {
	return (*PersistentMap)(atomic.LoadPointer(&ns.mappings))
}

func (ns *Namespace) compareAndSwapMappings(old, new *PersistentMap) bool {
	return atomic.CompareAndSwapPointer(&ns.mappings, unsafe.Pointer(old), unsafe.Pointer(new))
}

func (ns *Namespace) Name() *Symbol {
	return ns.name
}

func (ns *Namespace) String() string {
	return fmt.Sprintf("#namespace[%v]", ns.name)
}
