package lang

type IDeref interface {
	Deref(vm *VM) interface{}
}

type IBlockingDeref interface {
	DerefTimeout(vm *VM, ms int, timeoutValue interface{}) interface{}
}

type IRef interface {
	IDeref
}
