package lang_test

import (
	"reflect"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	_ "gitlab.com/catharinejm/gojure/.gojure_imports"
	. "gitlab.com/catharinejm/gojure/lang"
)

func TestBooks(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Compile Suite")
}

func code(inxns ...interface{}) []interface{} {
	result := []interface{}{}
	for _, inxn := range inxns {
		switch inxn := inxn.(type) {
		case []interface{}:
			result = append(result, code(inxn...)...)
		default:
			result = append(result, inxn)
		}
	}
	return result
}

func mapCode(elems ...interface{}) []interface{} {
	result := []interface{}{}
	for _, el := range elems {
		result = append(result, el)
	}
	return append(result, TH.BC_Map(len(elems)/2, false))
}

var _ = BeforeSuite(func() {
	InitEnv(false)
})

var _ = Describe("Compile", func() {
	var (
		vm          *VM
		testNS      *Namespace
		vCURRENT_NS *Var
	)

	BeforeEach(func() {
		testNS = FindOrCreateNamespace(ParseSymbol("compile-test-ns"))
		testNS.Import(ParseSymbol("clojure.lang"), FindGoPackage("gitlab.com/catharinejm/gojure/lang"))
		vCURRENT_NS = FindVarOrFail(nil, ParseSymbol("clojure.core/*ns*"))
		vm = PushDefaultBindings(nil).PushThreadBindings(NewPersistentMap(
			vCURRENT_NS, testNS,
		))
	})

	compile := func(input string) []interface{} {
		return TH.Compile(vm, input)
	}

	Describe("Compiling constants", func() {
		It("should return a function that has a single constant instruction", func() {
			Expect(compile("10")).To(Equal(code(
				TH.Quoted(Int(10)),
			)))
			Expect(compile(":foo")).To(Equal(code(
				TH.Quoted(ParseKeyword("foo")),
			)))
			Expect(compile(`"hello, world"`)).To(Equal(code(
				TH.Quoted("hello, world"),
			)))
		})
	})

	Describe("Compiling collections", func() {
		Context("without metadata", func() {
			It("includes instructions to build each element", func() {
				Expect(compile("[1 2 3]")).To(Equal(code(
					TH.Quoted(Int(1)),
					TH.Quoted(Int(2)),
					TH.Quoted(Int(3)),
					TH.BC_Vector(3, false),
				)))
				Expect(compile(`{:foo 10, "hello" 42}`)).To(Equal(code(
					TH.Quoted(ParseKeyword("foo")),
					TH.Quoted(Int(10)),
					TH.Quoted("hello"),
					TH.Quoted(Int(42)),
					TH.BC_Map(2, false),
				)))
				Expect(compile("#{1 2 3}")).To(Equal(code(
					TH.Quoted(Int(1)),
					TH.Quoted(Int(2)),
					TH.Quoted(Int(3)),
					TH.BC_Set(3, false),
				)))
			})
		})
		Context("with metadata", func() {
			It("includes instructions to build the metadata map before the elements", func() {
				Expect(compile("^:potato [1 2 3]")).To(Equal(code(
					mapCode(TH.Quoted(ParseKeyword("potato")), TH.Quoted(true)),
					TH.Quoted(Int(1)),
					TH.Quoted(Int(2)),
					TH.Quoted(Int(3)),
					TH.BC_Vector(3, true),
				)))
				Expect(compile(`^{:what "a riot"} {:foo 10, "hello" 42}`)).To(Equal(code(
					mapCode(TH.Quoted(ParseKeyword("what")), TH.Quoted("a riot")),
					TH.Quoted(ParseKeyword("foo")),
					TH.Quoted(Int(10)),
					TH.Quoted("hello"),
					TH.Quoted(Int(42)),
					TH.BC_Map(2, true),
				)))
				Expect(compile("^builtin/string #{1 2 3}")).To(Equal(code(
					mapCode(TH.Quoted(ParseKeyword("tag")), TH.Quoted(TypeOf(""))),
					TH.Quoted(Int(1)),
					TH.Quoted(Int(2)),
					TH.Quoted(Int(3)),
					TH.BC_Set(3, true),
				)))
			})
		})
	})

	Describe("compiling invokes", func() {
		var idCall []interface{}
		BeforeEach(func() {
			compile("(def id (fn* id [x] x))")
			idCall = compile("(id 10)")
		})
		It("pushes the fn, then args, then the invoke", func() {
			Expect(idCall).To(Equal(code(
				TH.BC_DerefVar(FindVarOrFail(vm.CurrentNS(), ParseSymbol("id"))),
				TH.Quoted(Int(10)),
				TH.BC_Invoke(2, "<NO FILE>", 1, 1),
			)))
		})
	})

	Describe("compiling let*", func() {
		Context("in expression position", func() {
			Context("with no body", func() {
				var letCode []interface{}
				BeforeEach(func() {
					letCode = compile("(let* [x 10, y 20])")
				})
				It("pushes a local for each bind, and a nil result", func() {
					Expect(letCode).To(Equal(code(
						TH.Quoted(Int(10)),
						TH.BC_PushLocal("x"),
						TH.Quoted(Int(20)),
						TH.BC_PushLocal("y"),
						TH.Quoted(nil),
						TH.BC_PopLocals(2),
					)))
				})
			})
			Context("with a body", func() {
				var letCode []interface{}
				BeforeEach(func() {
					letCode = compile("(let* [x 10, y 20] x y)")
				})
				It("pushes a local for each bind, and pops all but the last statement", func() {
					Expect(letCode).To(Equal(code(
						TH.Quoted(Int(10)),
						TH.BC_PushLocal("x"),
						TH.Quoted(Int(20)),
						TH.BC_PushLocal("y"),
						TH.BC_Param(1, "x"),
						TH.PopReturn(),
						TH.BC_Param(2, "y"),
						TH.BC_PopLocals(2),
					)))
				})
			})
		})
		Context("in statement position", func() {
			Context("with a body", func() {
				var letCode []interface{}
				BeforeEach(func() {
					letCode = compile("(do (let* [x 10] x) 123)")
				})
				It("pops resulting expression", func() {
					Expect(letCode).To(Equal(code(
						TH.Quoted(Int(10)),
						TH.BC_PushLocal("x"),
						TH.BC_Param(1, "x"),
						TH.PopReturn(),
						TH.BC_PopLocals(1),
						TH.Quoted(Int(123)),
					)))
				})
			})
			Context("without a body", func() {
				var letCode []interface{}
				BeforeEach(func() {
					letCode = compile("(do (let* [x 10]) 123)")
				})
				It("pops the expression", func() {
					Expect(letCode).To(Equal(code(
						TH.Quoted(Int(10)),
						TH.BC_PushLocal("x"),
						TH.BC_PopLocals(1),
						TH.Quoted(Int(123)),
					)))
				})
			})
		})
	})

	Describe("compiling a host expression", func() {
		Context("with a cached type", func() {
			var (
				cachedRT *CachedType
				hostCode []interface{}
				consFn   *DirectFn
			)
			BeforeEach(func() {
				cachedRT = LookupType(reflect.TypeOf(RT))
				if cachedRT == nil {
					panic("clojure.lang/RT is not cached")
				}
				consFn, _ = RT.ValAt(cachedRT.Methods(), ParseKeyword("Cons"), nil).(*DirectFn)
				if consFn == nil {
					panic("(RT).Cons is not cached")
				}
				hostCode = compile("(. clojure.lang/RT Cons 1 nil)")
			})
			It("compiles to a DirectFn invoke", func() {
				Expect(hostCode).To(Equal(code(
					TH.Quoted(consFn),
					TH.Quoted(RT),
					TH.Quoted(Int(1)),
					TH.Quoted(nil),
					TH.BC_Invoke(4, "<NO FILE>", 1, 1),
				)))
			})
		})
	})
})
