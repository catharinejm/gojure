package lang

import (
	"fmt"
	"strings"
)

type symbolParts struct {
	ns   string
	name string
}

type Symbol struct {
	symbolParts
	gensym int64
	metadata
}

func NewSymbol(ns, name string) *Symbol {
	return &Symbol{symbolParts{ns, name}, 0, metadata{}}
}

func ParseSymbol(nsname string) *Symbol {
	var ns, name string
	if nsname[0] == '/' {
		name = nsname
	} else {
		strs := strings.SplitN(nsname, "/", 2)
		if len(strs) == 2 && len(strs[1]) > 0 {
			ns = strs[0]
			name = strs[1]
		} else {
			name = nsname
		}
	}
	return NewSymbol(ns, name)
}

func NewGensym(name string) *Symbol {
	id := RT.NextID()
	name = fmt.Sprintf("%s__%d__gensym__", name, id)
	return &Symbol{symbolParts{"", name}, id, metadata{}}
}

func simpleGensym(name string) *Symbol {
	id := RT.NextID()
	name = fmt.Sprint(name, id)
	return &Symbol{symbolParts{"", name}, id, metadata{}}
}

func (s *Symbol) Equiv(other interface{}) bool {
	if s1, ok := other.(*Symbol); ok {
		return s == s1 || s.symbolParts == s1.symbolParts && s.gensym == s1.gensym
	}
	return false
}

func (s *Symbol) Namespace() string {
	return s.ns
}

func (s *Symbol) NamespaceSym() *Symbol {
	if s.ns == "" {
		return nil
	}
	return &Symbol{symbolParts{"", s.ns}, 0, metadata{}}
}

func (s *Symbol) Name() string {
	return s.name
}

func (s *Symbol) NameSym() *Symbol {
	if s.ns == "" && s.gensym == 0 && s.meta == nil {
		return s
	}
	return &Symbol{symbolParts{"", s.name}, 0, metadata{}}
}

func (s *Symbol) WithMeta(newMeta PersistentMap) IObj {
	if s.meta == newMeta {
		return s
	}
	return &Symbol{s.symbolParts, s.gensym, metadata{newMeta}}
}

func (s *Symbol) IsGensym() bool {
	return s.gensym != 0
}

func (s *Symbol) CompareTo(other interface{}) int {
	o := other.(*Symbol)
	switch {
	case s.Equiv(o):
		return 0
	case s.ns == "" && o.ns != "":
		return -1
	case s.ns != "" && o.ns == "":
		return 1
	default:
		nsc := stringComparable(s.ns).CompareTo(o.ns)
		if nsc != 0 {
			return nsc
		}
		if nmc := stringComparable(s.name).CompareTo(o.name); nmc != 0 {
			return nmc
		}
		return icomp(int(s.gensym), int(o.gensym))
	}
}

func (s *Symbol) String() string {
	if s.ns == "" {
		return s.name
	}
	return fmt.Sprintf("%s/%s", s.ns, s.name)
}

var (
	_ Comparable = &Symbol{}
)
