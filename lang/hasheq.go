package lang

type HashEq interface {
	HashEq() uint32
}

type orderedHashEqer interface {
	orderedHashEq(orderedHashEqer) uint32
	Seq() Seq
}

type orderedHashEqCache uint32

func (h *orderedHashEqCache) orderedHashEq(elems orderedHashEqer) uint32 {
	if *h == 0 {
		*h = orderedHashEqCache(HashOrdered(elems.Seq()))
	}
	return uint32(*h)
}

type unorderedHashEqer interface {
	unorderedHashEq(unorderedHashEqer) uint32
	Seq() Seq
}

type unorderedHashEqCache uint32

func (h *unorderedHashEqCache) unorderedHashEq(elems unorderedHashEqer) uint32 {
	if *h == 0 {
		*h = unorderedHashEqCache(HashUnordered(elems.Seq()))
	}
	return uint32(*h)
}
