package lang

import "fmt"

type PersistentMapBuilderType struct{}

var PersistentMapBuilder *PersistentMapBuilderType = &PersistentMapBuilderType{}

func (_ *PersistentMapBuilderType) FromSeq(elems Seq) PersistentMap {
	m := EMPTY_MAP.AsTransientM()
	for elems = RT.Seq(elems); elems != nil; elems = elems.Next().Next() {
		if elems.Next() == nil {
			panic(fmt.Errorf("No value supplied for key: %v", elems.First()))
		}
		m.AssocM(elems.First(), RT.Second(elems))
	}
	return m.PersistentM()
}

func (_ *PersistentMapBuilderType) PrintNative() string {
	return "lang.PersistentMapBuilder"
}
