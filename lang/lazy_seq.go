package lang

import "sync"

type lazySeq struct {
	mu sync.Mutex
	fn IFn
	sv interface{}
	s  Seq
	metadata
}

func NewLazySeq(fn IFn) *lazySeq {
	return &lazySeq{sync.Mutex{}, fn, nil, nil, metadata{}}
}

func (ls *lazySeq) WithMeta(meta PersistentMap) IObj {
	if ls.meta == meta {
		return ls
	}
	return &lazySeq{sync.Mutex{}, ls.fn, ls.sv, ls.s, metadata{meta}}
}

func (ls *lazySeq) sval() interface{} {
	ls.mu.Lock()
	defer ls.mu.Unlock()
	return ls.svalLocked()
}

func (ls *lazySeq) svalLocked() interface{} {
	if ls.fn != nil {
		ls.sv = ls.fn.Invoke(nil, nil)
		ls.fn = nil
	}
	if ls.sv != nil {
		return ls.sv
	}
	return ls.s
}

func (ls *lazySeq) Seq() Seq {
	if ls.s == nil {
		ls.mu.Lock()
		defer ls.mu.Unlock()
		ls.svalLocked()
		if ls.sv != nil {
			lziface := ls.sv
			ls.sv = nil
			for lz, _ := lziface.(*lazySeq); lz != nil; lz, _ = lziface.(*lazySeq) {
				if lz == ls {
					lziface = lz.svalLocked()
				} else {
					lziface = lz.sval()
				}
			}
			ls.s = RT.Seq(lziface)
		}
		if ls.s == nil {
			ls.s = EMPTY_LIST
		}
	}
	return ls.s.Seq()
}

func (ls *lazySeq) Count() int {
	c := 0
	for s := ls.Seq(); s != nil; s = s.Next() {
		c++
	}
	return c
}

func (ls *lazySeq) First() interface{} {
	ls.Seq()
	if ls.s == nil {
		return nil
	}
	return ls.s.First()
}

func (ls *lazySeq) Next() Seq {
	ls.Seq()
	if ls.s == nil {
		return nil
	}
	return ls.s.Next()
}

func (ls *lazySeq) Conj(x interface{}) PersistentCollection {
	return newCons(x, ls)
}

func (ls *lazySeq) Equiv(other interface{}) bool {
	s := ls.Seq()
	if s != nil {
		return RT.Equiv(s, other)
	}
	_, isseq := other.(Seq)
	return isseq && RT.Seq(other) == nil
}

func (ls *lazySeq) String() string {
	return SeqString(ls.Seq())
}

var (
	_ Seq                  = &lazySeq{}
	_ PersistentCollection = &lazySeq{}
	_ IObj                 = &lazySeq{}
)
