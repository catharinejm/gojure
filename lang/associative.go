package lang

type Associative interface {
	PersistentCollection
	ILookup
	ContainsKey(key interface{}) bool
	EntryAt(key interface{}) MapEntry
	Assoc(key, val interface{}) Associative
}

type ILookup interface {
	ValAt(key, notfound interface{}) interface{}
}
