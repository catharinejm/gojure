package lang

type PersistentCollection interface {
	Conj(x interface{}) PersistentCollection
	Equiv(x interface{}) bool
	Seq() Seq
	Count() int
}
