package lang

import "fmt"

type Volatile struct {
	val atomicBox
}

func NewVolatile(val interface{}) *Volatile {
	v := &Volatile{}
	v.val.Store(val)
	return v
}

func (v *Volatile) Deref(vm *VM) interface{} {
	return v.val.Load()
}

func (v *Volatile) Reset(newval interface{}) interface{} {
	v.val.Store(newval)
	return newval
}

func (v *Volatile) String() string {
	return fmt.Sprintf("#Volatile[%v]", v.val.Load())
}
