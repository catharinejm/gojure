package lang

import (
	"fmt"
	"unicode/utf8"
)

type Comparable interface {
	CompareTo(other interface{}) int
}

func ComparableFor(x interface{}) Comparable {
	switch x := x.(type) {
	case Comparable:
		return x
	case string:
		return stringComparable(x)
	default:
		panic(fmt.Errorf("Can't create Comparable instance for %T", x))
	}
}

type stringComparable string

func (s stringComparable) CompareTo(other interface{}) int {
	o := other.(string)
	contentsComp, lenComp := s.compareWithLen(o)
	switch {
	case lenComp == 0:
		return contentsComp
	case lenComp < 0:
		if contentsComp <= 0 {
			return -1
		}
		return 1
	default:
		if contentsComp >= 0 {
			return 1
		}
		return -1
	}
}

func icomp(x, y int) int {
	if x < y {
		return -1
	}
	if x > y {
		return 1
	}
	return 0
}

func (sc stringComparable) compareWithLen(o string) (contentsComp, lenComp int) {
	s := string(sc)
	sz := 0
	for ; len(s) > 0 && len(o) > 0; s, o = s[sz:], o[sz:] {
		var r0 rune
		r0, sz = utf8.DecodeRuneInString(s)
		r1, _ := utf8.DecodeRuneInString(o)
		contentsComp = icomp(int(r0), int(r1))
		if contentsComp != 0 {
			break
		}
	}
	lenComp = icomp(utf8.RuneCountInString(s), utf8.RuneCountInString(o))
	return
}
