package lang

type EditableCollection interface {
	AsTransient() TransientCollection
}

type TransientCollection interface {
	Conj(val interface{}) TransientCollection
	Persistent() PersistentCollection
	Count() int
}

type TransientAssociative interface {
	TransientCollection
	ILookup
	Assoc(key, val interface{}) TransientAssociative
	ContainsKey(key interface{}) bool
	EntryAt(key interface{}) MapEntry
}

type TransientVector interface {
	TransientAssociative
	Indexer
	AssocN(i int, val interface{}) TransientVector
	AssocV(key, val interface{}) TransientVector
	ConjV(val interface{}) TransientVector
	Pop() TransientVector
	PersistentV() PersistentVector
}

type TransientMap interface {
	TransientAssociative
	FastCounter
	AssocM(key, val interface{}) TransientMap
	Dissoc(key interface{}) TransientMap
	PersistentM() PersistentMap
}

type TransientSet interface {
	TransientCollection
	FastCounter
	ConjS(key interface{}) TransientSet
	Disjoin(key interface{}) TransientSet
	Contains(key interface{}) bool
	Get(key interface{}) interface{}
	PersistentS() PersistentSet
}
