package lang

import (
	"errors"
	"fmt"
)

// TODO: Do these need to use atomics??
// They should always be run on a single goroutine, but what happens
// if a goroutine switches OS threads? Seems like that must be OK or
// it would wreak havoc on go programs in general.
func PushThreadBindings(vm *VM, bindings PersistentMap) *VM {
	if vm == nil {
		vm = FreshVM()
	}
	bmap := vm.bindings
	for bs := RT.Seq(bindings); bs != nil; bs = bs.Next() {
		entry := bs.First().(MapEntry)
		v := entry.Key().(*Var)
		if !v.IsDynamic() {
			panic(fmt.Errorf("Can't dynamically bind non-dynamic var: %v", v))
		}
		bmap = bmap.AssocM(v, newTbox(vm.tcid, entry.Value()))
	}
	return &VM{vm.tcid, bmap, vm}
}

func PopThreadBindings(vm *VM) *VM {
	if vm == nil {
		panic(errors.New("PopThreadBindings: nil vm"))
	}
	if vm.prev == nil {
		panic(errors.New("Pop without matching push"))
	}
	return vm.prev
}

func wrapThreadBindings(vm *VM, bindings PersistentMap) *VM {
	if vm == nil {
		vm = FreshVM()
	}
	return &VM{vm.tcid, bindings, nil}
}

func PushDefaultBindings(vm *VM) *VM {
	return PushThreadBindings(vm, NewPersistentMap(
		vCURRENT_NS, vCURRENT_NS.Deref(vm),
		vIN, vIN.Deref(vm),
		vOUT, vOUT.Deref(vm),
		vERR, vERR.Deref(vm),
		vUNCHECKED_MATH, vUNCHECKED_MATH.Deref(vm),
		vEVAL_DEBUG, vEVAL_DEBUG.Deref(vm),
		vCOMPILE_DEBUG, vCOMPILE_DEBUG.Deref(vm),
		vOPTIMIZE, vOPTIMIZE.Deref(vm),
		vWARN_ON_REFLECTION, vWARN_ON_REFLECTION.Deref(vm),
	))
}

func GetThreadBindings(vm *VM) PersistentMap {
	ret := PersistentMap(EMPTY_MAP)
	if vm != nil {
		for bs := vm.bindings.Seq(); bs != nil; bs = bs.Next() {
			entry := bs.First().(MapEntry)
			ret = ret.AssocM(entry.Key(), entry.Value().(*tbox).Load())
		}
	}
	return ret
}

func (vm *VM) getThreadBinding(v *Var) *tbox {
	return vm.bindings.ValAt(v, (*tbox)(nil)).(*tbox)
}

func (vm *VM) CurrentNS() *Namespace {
	return vCURRENT_NS.Deref(vm).(*Namespace)
}

type vmHolder struct {
	vm *VM
}

func (h *vmHolder) PushThreadBindings(bindings PersistentMap) {
	h.vm = PushThreadBindings(h.vm, bindings)
}

func (h *vmHolder) PopThreadBindings() {
	h.vm = PopThreadBindings(h.vm)
}
