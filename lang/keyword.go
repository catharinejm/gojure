package lang

import (
	"fmt"
	"sync"
)

type Keyword struct {
	sym *Symbol
}

var (
	kwcache sync.Map
)

func NewKeyword(sym *Symbol) *Keyword {
	if sym.Meta() != nil {
		sym = sym.WithMeta(nil).(*Symbol)
	}
	return internKeyword(sym)
}

func ParseKeyword(nsname string) *Keyword {
	return NewKeyword(ParseSymbol(nsname))
}

func FindKeyword(sym *Symbol) *Keyword {
	kwiface, _ := kwcache.Load(sym.symbolParts)
	kw, _ := kwiface.(*Keyword)
	return kw
}

// TODO: is there any way to allow keywords to be garbage collected?
// uintptr as weak ptr hack works, but only as long as heap allocations
// are guaranteed not to move
func internKeyword(sym *Symbol) *Keyword {
	if kw, ok := kwcache.Load(sym.symbolParts); ok {
		return kw.(*Keyword)
	} else {
		kw, _ := kwcache.LoadOrStore(sym.symbolParts, &Keyword{sym})
		return kw.(*Keyword)
	}
}

func (k *Keyword) Equiv(other interface{}) bool {
	if other, ok := other.(*Keyword); ok {
		return k == other
	}
	return false
}

func (k *Keyword) Namespace() string {
	return k.sym.ns
}

func (k *Keyword) Name() string {
	return k.sym.name
}

func (k *Keyword) Symbol() *Symbol {
	return k.sym
}

func (k *Keyword) String() string {
	return fmt.Sprintf(":%v", k.sym)
}

//// IFn ////
func (k *Keyword) Invoke(vm *VM, args Seq) interface{} {
	switch RT.Count(args) {
	case 1:
		if ilookup, _ := args.First().(ILookup); ilookup != nil {
			return ilookup.ValAt(k, nil)
		}
		return RT.Get(args.First(), k, nil)
	case 2:
		if ilookup, _ := args.First().(ILookup); ilookup != nil {
			return ilookup.ValAt(k, RT.Second(args))
		}
		return RT.Get(args.First(), k, RT.Second(args))
	default:
		panic(&arityError{RT.Count(args), "1,2"})
	}
}

func (k *Keyword) VM(vm *VM) *VM {
	return vm
}

//// Comparable ////
func (k *Keyword) CompareTo(other interface{}) int {
	return k.sym.CompareTo(other.(*Keyword).sym)
}

var (
	_ IFn        = &Keyword{}
	_ Comparable = &Keyword{}
)
