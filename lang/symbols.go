package lang

var (
	// Special form symbols
	sDEF    *Symbol = NewSymbol("", "def")
	sQUOTE  *Symbol = NewSymbol("", "quote")
	sLET    *Symbol = NewSymbol("", "let*")
	sLOOP   *Symbol = NewSymbol("", "loop*")
	sRECUR  *Symbol = NewSymbol("", "recur")
	sFN     *Symbol = NewSymbol("", "fn*")
	sDO     *Symbol = NewSymbol("", "do")
	sIF     *Symbol = NewSymbol("", "if")
	sVAR    *Symbol = NewSymbol("", "var")
	sGO     *Symbol = NewSymbol("", "go*")
	sDOT    *Symbol = NewSymbol("", ".")
	sMAKE   *Symbol = NewSymbol("", "make")
	sNEW    *Symbol = NewSymbol("", "new")
	sASSIGN *Symbol = NewSymbol("", "set!")

	sPUSH_THREAD_BINDINGS = NewSymbol("", "push-thread-bindings")
	sPOP_THREAD_BINDINGS  = NewSymbol("", "pop-thread-bindings")

	specialSymbols PersistentSet = NewPersistentSet(
		sDEF, sQUOTE, sLET, sLOOP, sRECUR, sFN, sDO, sIF, sVAR, sGO,
		sDOT, sMAKE, sNEW, sASSIGN, sPUSH_THREAD_BINDINGS, sPOP_THREAD_BINDINGS,
	)

	// built-in macros
	sSYNTAX_QUOTE     *Symbol = NewSymbol("", "syntax-quote")
	sUNQUOTE          *Symbol = NewSymbol("", "unquote")
	sUNQUOTE_SPLICING *Symbol = NewSymbol("", "unquote-splicing")

	// Useful symbols
	sAMP *Symbol = NewSymbol("", "&")
	sPCT *Symbol = NewSymbol("", "%")

	// references to Vars
	sAPPLY     *Symbol
	sCONJ      *Symbol
	sLIST      *Symbol
	sCONCAT    *Symbol
	sVEC       *Symbol
	sHASH_MAP  *Symbol
	sDEREF     *Symbol
	sWITH_META *Symbol

	// Useful Keywords
	kTAG         *Keyword = NewKeyword(NewSymbol("", "tag"))
	kRETTAG      *Keyword = NewKeyword(NewSymbol("", "rettag"))
	kFILE        *Keyword = NewKeyword(NewSymbol("", "file"))
	kLINE        *Keyword = NewKeyword(NewSymbol("", "line"))
	kCOLUMN      *Keyword = NewKeyword(NewSymbol("", "column"))
	kDYNAMIC     *Keyword = NewKeyword(NewSymbol("", "dynamic"))
	kMACRO       *Keyword = NewKeyword(NewSymbol("", "macro"))
	kPRIVATE     *Keyword = NewKeyword(NewSymbol("", "private"))
	kARGLISTS    *Keyword = NewKeyword(NewSymbol("", "arglists"))
	kCAPTURE_ENV *Keyword = NewKeyword(NewSymbol("", "capture-env"))
	kONCE        *Keyword = NewKeyword(NewSymbol("", "once"))
	kNO_IMPL     *Keyword = NewKeyword(NewSymbol("", "no-impl"))
)

func IsSpecialSymbol(x interface{}) bool {
	return specialSymbols.Contains(x)
}
