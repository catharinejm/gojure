package lang

import (
	"fmt"
	"os"
	"reflect"
)

type Type struct {
	reflect.Type
	accepts      func(x interface{}) bool
	sliceof      func(x interface{}) interface{}
	valuesliceof func(x interface{}) interface{}
}

func NewType(rtype reflect.Type, accepts func(interface{}) bool, sliceof, valuesliceof func(interface{}) interface{}) *Type {
	return &Type{rtype, accepts, sliceof, valuesliceof}
}

func (t *Type) Accepts(x interface{}) bool {
	return t.accepts != nil && t.accepts(x)
}

func (t *Type) SliceOf(x interface{}) interface{} {
	return t.sliceof(x)
}

func (t *Type) ValueSliceOf(x interface{}) interface{} {
	if t.valuesliceof == nil {
		return t.SliceOf(x)
	}
	return t.valuesliceof(x)
}

func unwrapType(rt reflect.Type) reflect.Type {
	if t, ok := rt.(*Type); ok {
		return t.Type
	}
	return rt
}

func WrapType(rt reflect.Type) *Type {
	if rt == nil {
		return nil
	}
	if t, ok := rt.(*Type); ok {
		return t
	}
	return &Type{
		Type: rt,
		accepts: func(x interface{}) bool {
			fmt.Fprintf(os.Stderr, "reflective type accepts: %v\n", rt)
			return x != nil && reflect.TypeOf(x).AssignableTo(rt)
		},
		sliceof: func(x interface{}) interface{} {
			seq := RT.Seq(x)
			l := RT.Count(seq)
			slice := reflect.MakeSlice(reflect.SliceOf(rt), l, l)
			for i, s := 0, seq; s != nil; i, s = i+1, s.Next() {
				slice.Index(i).Set(massagedValueOf(rt, s.First()))
			}
			return slice.Interface()
		},
	}
}

func TypeOf(x interface{}) *Type {
	if x == nil {
		return nil
	}
	rt := reflect.TypeOf(x)
	if rt.Kind() == reflect.Ptr && rt.Elem().Kind() == reflect.Interface {
		rt = rt.Elem()
	}
	if cached := LookupType(rt); cached != nil {
		return cached.Type()
	}
	return WrapType(rt)
}
