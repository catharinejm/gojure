package lang

import "strings"

type Rune rune

func (r Rune) String() string {
	var sb strings.Builder
	sb.WriteRune('\\')
	switch r {
	case '\n':
		sb.WriteString("newline")
	case ' ':
		sb.WriteString("space")
	case '\t':
		sb.WriteString("tab")
	case '\b':
		sb.WriteString("backspace")
	case '\f':
		sb.WriteString("formfeed")
	case '\r':
		sb.WriteString("return")
	default:
		sb.WriteRune(rune(r))
	}
	return sb.String()
}

func ToRune(x int) Rune {
	return Rune(int32(x))
}

func (r Rune) CompareTo(other interface{}) int {
	return icomp(int(r), int(other.(Rune)))
}

func (r Rune) HashEq() uint32 {
	return HashInt32(int32(r))
}
