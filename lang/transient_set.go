package lang

type transientSet struct {
	m TransientMap
}

func newTransientSet(ps *persistentSet) *transientSet {
	return &transientSet{ps.m.AsTransientM()}
}

func (ts *transientSet) Persistent() PersistentCollection {
	return ts.PersistentS()
}

func (ts *transientSet) PersistentS() PersistentSet {
	return &persistentSet{m: ts.m.PersistentM()}
}

func (ts *transientSet) Count() int {
	return ts.m.Count()
}

func (ts *transientSet) FastCount() int {
	return ts.m.FastCount()
}

func (ts *transientSet) Conj(x interface{}) TransientCollection {
	return ts.ConjS(x)
}

func (ts *transientSet) ConjS(x interface{}) TransientSet {
	if !ts.Contains(x) {
		ts.m.Assoc(x, x)
	}
	return ts
}

func (ts *transientSet) Contains(x interface{}) bool {
	return ts.m.ContainsKey(x)
}

func (ts *transientSet) Disjoin(x interface{}) TransientSet {
	if ts.Contains(x) {
		ts.m.Dissoc(x)
	}
	return ts
}

func (ts *transientSet) Get(x interface{}) interface{} {
	return ts.m.ValAt(x, nil)
}

var (
	_ TransientCollection = &transientSet{}
	_ TransientSet        = &transientSet{}
	_ FastCounter         = &transientSet{}
)
