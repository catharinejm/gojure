package lang

type SliceSort struct {
	cmp   func(x, y interface{}) int
	slice []interface{}
}

func NewSliceSort(cmp IFn, slice []interface{}) *SliceSort {
	if cmp == nil {
		return &SliceSort{RT.Compare, slice}
	}
	return &SliceSort{
		func(x, y interface{}) int {
			return int(cmp.Invoke(nil, NewPersistentList(x, y)).(Number).Int64())
		},
		slice,
	}
}

func (ss SliceSort) Len() int {
	return len(ss.slice)
}

func (ss SliceSort) Less(i, j int) bool {
	return ss.cmp(ss.slice[i], ss.slice[j]) == -1
}

func (ss SliceSort) Swap(i, j int) {
	ss.slice[i], ss.slice[j] = ss.slice[j], ss.slice[i]
}
