package lang

import (
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

var (
	vCURRENT_NS *Var

	vIN  *Var
	vOUT *Var
	vERR *Var

	vUNCHECKED_MATH *Var

	vEVAL_DEBUG         *Var
	vCOMPILE_DEBUG      *Var
	vOPTIMIZE           *Var
	vWARN_ON_REFLECTION *Var

	coreNS *Namespace
)

func InitEnv(loadCore bool) {
	vminit.Do(func() { doInitEnv(loadCore) })
}

func doInitEnv(loadCore bool) {
	initDefaults()

	coreNS = FindOrCreateNamespace(NewSymbol("", "clojure.core"))

	sAPPLY = NewSymbol(coreNS.name.String(), "apply")
	sCONJ = NewSymbol(coreNS.name.String(), "conj")
	sLIST = NewSymbol(coreNS.name.String(), "list")
	sCONCAT = NewSymbol(coreNS.name.String(), "concat")
	sVEC = NewSymbol(coreNS.name.String(), "vec")
	sHASH_MAP = NewSymbol(coreNS.name.String(), "hash-map")
	sDEREF = NewSymbol(coreNS.name.String(), "deref")
	sWITH_META = NewSymbol(coreNS.name.String(), "with-meta")

	vCURRENT_NS = InternVar(coreNS,
		NewSymbol("", "*ns*"),
		coreNS,
	).SetDynamic()
	vCURRENT_NS.ResetMeta(NewPersistentMap(kTAG, TypeOf((*Namespace)(nil))))

	vIN = InternVar(coreNS,
		NewSymbol("", "*in*"),
		os.Stdin,
	).SetDynamic()

	vOUT = InternVar(coreNS,
		NewSymbol("", "*out*"),
		os.Stdout,
	).SetDynamic()
	vOUT.ResetMeta(NewPersistentMap(kTAG, TypeOf((*io.StringWriter)(nil))))

	vERR = InternVar(coreNS,
		NewSymbol("", "*err*"),
		os.Stderr,
	).SetDynamic()
	vERR.ResetMeta(NewPersistentMap(kTAG, TypeOf((*io.StringWriter)(nil))))

	vUNCHECKED_MATH = InternVar(coreNS,
		NewSymbol("", "*unchecked-math*"),
		false,
	).SetDynamic()

	vEVAL_DEBUG = InternVar(coreNS,
		ParseSymbol("*eval-debug*"),
		false,
	).SetDynamic()

	vCOMPILE_DEBUG = InternVar(coreNS,
		ParseSymbol("*compile-debug*"),
		false,
	).SetDynamic()

	vOPTIMIZE = InternVar(coreNS,
		ParseSymbol("*optimize*"),
		true,
	).SetDynamic()

	vWARN_ON_REFLECTION = InternVar(coreNS,
		ParseSymbol("*warn-on-reflection*"),
		false,
	).SetDynamic()

	InternVar(coreNS,
		NewSymbol("", "in-ns"),
		NewDirectFn("in-ns", 1, nil, nil, func(vm *VM, nssym interface{}) interface{} {
			InNamespace(vm, nssym.(*Symbol))
			return nil
		}),
	)

	InternVar(coreNS,
		NewSymbol("", "ns"),
		NewDirectFn("ns", 3, nil, nil, func(vm *VM, __form, __env, sym interface{}) interface{} {
			InNamespace(vm, sym.(*Symbol))
			return nil
		}),
	).SetMacro()

	InternVar(coreNS,
		NewSymbol("", "macroexpand"),
		NewDirectFn("macroexpand", 1, nil, nil, func(vm *VM, x interface{}) interface{} {
			return withByteCompiler(vm, func(bc *ByteCompiler) interface{} { return bc.Macroexpand(x) })
		}),
	)

	InternVar(coreNS,
		NewSymbol("", "macroexpand-all"),
		NewDirectFn("macroexpand-all", 1, nil, nil, func(vm *VM, x interface{}) interface{} {
			return withByteCompiler(vm, func(bc *ByteCompiler) interface{} {
				var macroexpandAll func(interface{}) interface{}
				macroexpandAll = func(x interface{}) interface{} {
					form := bc.Macroexpand(x)
					if seqable, ok := form.(PersistentCollection); ok {
						results := make([]interface{}, 0)
						for seq := seqable.Seq(); seq != nil; seq = seq.Next() {
							switch el := seq.First().(type) {
							case MapEntry:
								results = append(results,
									macroexpandAll(el.Key()), macroexpandAll(el.Value()))
							default:
								results = append(results, macroexpandAll(seq.First()))
							}
						}
						switch form.(type) {
						case PersistentVector:
							return PersistentVectorBuilder.CreateOwning(results...)
						case PersistentMap:
							return NewPersistentMap(results...)
						case PersistentSet:
							return NewPersistentSet(results...)
						default:
							return NewPersistentList(results...)
						}
					}
					return form
				}
				return macroexpandAll(x)
			})
		}),
	)

	InternVar(coreNS,
		NewSymbol("", "macroexpand-1"),
		NewDirectFn("macroexpand-1", 1, nil, nil, func(vm *VM, x interface{}) interface{} {
			return withByteCompiler(vm, func(bc *ByteCompiler) interface{} { return bc.Macroexpand1(x) })
		}),
	)

	InternVar(coreNS,
		NewSymbol("", "compile"),
		NewDirectFn("compile", 1, nil, nil, func(vm *VM, form interface{}) interface{} {
			if iobj, _ := form.(IObj); iobj != nil && iobj.Meta() != nil {
				meta := iobj.Meta().Dissoc(kFILE).Dissoc(kLINE).Dissoc(kCOLUMN)
				if meta.Count() == 0 {
					form = iobj.WithMeta(nil)
				} else {
					form = iobj.WithMeta(meta)
				}
			}
			return RT.Seq(withByteCompiler(vm, func(bc *ByteCompiler) interface{} { return bc.Compile(form).bodies[0] }))
		}),
	)

	InternVar(coreNS,
		NewSymbol("", "fn-body"),
		NewDirectFn("fn-body", 2, nil, nil, func(vm *VM, fn, arity interface{}) interface{} {
			switch fn := fn.(type) {
			case *bc_Fn:
				return RT.Seq(fn.bodies[arity.(Int)])
			case *Fn:
				return RT.Seq(fn.bodies[arity.(Int)])
			default:
				return nil
			}
		}),
	)

	// InternVar(coreNS,
	// 	NewSymbol("", "type"),
	// 	&ReflectiveFn{1, "type", false, reflect.ValueOf(reflect.TypeOf)},
	// )

	// InternVar(coreNS,
	// 	NewSymbol("", "first"),
	// 	&ReflectiveFn{1, "first", false, reflect.ValueOf(RT.First)},
	// )

	// InternVar(coreNS,
	// 	NewSymbol("", "next"),
	// 	&ReflectiveFn{1, "next", false, reflect.ValueOf(RT.Next)},
	// )

	// InternVar(coreNS,
	// 	NewSymbol("", "depush"),
	// 	&ReflectiveFn{1, "depush", false, reflect.ValueOf(func(x interface{}) interface{} {
	// 		if x, ispush := x.(*bc_PushReturn); ispush {
	// 			return x.value
	// 		}
	// 		return x
	// 	})},
	// )

	InternVar(coreNS,
		NewSymbol("", "dequote"),
		NewDirectFn("dequote", 1, nil, nil, func(vm *VM, x interface{}) interface{} {
			if x, isquote := x.(*bc_Quoted); isquote {
				return x.value
			}
			return x
		}),
	)

	InternVar(coreNS,
		NewSymbol("", "println"),
		NewDirectFn("println", -1, nil, nil, func(vm *VM, variadic interface{}) interface{} {
			var sb strings.Builder
			var xs []interface{}
			if variadic != nil {
				xs = variadic.([]interface{})
			}
			for i, x := range xs {
				sb.WriteString(fmt.Sprintf("%v", x))
				if i < len(xs)-1 {
					sb.WriteString(" ")
				}
			}
			fmt.Println(sb.String())
			return nil
		}),
	)

	InternVar(coreNS,
		NewSymbol("", "panic"),
		NewDirectFn("panic", 1, nil, nil, func(vm *VM, x interface{}) interface{} { panic(x) }),
	)

	InternVar(coreNS,
		NewSymbol("", "the-vm"),
		NewDirectFn("the-vm", 0, nil, TypeOf((*VM)(nil)), func(vm *VM) interface{} { return vm }),
	)

	InternVar(coreNS,
		NewSymbol("", "sleep"),
		NewDirectFn("sleep", 1, nil, nil, func(vm *VM, d interface{}) interface{} {
			time.Sleep(time.Duration(d.(Int)) * time.Millisecond)
			return nil
		}),
	)

	InternVar(coreNS,
		ParseSymbol("import"),
		NewDirectFn("import", 4, nil, nil, func(vm *VM, __form, __env, sym, path interface{}) interface{} {
			pkg := FindGoPackage(path.(string))
			if pkg == nil {
				panic(fmt.Errorf("no such package: %q", path))
			}
			vm.CurrentNS().Import(sym.(*Symbol), pkg)
			return nil
		}),
	).SetMacro()

	InternVar(coreNS,
		ParseSymbol("load-core"),
		NewDirectFn("load-core", 0, nil, nil, func(vm *VM) interface{} {
			LoadFile(vm, "./clj/core.clj")
			return nil
		}),
	)

	if loadCore {
		vm := PushDefaultBindings(nil)
		LoadFile(vm, "./clj/core.clj")
	}
}
