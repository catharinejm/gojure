package lang

type Reversible interface {
	RSeq() Seq
}
