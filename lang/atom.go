package lang

type Atom struct {
	state atomicBox
	resettableMetadata
}

func NewAtom(state interface{}) *Atom {
	a := &Atom{}
	a.state.Store(state)
	return a
}

func (a *Atom) Deref(vm *VM) interface{} {
	return a.state.Load()
}

func (a *Atom) Swap(vm *VM, f IFn, args Seq) interface{} {
	return a.state.Swap(func(old interface{}) interface{} {
		return f.Invoke(vm, RT.Cons(old, args))
	})
}

func (a *Atom) SwapVals(vm *VM, f IFn, args Seq) PersistentVector {
	var oldv interface{}
	newv := a.state.Swap(func(old interface{}) interface{} {
		oldv = old
		return f.Invoke(vm, RT.Cons(old, args))
	})
	return PersistentVectorBuilder.CreateOwning(oldv, newv)
}

func (a *Atom) CompareAndSet(old, new interface{}) bool {
	return a.state.CompareAndSwap(old, new)
}

func (a *Atom) Reset(newval interface{}) interface{} {
	a.state.Store(newval)
	return newval
}

func (a *Atom) ResetVals(newval interface{}) interface{} {
	var oldv interface{}
	a.state.Swap(func(old interface{}) interface{} {
		oldv = old
		return newval
	})
	return PersistentVectorBuilder.CreateOwning(oldv, newval)
}
