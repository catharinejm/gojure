package lang

import (
	"strings"
)

type Seq interface {
	PersistentCollection
	First() interface{}
	Next() Seq
}

type Seqable interface {
	Seq() Seq
}

func SeqString(s Seq) string {
	var sb strings.Builder
	sb.WriteString("(")
	for s != nil {
		sb.WriteString(RT.Print(s.First()))
		s = s.Next()
		if s != nil {
			sb.WriteString(" ")
		}
	}
	sb.WriteString(")")
	return sb.String()
}

func SeqCount(s Seq) int {
	c := 0
	for ; s != nil; s = s.Next() {
		if s, isfast := s.(FastCounter); isfast {
			return c + s.FastCount()
		}
		c++
	}
	return c
}

func SeqNth(s Seq, i int, notfound interface{}) interface{} {
	for s != nil && i > 0 {
		s = s.Next()
		i--
	}
	if s == nil || i < 0 {
		return notfound
	}
	return s.First()
}

func SeqEquiv(x Seq, y interface{}) bool {
	if y, ok := y.(Seq); ok {
		if x == y {
			return true
		}
		for x != nil && y != nil {
			if !RT.Equiv(x.First(), y.First()) {
				return false
			}
			x = x.Next()
			y = y.Next()
		}
		return x == nil && y == nil
	}
	return false
}
