package lang

import (
	"fmt"
	"math"
	"math/big"
	"reflect"
)

type Number interface {
	IsInt() bool
	Int() Int
	Int64() int64

	IsFloat() bool
	Float() Float
	Float64() float64

	Promote(n Number) (Number, bool)

	IsZero() bool
	IsPos() bool
	IsNeg() bool

	Add(n Number) Number
	Sub(n Number) Number

	Multiply(n Number) Number
	Divide(n Number) Number

	Quotient(n Number) Number
	Remainder(n Number) Number

	Equiv(n Number) bool

	LT(n Number) bool
	LTE(n Number) bool
	GT(n Number) bool
	GTE(n Number) bool

	Negate() Number

	Inc() Number
	Dec() Number

	Max(n Number) Number
	Min(n Number) Number

	HashEq() uint32

	String() string
}

type NumbersType struct{}

var Numbers *NumbersType = &NumbersType{}

func (_ *NumbersType) Equiv(x, y Number) bool {
	return x.Equiv(y)
}

func (_ *NumbersType) PrintNative() string {
	return "lang.Numbers"
}

func ToNumber(x interface{}) Number {
	switch x := x.(type) {
	case Number:
		return x
	default:
		return valueToNumber(reflect.ValueOf(x))
	}
}

func AssertInt(x interface{}) Int {
	switch x := x.(type) {
	case Int:
		return x
	default:
		return CoerceNumber(x).(Int)
	}
}

func CoerceNumber(x interface{}) Number {
	switch x := x.(type) {
	case Number:
		return x
	default:
		if n := valueToNumber(reflect.ValueOf(x)); n != nil {
			return n
		}
		panic(fmt.Errorf("Can't coerce %T to number", x))
	}
}

func valueToNumber(xval reflect.Value) Number {
	k := xval.Kind()
	switch {
	case kindIsInt(k):
		return Int(xval.Int())
	case kindIsUint(k):
		return Int(int64(xval.Uint()))
	case kindIsFloat(k):
		return Float(xval.Float())
	default:
		return nil
	}
}

type Int int64

func (i Int) IsInt() bool      { return true }
func (i Int) Int() Int         { return i }
func (i Int) Int64() int64     { return int64(i) }
func (i Int) IsFloat() bool    { return false }
func (i Int) Float() Float     { return Float(float64(i)) }
func (i Int) Float64() float64 { return float64(i) }

func (i Int) Promote(n Number) (Number, bool) {
	if n.IsInt() {
		return i, false
	}
	return Float(i.Float()), true
}

func (i Int) IsZero() bool { return i == 0 }
func (i Int) IsPos() bool  { return i > 0 }
func (i Int) IsNeg() bool  { return i < 0 }

func (i Int) Add(n Number) Number {
	switch n := n.(type) {
	case Float:
		return Float(i.Float64() + n.Float64())
	default:
		return Int(i.Int64() + n.Int64())
	}
}

func (i Int) Sub(n Number) Number {
	switch n := n.(type) {
	case Float:
		return Float(i.Float64() + n.Float64())
	default:
		return Int(i.Int64() + n.Int64())
	}
}

func (i Int) Multiply(n Number) Number {
	switch n := n.(type) {
	case Float:
		return Float(i.Float64() * n.Float64())
	default:
		return Int(i.Int64() * n.Int64())
	}
}

// TODO: Divide should return a ratio
func (i Int) Divide(n Number) Number {
	switch n := n.(type) {
	case Float:
		return Float(i.Float64() / n.Float64())
	default:
		return Int(i.Int64() / n.Int64())
	}
}

func (i Int) Quotient(n Number) Number {
	switch n := n.(type) {
	case Float:
		return i.Float().Quotient(n)
	default:
		return Int(i.Int64() / n.Int64())
	}
}

func (i Int) Remainder(n Number) Number {
	switch n := n.(type) {
	case Float:
		return i.Float().Remainder(n)
	default:
		return Int(i.Int64() % n.Int64())
	}
}

func (i Int) Equiv(n Number) bool {
	j, isint := n.(Int)
	return isint && i == j
}

func (i Int) LT(n Number) bool {
	switch n := n.(type) {
	case Float:
		return i.Float64() < n.Float64()
	default:
		return i.Int64() < n.Int64()
	}
}

func (i Int) LTE(n Number) bool {
	switch n := n.(type) {
	case Float:
		return i.Float64() <= n.Float64()
	default:
		return i.Int64() <= n.Int64()
	}
}

func (i Int) GT(n Number) bool {
	switch n := n.(type) {
	case Float:
		return i.Float64() > n.Float64()
	default:
		return i.Int64() > n.Int64()
	}
}

func (i Int) GTE(n Number) bool {
	switch n := n.(type) {
	case Float:
		return i.Float64() >= n.Float64()
	default:
		return i.Int64() >= n.Int64()
	}
}

func (i Int) Negate() Number { return -i }

func (i Int) Inc() Number { return i + 1 }
func (i Int) Dec() Number { return i - 1 }

func (i Int) Max(n Number) Number {
	switch n.(type) {
	case Float:
		return i.Float().Max(n)
	default:
		if i.Int64() > n.Int64() {
			return i
		}
		return n
	}
}

func (i Int) Min(n Number) Number {
	switch n.(type) {
	case Float:
		return i.Float().Min(n)
	default:
		if i.Int64() < n.Int64() {
			return i
		}
		return n
	}
}

//// Comparable ////
func (i Int) CompareTo(other interface{}) int {
	switch n := CoerceNumber(other).(type) {
	case Float:
		return i.Float().CompareTo(n)
	default:
		switch {
		case i.Int64() == n.Int64():
			return 0
		case i.Int64() < n.Int64():
			return -1
		default:
			return 1
		}
	}
}

// Bitwise ops (Int only)
func (i Int) Not() Int {
	return Int(^i)
}

func (i Int) And(j Int) Int {
	return Int(i & j)
}

func (i Int) Or(j Int) Int {
	return Int(i | j)
}

func (i Int) Xor(j Int) Int {
	return Int(i ^ j)
}

func (i Int) AndNot(j Int) Int {
	return Int(i &^ j)
}

func (i Int) ClearBit(bit Int) Int {
	return Int(i &^ (1 << uint64(bit)))
}

func (i Int) SetBit(bit Int) Int {
	return Int(i | (1 << uint64(bit)))
}

func (i Int) FlipBit(bit Int) Int {
	return Int(i ^ (1 << uint64(bit)))
}

func (i Int) TestBit(bit Int) bool {
	return i&(1<<uint64(bit)) != 0
}

func (i Int) ShiftLeft(shift Int) Int {
	return Int(i << uint64(shift))
}

func (i Int) ShiftRight(shift Int) Int {
	return Int(i >> uint64(shift))
}

func (i Int) UnsignedShiftRight(shift Int) Int {
	return Int(int64(uint64(i) >> uint64(shift)))
}

func (i Int) HashEq() uint32 {
	return HashInt64(int64(i))
}

func (i Int) String() string {
	return fmt.Sprint(int64(i))
}

type Float float64

func (f Float) IsInt() bool      { return false }
func (f Float) Int() Int         { return Int(int64(f)) }
func (f Float) Int64() int64     { return int64(f) }
func (f Float) IsFloat() bool    { return true }
func (f Float) Float() Float     { return f }
func (f Float) Float64() float64 { return float64(f) }

func (f Float) Promote(n Number) (Number, bool) {
	return f, false
}

func (f Float) IsZero() bool { return f == 0 }
func (f Float) IsPos() bool  { return f > 0 }
func (f Float) IsNeg() bool  { return f < 0 }

func (f Float) Add(n Number) Number {
	return Float(f.Float64() + n.Float64())
}

func (f Float) Sub(n Number) Number {
	return Float(f.Float64() - n.Float64())
}

func (f Float) Multiply(n Number) Number {
	return Float(f.Float64() * n.Float64())
}

func (f Float) Divide(n Number) Number {
	return Float(f.Float64() / n.Float64())
}

func (f Float) Quotient(n Number) Number {
	q := f.Float64() / n.Float64()
	qi, acc := big.NewFloat(q).Int64()
	if acc == big.Exact || qi > math.MinInt64 && qi < math.MaxInt64 {
		return Float(float64(qi))
	}
	// bi, _ := q.Int(nil)
	// q.SetInt(bi)
	// return q
	panic(fmt.Errorf("quotient overflow"))
}

func (f Float) Remainder(n Number) Number {
	q := f.Float64() / n.Float64()
	qi, acc := big.NewFloat(q).Int64()
	if acc == big.Exact || qi > math.MinInt64 && qi < math.MaxInt64 {
		return Float(f.Float64() - float64(qi)*n.Float64())
	}
	panic(fmt.Errorf("remainder overflow"))
}

func (f Float) Equiv(n Number) bool {
	g, isfloat := n.(Float)
	return isfloat && f == g
}

func (f Float) LT(n Number) bool {
	return f.Float64() < n.Float64()
}

func (f Float) LTE(n Number) bool {
	return f.Float64() <= n.Float64()
}

func (f Float) GT(n Number) bool {
	return f.Float64() > n.Float64()
}

func (f Float) GTE(n Number) bool {
	return f.Float64() >= n.Float64()
}

func (f Float) Negate() Number { return -f }

func (f Float) Inc() Number { return f + 1 }
func (f Float) Dec() Number { return f - 1 }

func (f Float) Max(n Number) Number {
	if math.IsNaN(f.Float64()) {
		return f
	}
	if math.IsNaN(n.Float64()) {
		return n
	}
	if f.Float64() > n.Float64() {
		return f
	}
	return n
}

func (f Float) Min(n Number) Number {
	if math.IsNaN(f.Float64()) {
		return f
	}
	if math.IsNaN(n.Float64()) {
		return n
	}
	if f.Float64() < n.Float64() {
		return f
	}
	return n
}

//// Comparable ////
func (f Float) CompareTo(other interface{}) int {
	n := CoerceNumber(other)
	switch {
	case f.Float64() < n.Float64():
		return -1
	case f.Float64() > n.Float64():
		return 1
	default:
		return 0
	}
}

const negativeZero = 1 << 63

func (f Float) HashEq() uint32 {
	bits := math.Float64bits(float64(f))
	if bits == negativeZero {
		return 0 // same as positive zero
	}
	return HashInt64(int64(bits))
}

func (f Float) String() string {
	return fmt.Sprint(float64(f))
}

var (
	_ Number = Int(0)
	_ Number = Float(0)
)
