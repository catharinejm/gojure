package lang_test

import (
	"testing"

	. "gitlab.com/catharinejm/gojure/lang"
)

func TestRT_Equiv(t *testing.T) {
	foo := NewSymbol("", "foo")
	foo2 := NewSymbol("", "foo")
	bar := NewSymbol("", "bar")
	if !RT.Equiv(foo, foo) {
		t.Errorf("foo does not equal itself!")
	}
	if !RT.Equiv(foo, foo2) {
		t.Errorf("foo does not equal identical foo2")
	}
	if RT.Equiv(foo, bar) {
		t.Errorf("foo equals bar???")
	}
}

func TestRT_Equiv_Numbers(t *testing.T) {
	if !RT.Equiv(int(10), int64(10)) {
		t.Errorf("int(10) is not int64(10)")
	}
	if !RT.Equiv(uint(10), uint16(10)) {
		t.Errorf("uint(10) is not uint16(10)")
	}
	if !RT.Equiv(float32(1.0), float64(1.0)) {
		t.Errorf("float32(1.0) is not float64(1.0)")
	}
}
