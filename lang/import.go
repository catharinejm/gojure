package lang

import (
	"fmt"
)

type Import struct {
	gopkg  *GoPackage
	name   *Symbol
	object interface{}
}

func NewImport(pkg *GoPackage, name *Symbol, object interface{}) *Import {
	imp := &Import{pkg, name, object}
	// switch object := object.(type) {
	// case nil, reflect.Type, *ReflectiveFn:
	// 	imp.object = object
	// default:
	// 	val := reflect.ValueOf(object)
	// 	if val.Kind() == reflect.Func {
	// 		imp.object = mkReflectiveFn(name.String(), val)
	// 	} else {
	// 		imp.object = object
	// 	}
	// }
	return imp
}

func (imp *Import) Object() interface{} {
	return imp.object
}

func (imp *Import) String() string {
	return fmt.Sprintf("#import[%q.%v %v]", imp.gopkg.path, imp.name, imp.object)
}
