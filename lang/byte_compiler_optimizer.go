package lang

import (
	"errors"
	"fmt"
	"os"
	"reflect"
)

type optimizer struct {
	bc     *ByteCompiler
	types  stack
	locals stack
	newPCs []int
	warn   bool
}

type indexedType struct {
	pc  int
	typ *Type

	restArity    int
	appliedTypes [MAX_ARITY + 1]*Type
}

func (ixt *indexedType) setArglists(arglists Seq, resolveTag func(PersistentMap) *Type) {
	maxFixed := -1
	for arglists = RT.Seq(arglists); arglists != nil; arglists = arglists.Next() {
		args := arglists.First().(PersistentVector)
		if args.Count() > 1 && RT.Equiv(args.Nth(args.Count()-2, nil), sAMP) {
			ixt.restArity = args.Count() - 1
			ixt.appliedTypes[args.Count()-1] = resolveTag(RT.Meta(args))
		} else {
			if args.Count() > maxFixed {
				maxFixed = args.Count()
			}
			ixt.appliedTypes[args.Count()] = resolveTag(RT.Meta(args))
		}
	}
	if maxFixed+1 < ixt.restArity {
		ixt.appliedTypes[ixt.restArity-1] = ixt.appliedTypes[ixt.restArity]
	}
}

func (o *optimizer) optimize(fn *bc_Fn) *bc_Fn {
	o.warn = RT.BoolCast(vWARN_ON_REFLECTION.Deref(o.bc.vm))
	for bix, m := range fn.bodies {
		if m != nil {
			o.types = stack{}
			o.locals = stack{}
			for i := 0; i <= bix; i++ {
				o.locals.push(nil)
			}
			o.newPCs = nil
			fn.bodies[bix] = o.optimizeBody(m)
		}
	}
	return fn
}

func isPop(code interface{}) bool {
	_, ispop := code.(*bc_PopReturn)
	return ispop
}

func (o *optimizer) adjustPC(pc int) int {
	for i := len(o.newPCs) - 1; i >= 0; i-- {
		if pc >= o.newPCs[i] {
			return pc + i + 1
		}
	}
	return pc
}

func (o *optimizer) fixPCs(code []interface{}) {
	for _, bytecode := range code {
		switch bytecode := bytecode.(type) {
		case *bc_Recur:
			bytecode.pc = o.adjustPC(bytecode.pc)
		case *bc_Branch:
			bytecode.elsePC = o.adjustPC(bytecode.elsePC)
		case *bc_Jump:
			bytecode.pc = o.adjustPC(bytecode.pc)
		}
	}
}

func (o *optimizer) addPC(pc int) {
	if len(o.newPCs) == 0 || o.newPCs[len(o.newPCs)-1] <= pc {
		o.newPCs = append(o.newPCs, pc)
	} else {
		i := len(o.newPCs) - 1
		for ; i >= 0 && o.newPCs[i] > pc; i-- {
		}
		o.newPCs = append(o.newPCs, 0)
		copy(o.newPCs[i+2:], o.newPCs[i+1:])
		o.newPCs[i+1] = pc
	}
}

func (o *optimizer) optimizeBody(code []interface{}) (output []interface{}) {
	for pc := 0; pc < len(code); pc++ {
		o.doOptimizeBody(code, &pc, &output)
	}
	if o.bc.dbg {
		o.bc.debug("input:")
		for i, c := range code {
			o.bc.debug("\t% 3d: %v", i, c)
		}
	}
	o.fixPCs(output)
	if o.bc.dbg {
		o.bc.debug("output:")
		for i, c := range output {
			o.bc.debug("\t% 3d: %v", i, c)
		}
	}
	return
}

func (o *optimizer) doOptimizeBody(code []interface{}, pc *int, output *[]interface{}) {
	bytecode := code[*pc]
	// o.bc.debug("optimize bytecode: (pc = %d) %v", pc, bytecode)
	switch bytecode := bytecode.(type) {
	case *bc_FieldOrMethodCall:
		args := o.types.peekN(bytecode.count)
		ixt := args[0].(indexedType)
		recvtype := bytecode.recvTag
		if recvtype == nil {
			recvtype = ixt.typ
		}
		if recvtype != nil {
			if cached := LookupType(recvtype.Type); cached != nil {
				if m, _ := cached.Methods().ValAt(bytecode.selector, nil).(*DirectFn); m != nil {
					o.types.popN(bytecode.count)
					adjustedPC := o.adjustPC(ixt.pc)
					o.bc.debug("found direct fn for %v", recvtype)
					o.bc.debug("indexedType{pc: %d, typ: %v} (adjustedPC = %d)", ixt.pc, ixt.typ, adjustedPC)
					*output = append(*output, bytecode)
					o.bc.debug("output before = %v", (*output)[adjustedPC:])
					copy((*output)[adjustedPC+1:], (*output)[adjustedPC:])
					(*output)[adjustedPC] = &bc_Quoted{m}
					o.pushInvokeType(o.directFnType(m, ixt.pc), bytecode.count+1)
					*output = append(*output, &bc_Invoke{bytecode.count + 1, bytecode.sourceInfo})
					o.bc.debug("output after  = %v", (*output)[adjustedPC:])
					o.addPC(ixt.pc + bytecode.count + 1)
					return
				}
			}
		}
		if o.warn {
			fmt.Fprintf(os.Stderr, "%v: no direct invoke for (%v).%s\n", bytecode.sourceInfo, recvtype, bytecode.selector.Name())
		}
	case *bc_Branch:
		*output = append(*output, bytecode)
		o.types.pop() // pop condition
		elsePC := bytecode.elsePC
		endPC := code[elsePC-1].(*bc_Jump).pc
		// if o.bc.dbg {
		// 	o.bc.debug("optimizing branch: (elsePC = %d, endPC = %d)", elsePC, endPC)
		// 	for i := *pc; i < endPC; i++ {
		// 		o.bc.debug("\t% 3d: %v", i, code[i])
		// 	}
		// 	o.bc.debug("\nthen code:")
		// 	for i, c := range code[*pc+1 : elsePC-1] {
		// 		o.bc.debug("\t% 3d: %v", i+*pc+1, c)
		// 	}
		// 	o.bc.debug("\nelse code:")
		// 	for i, c := range code[elsePC:endPC] {
		// 		o.bc.debug("\t% 3d: %v", i+elsePC, c)
		// 	}
		// }
		for *pc++; *pc < elsePC-1; *pc++ {
			o.doOptimizeBody(code, pc, output)
		}
		*output = append(*output, code[elsePC-1])
		thenType := o.popType()
		for *pc = elsePC; *pc < endPC; *pc++ {
			o.doOptimizeBody(code, pc, output)
		}
		elseType := o.popType()
		if thenType.typ == nil || elseType.typ == nil {
			o.pushType(nil, endPC-1)
		} else if thenType.typ.Type.AssignableTo(elseType.typ.Type) {
			o.pushType(elseType.typ, endPC-1)
		} else if elseType.typ.Type.AssignableTo(thenType.typ.Type) {
			o.pushType(thenType.typ, endPC-1)
		} else {
			o.pushType(nil, endPC-1)
		}
		*pc = endPC - 1
		return
	}
	o.evalType(bytecode, *pc)
	*output = append(*output, bytecode)
}

var (
	listtype         = TypeOf((*PersistentList)(nil))
	vectortype       = TypeOf((*PersistentVector)(nil))
	maptype          = TypeOf((*PersistentMap)(nil))
	settype          = TypeOf((*PersistentSet)(nil))
	inttype          = TypeOf(Int(0))
	floattype        = TypeOf(Float(0))
	runetype         = TypeOf(Rune(0))
	keywordtype      = TypeOf((*Keyword)(nil))
	symboltype       = TypeOf((*Symbol)(nil))
	vartype          = TypeOf((*Var)(nil))
	directfntype     = TypeOf((*DirectFn)(nil))
	reflectivefntype = TypeOf((*ReflectiveFn)(nil))
)

func (o *optimizer) pushType(t *Type, pc int) {
	o.types.push(indexedType{pc: pc, typ: t})
}

func (o *optimizer) popType() indexedType {
	return o.types.pop().(indexedType)
}

func (o *optimizer) evalType(bytecode interface{}, pc int) {
	switch bytecode := bytecode.(type) {
	case *bc_DerefVar:
		t := o.bc.resolveTag(bytecode.v.Meta())
		arglists := RT.Seq(RT.ValAt(bytecode.v.Meta(), kARGLISTS, nil))
		ixt := indexedType{pc: pc, typ: t}
		ixt.setArglists(arglists, o.bc.resolveTag)
		o.types.push(ixt)
	case *bc_Def:
		topPC := pc
		if bytecode.hasValue {
			topPC = o.popType().pc
		}
		if bytecode.hasMeta {
			topPC = o.popType().pc
		}
		o.pushType(vartype, topPC)
	case *bc_Param:
		if bytecode.tag != nil {
			o.pushType(bytecode.tag, pc)
		} else {
			t, _ := o.locals.at(bytecode.index).(*Type)
			o.pushType(t, pc)
		}
	case *bc_ClosedOver:
		o.pushType(bytecode.tag, pc)
	case *bc_Quoted:
		switch val := bytecode.value.(type) {
		case *DirectFn:
			o.types.push(o.directFnType(val, pc))
		case *ReflectiveFn:
			o.types.push(o.reflectiveFnType(val, pc))
		// case IFn:
		// 	push(nil)
		default:
			o.pushType(TypeOf(val), pc)
		}
	case *bc_EmptyList:
		topPC := pc
		if bytecode.hasMeta {
			topPC = o.popType().pc
		}
		o.pushType(listtype, topPC)
	case *bc_Vector:
		topPC := pc
		if bytecode.count > 0 {
			o.types.popN(bytecode.count - 1)
			topPC = o.popType().pc
		}
		if bytecode.hasMeta {
			topPC = o.popType().pc
		}
		o.pushType(vectortype, topPC)
	case *bc_Map:
		topPC := pc
		if bytecode.count > 0 {
			o.types.popN(bytecode.count - 1)
			topPC = o.popType().pc
		}
		if bytecode.hasMeta {
			topPC = o.popType().pc
		}
		o.pushType(maptype, topPC)
	case *bc_Set:
		topPC := pc
		if bytecode.count > 0 {
			o.types.popN(bytecode.count - 1)
			topPC = o.popType().pc
		}
		if bytecode.hasMeta {
			topPC = o.popType().pc
		}
		o.pushType(settype, topPC)
	case *bc_Fn:
		topPC := pc
		if bytecode.closes > 0 {
			o.types.popN(bytecode.closes - 1)
			topPC = o.popType().pc
		}
		if bytecode.hasMeta {
			topPC = o.popType().pc
		}
		o.pushType(bytecode.rettag, topPC)
	case *bc_Invoke:
		o.types.popN(bytecode.count - 1)
		ret := o.popType()
		o.pushInvokeType(ret, bytecode.count)
	case *bc_Branch:
		// o.types.pop()
		panic(errors.New("branch must be handled outside of evalType()"))
	case *bc_PushLocal:
		o.locals.push(o.popType().typ)
	case *bc_PopLocals:
		o.locals.popN(bytecode.count)
	case *bc_Jump:
		panic(errors.New("jump must be handled outside of evalType()"))
	case *bc_PopReturn:
		o.types.pop()
	case *bc_Recur:
		// noop
	case *bc_PushThreadBindings:
		o.types.pop()
	case *bc_PopThreadBindings:
		// noop
	case *bc_Go:
		// noop
	case *bc_FieldOrMethodCall:
		recvixt := o.types.peekN(bytecode.count)[0].(indexedType)
		recvtype := recvixt.typ
		o.types.popN(bytecode.count)
		if recvtype != nil {
			if m, ok := recvtype.MethodByName(bytecode.selector.Name()); ok {
				o.types.push(indexedType{pc: recvixt.pc, typ: o.reflectiveReturnType(m.Type)})
				break
			} else {
				if recvtype.Kind() == reflect.Ptr {
					recvtype = WrapType(recvtype.Elem())
				}
				if recvtype.Kind() == reflect.Struct {
					if f, ok := recvtype.FieldByName(bytecode.selector.Name()); ok {
						o.types.push(indexedType{
							pc:  recvixt.pc,
							typ: WrapType(massageReturnType(f.Type)),
						})
						break
					}
				}
			}
		}
		o.types.push(indexedType{pc: recvixt.pc})
	case *bc_SetField:
		o.types.pop()
		topPC := o.popType().pc
		o.pushType(nil, topPC)
	case *bc_Make:
		topPC := pc
		if bytecode.count > 0 {
			o.types.popN(bytecode.count - 1)
			topPC = o.popType().pc
		}
		o.pushType(bytecode.typ, topPC)
	case *bc_New:
		o.pushType(bytecode.typ, pc)
	}
}

func (o *optimizer) pushInvokeType(ret indexedType, invokeCount int) {
	// o.bc.debug("pushInvokeType: %#v", ret)
	invtype := indexedType{pc: ret.pc}
	if ret.restArity > 0 && ret.restArity <= invokeCount-1 {
		invtype.typ = ret.appliedTypes[ret.restArity]
	} else if invokeCount-1 < len(ret.appliedTypes) {
		invtype.typ = ret.appliedTypes[invokeCount-1]
	}
	if invtype.typ == nil {
		invtype.typ = ret.typ
	}
	// o.bc.debug("\tpushing: %#v", invtype)
	o.types.push(invtype)
}

func (o *optimizer) directFnType(fn *DirectFn, pc int) indexedType {
	return o.fnType(directfntype, fn.rettype, fn.arity, pc)
}

func (o *optimizer) reflectiveFnType(fn *ReflectiveFn, pc int) indexedType {
	return o.fnType(reflectivefntype, o.reflectiveReturnType(fn.fn.Type()), fn.arity, pc)
}

func (o *optimizer) fnType(fntype, rettype *Type, arity, pc int) indexedType {
	ixt := indexedType{pc: pc, typ: fntype}
	if arity < 0 {
		ixt.restArity = -arity
		ixt.appliedTypes[-arity-1] = rettype
		ixt.appliedTypes[-arity] = rettype
	} else {
		ixt.appliedTypes[arity] = rettype
	}
	return ixt
}

func (o *optimizer) reflectiveReturnType(fntype reflect.Type) (rettype *Type) {
	if fntype.NumOut() == 1 {
		rettype = WrapType(massageReturnType(fntype.Out(0)))
	} else if fntype.NumOut() > 1 {
		rettype = vectortype
	}
	return
}

func massageReturnType(typ reflect.Type) reflect.Type {
	switch {
	case typ.Kind() == reflect.Struct || typ.Kind() == reflect.Array:
		return reflect.PtrTo(typ)
	case kindIsInt(typ.Kind()) || kindIsUint(typ.Kind()):
		if typ.String() == "rune" {
			return runetype
		}
		return inttype
	case kindIsFloat(typ.Kind()):
		return floattype
	default:
		return typ
	}
}
