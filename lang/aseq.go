package lang

type aseq struct {
	first func() interface{}
	next  func() Seq
	count func() int
	metadata
}

type aseqFastCount struct {
	aseq
}

func (s *aseqFastCount) FastCount() int {
	return s.count()
}

func (s *aseqFastCount) WithMeta(meta PersistentMap) IObj {
	if s.meta == meta {
		return s
	}
	aseq := s.aseq
	aseq.meta = meta
	return &aseqFastCount{aseq}
}

func (s *aseq) Count() int {
	return s.count()
}

func (s *aseq) Seq() Seq {
	return s
}

func (s *aseq) First() interface{} {
	return s.first()
}

func (s *aseq) Next() Seq {
	return s.next()
}

func (s *aseq) Conj(x interface{}) PersistentCollection {
	return newCons(x, s)
}

func (s *aseq) Equiv(x interface{}) bool {
	return SeqEquiv(s, x)
}

func (s *aseq) String() string {
	return SeqString(s)
}

func (s *aseq) WithMeta(meta PersistentMap) IObj {
	if s.meta == meta {
		return s
	}
	return &aseq{s.first, s.next, s.count, metadata{meta}}
}

var (
	_ PersistentCollection = &aseq{}
	_ Seq                  = &aseq{}
	_ IObj                 = &aseq{}

	_ PersistentCollection = &aseqFastCount{}
	_ Seq                  = &aseqFastCount{}
	_ IObj                 = &aseqFastCount{}
	_ FastCounter          = &aseqFastCount{}
)
