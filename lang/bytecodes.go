package lang

import (
	"fmt"
	"strings"
)

//// Values/exprs ////

type bc_Fn struct {
	name       *Symbol
	closes     int
	restArity  int
	bodies     [MAX_ARITY + 1][]interface{}
	captureEnv bool
	hasMeta    bool
	rettag     *Type
}

type sourceInfoHaver interface {
	SourceInfo() sourceInfo
}

type sourceInfo struct {
	file string
	line int
	col  int
	fn   string
}

func (si sourceInfo) SourceInfo() sourceInfo {
	return si
}

func (si sourceInfo) String() string {
	var sb strings.Builder
	sb.WriteString(si.file)
	if si.line > 0 {
		sb.WriteString(fmt.Sprint(":", si.line))
		if si.col > 0 {
			sb.WriteString(fmt.Sprint(":", si.col))
		}
	}
	return sb.String()
}

func (si sourceInfo) FnName() string {
	return si.fn
}

type bc_Invoke struct {
	count int
	sourceInfo
}

type bc_DerefVar struct {
	v *Var
}

type bc_Def struct {
	v        *Var
	hasValue bool
	hasMeta  bool
}

type bc_Param struct {
	index int
	name  *Symbol
	tag   *Type
}

type bc_ClosedOver struct {
	index int
	name  *Symbol
	tag   *Type
}

type bc_Quoted struct {
	value interface{}
}

type bc_EmptyList struct {
	hasMeta bool
}

type bc_Vector struct {
	count   int
	hasMeta bool
}

type bc_Map struct {
	count   int
	hasMeta bool
}

type bc_Set struct {
	count   int
	hasMeta bool
}

type bc_FieldOrMethodCall struct {
	count    int
	selector *Keyword
	recvTag  *Type
	sourceInfo
}

type bc_SetField struct {
	field *Keyword
	sourceInfo
}

type bc_New struct {
	typ *Type
}

type bc_Make struct {
	typ   *Type
	count int
}

//// Control flow ////

type bc_PushLocal struct {
	name *Symbol
}

type bc_Go struct{}

type bc_Recur struct {
	pc         int
	firstLocal int
	arity      int
}

type bc_PopLocals struct {
	count int
}

type bc_PopReturn struct{}

type bc_Jump struct {
	pc int
}

type bc_Branch struct {
	elsePC int
}

type bc_PushThreadBindings struct{}

type bc_PopThreadBindings struct{}

//// String() defs ////

func (bc *bc_Fn) String() string {
	return fmt.Sprintf("bc_Fn{%v}", bc.name)
}

func (bc *bc_Invoke) String() string {
	return fmt.Sprintf("bc_Invoke{%d}", bc.count)
}

func (bc *bc_DerefVar) String() string {
	return fmt.Sprintf("bc_DerefVar{%v}", bc.v)
}

func (bc *bc_Def) String() string {
	return fmt.Sprintf("bc_Def{%v}", bc.v)
}
func (bc *bc_Param) String() string {
	return fmt.Sprintf("bc_Param{%d, %v, tag = %v}", bc.index, bc.name, bc.tag)
}

func (bc *bc_ClosedOver) String() string {
	return fmt.Sprintf("bc_ClosedOver{%d, %v, tag = %v}", bc.index, bc.name, bc.tag)
}

func (bc *bc_Quoted) String() string {
	if _, isstr := bc.value.(string); isstr {
		return fmt.Sprintf("bc_Quoted{%q}", bc.value)
	}
	return fmt.Sprintf("bc_Quoted{%v}", bc.value)
}

func (bc *bc_EmptyList) String() string {
	return "bc_EmptyList{}"
}

func (bc *bc_Vector) String() string {
	return fmt.Sprintf("bc_Vector{%d}", bc.count)
}

func (bc *bc_Map) String() string {
	return fmt.Sprintf("bc_Map{%d}", bc.count)
}

func (bc *bc_Set) String() string {
	return fmt.Sprintf("bc_Set{%d}", bc.count)
}

func (bc *bc_FieldOrMethodCall) String() string {
	return fmt.Sprintf("bc_FieldOrMethodCall{%v, args = %d}", bc.selector, bc.count)
}

func (bc *bc_SetField) String() string {
	return fmt.Sprintf("bc_SetField{%v}", bc.field)
}

func (bc *bc_Make) String() string {
	return fmt.Sprintf("bc_Make{%v}", bc.typ)
}

func (bc *bc_New) String() string {
	return fmt.Sprintf("bc_New{%v}", bc.typ)
}

func (bc *bc_PushLocal) String() string {
	return fmt.Sprintf("bc_PushLocal{%v}", bc.name)
}

func (bc *bc_Go) String() string {
	return "bc_Go{}"
}

func (bc *bc_Recur) String() string {
	return fmt.Sprintf("bc_Recur{pc = %d, firstLocal = %d, arity = %d}", bc.pc, bc.firstLocal, bc.arity)
}

func (bc *bc_PopLocals) String() string {
	return fmt.Sprintf("bc_PopLocals{%d}", bc.count)
}

func (bc *bc_PopReturn) String() string {
	return "bc_PopReturn{}"
}

func (bc *bc_Jump) String() string {
	return fmt.Sprintf("bc_Jump{%d}", bc.pc)
}

func (bc *bc_Branch) String() string {
	return fmt.Sprintf("bc_Branch{%d}", bc.elsePC)
}

func (bc *bc_PushThreadBindings) String() string {
	return "bc_PushThreadBindings{}"
}

func (bc *bc_PopThreadBindings) String() string {
	return "bc_PopThreadBindings{}"
}
