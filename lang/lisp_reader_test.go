package lang_test

import (
	"io"
	"testing"

	. "gitlab.com/catharinejm/gojure/lang"
)

type T struct{ *testing.T }

func (t T) testRead(s string, expected interface{}) {
	var form, err interface{}
	func() {
		defer func() {
			err = recover()
		}()
		form = ReadString(FreshVM(), s)
		return
	}()
	if err != nil {
		t.Errorf("%#v -- error is not nil: %v\n", s, err)
	}
	if !RT.Equiv(form, expected) {
		t.Errorf("%#v -- form is not %v: %v\n", s, expected, form)
	}
}

func TestLispReader_Comment(t *testing.T) {
	T{t}.testRead("; comment\n", io.EOF)
	T{t}.testRead("; comment", io.EOF)
	T{t}.testRead("; comment\n foo", ParseSymbol("foo"))
	T{t}.testRead("[10 ;comment\n ]", NewPersistentVector(10))
	T{t}.testRead("10;asdf", 10)
	T{t}.testRead("#! comment\n", io.EOF)
	T{t}.testRead("#! comment", io.EOF)
	T{t}.testRead("#! comment\n foo", ParseSymbol("foo"))
	T{t}.testRead("[10 #!comment\n ]", NewPersistentVector(10))
	T{t}.testRead("10 #!asdf", 10)
}

func TestLispReader_DiscardMacro(t *testing.T) {
	T{t}.testRead("#_foo", io.EOF)
	T{t}.testRead("#_(foo)", io.EOF)
	T{t}.testRead("#_(foo) 10", 10)
	T{t}.testRead("(#_foo)", EMPTY_LIST)
	T{t}.testRead("(#_foo 123)", NewPersistentList(123))
}
