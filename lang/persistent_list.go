package lang

import "errors"

type PersistentList interface {
	Indexer
	IObj
	Seq
	FastCount() int
}

type persistentList struct {
	first interface{}
	rest  PersistentList
	count int
	metadata
	orderedHashEqCache
}

var (
	emptyHashEq            = HashOrdered(nil)
	EMPTY_LIST  *emptyList = &emptyList{}
)

type emptyList struct {
	metadata
}

func NewPersistentList(elems ...interface{}) PersistentList {
	l := PersistentList(EMPTY_LIST)
	for i := len(elems) - 1; i >= 0; i-- {
		l = &persistentList{
			first: elems[i],
			rest:  l,
			count: len(elems) - i,
		}
	}
	return l
}

func (pl *persistentList) Conj(x interface{}) PersistentCollection {
	return &persistentList{
		first:    x,
		rest:     pl,
		count:    pl.count + 1,
		metadata: pl.metadata,
	}
}

func (pl *persistentList) Count() int {
	return pl.count
}

func (pl *persistentList) FastCount() int {
	return pl.count
}

func (pl *persistentList) First() interface{} {
	return pl.first
}

func (pl *persistentList) Next() Seq {
	if pl.count == 1 {
		return nil
	}
	return pl.rest
}

func (pl *persistentList) Nth(i int, notFound interface{}) interface{} {
	if i < 0 || i >= pl.count {
		return notFound
	}
	l := Seq(pl)
	for j := 0; j < i; j++ {
		l = l.Next()
	}
	return l.First()
}

func (pl *persistentList) Seq() Seq {
	return pl
}

func (pl *persistentList) Peek() interface{} {
	return pl.First()
}

func (pl *persistentList) Pop() PersistentStack {
	if pl.count == 1 {
		return EMPTY_LIST.WithMeta(pl.meta).(PersistentStack)
	}
	return pl.rest.(PersistentStack)
}

func (pl *persistentList) WithMeta(newMeta PersistentMap) IObj {
	if pl.meta == newMeta {
		return pl
	}
	newPL := *pl
	newPL.metadata = metadata{newMeta}
	return &newPL
}

func (pl *persistentList) String() string {
	return SeqString(pl.Seq())
}

func (pl *persistentList) Equiv(other interface{}) bool {
	switch other := other.(type) {
	case *persistentList:
		if pl == other {
			return true
		}
		return SeqEquiv(pl, other)
	case Seq:
		return SeqEquiv(pl, other)
	default:
		return false
	}
}

func (pl *persistentList) HashEq() uint32 {
	return pl.orderedHashEq(pl)
}

/// empty list

func (el *emptyList) Conj(x interface{}) PersistentCollection {
	return &persistentList{
		first:    x,
		rest:     el,
		count:    1,
		metadata: el.metadata,
	}
}

func (el *emptyList) Count() int {
	return 0
}

func (el *emptyList) FastCount() int {
	return 0
}

func (el *emptyList) First() interface{} {
	return nil
}

func (el *emptyList) NextList() PersistentList {
	return nil
}

func (el *emptyList) Next() Seq {
	return nil
}

func (el *emptyList) Nth(i int, notfound interface{}) interface{} {
	return notfound
}

func (el *emptyList) Seq() Seq {
	return nil
}

func (el *emptyList) Peek() interface{} {
	return nil
}

func (el *emptyList) Pop() PersistentStack {
	panic(errors.New("can't pop empty list"))
}

func (el *emptyList) WithMeta(newMeta PersistentMap) IObj {
	if el.meta == newMeta {
		return el
	}
	return &emptyList{metadata: metadata{newMeta}}
}

func (el *emptyList) String() string {
	return "()"
}

func (el *emptyList) Equiv(other interface{}) bool {
	switch other := other.(type) {
	case Seq:
		return other.Seq() == nil
	default:
		return false
	}
}

func (el *emptyList) HashEq() uint32 {
	return emptyHashEq
}

var (
	_ PersistentList       = &persistentList{}
	_ PersistentCollection = &persistentList{}
	_ PersistentStack      = &persistentList{}
	_ FastCounter          = &persistentList{}
	_ IMeta                = &persistentList{}
	_ IObj                 = &persistentList{}

	_ PersistentList       = &emptyList{}
	_ PersistentStack      = &emptyList{}
	_ PersistentCollection = &emptyList{}
	_ FastCounter          = &emptyList{}
	_ IMeta                = &emptyList{}
	_ IObj                 = &emptyList{}
)
