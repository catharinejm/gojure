package lang

import (
	"fmt"
	"reflect"
	"strings"
)

const (
	MAX_ARITY = 10
)

var (
	vmType reflect.Type = reflect.TypeOf((*VM)(nil))
)

type arityError struct {
	given    int
	expected string
}

func (e *arityError) Error() string {
	return fmt.Sprintf("invalid arity, got: %v, expected: %s", e.given, e.expected)
}

type IFn interface {
	// Invoke invokes a Fn with the given EVALUATED args
	Invoke(vm *VM, args Seq) interface{}
	VM(vm *VM) *VM
}

type Fn struct {
	name           *Symbol
	closes         []interface{}
	restArity      int
	bodies         [MAX_ARITY + 1][]interface{}
	threadBindings PersistentMap
	metadata
}

func (fn *Fn) String() string {
	name := "<anonymous>"
	if fn.name != nil {
		name = fn.name.Name()
	}
	return fmt.Sprintf("#Fn[%s]", name)
}

func (fn *Fn) ArityCheck(given int) (isRestCall bool) {
	if given < len(fn.bodies) && fn.bodies[given] != nil {
		return fn.restArity > 0 && given >= fn.restArity
	}
	if fn.restArity > 0 && given >= fn.restArity-1 {
		// If there was a fixed arity at restArity-1, it was returned above
		return true
	}
	panic(fn.arityError(given))
}

func (fn *Fn) arityError(given int) error {
	var sb strings.Builder
	comma := false
	for i, b := range fn.bodies {
		if b != nil {
			if comma {
				sb.WriteByte(',')
			} else {
				comma = true
			}
			if fn.restArity > 0 && i == fn.restArity {
				if fn.bodies[i-1] == nil {
					sb.WriteString(fmt.Sprint(i-1, "+"))
				} else {
					sb.WriteString(fmt.Sprint(i, "+"))
				}
			} else {
				sb.WriteString(fmt.Sprint(i))
			}
		}
	}
	expected := sb.String()
	if expected == "" {
		expected = "no valid arities!"
	}
	return &arityError{given, expected}
}

func (fn *Fn) bodyAndParams(params Seq) (body []interface{}, adjustedParams []interface{}) {
	isRestCall := fn.ArityCheck(RT.Count(params))
	adjustedParams = []interface{}{fn}
	if isRestCall {
		body = fn.bodies[fn.restArity]
		for i := 0; i < fn.restArity-1; i++ {
			adjustedParams = append(adjustedParams, params.First())
			params = params.Next()
		}
		adjustedParams = append(adjustedParams, params)
	} else {
		body = fn.bodies[RT.Count(params)]
		for ; params != nil; params = params.Next() {
			adjustedParams = append(adjustedParams, params.First())
		}
	}
	return
}

func (fn *Fn) VM(vm *VM) *VM {
	if fn.threadBindings != nil {

		return wrapThreadBindings(vm, fn.threadBindings)
	}
	return vm
}

func (fn *Fn) Invoke(vm *VM, args Seq) interface{} {
	body, params := fn.bodyAndParams(args)
	ev := newEvaler(fn.VM(vm))
	ev.code = body
	ev.params.pushN(params)
	ev.closes = fn.closes
	defer ev.release()
	return ev.run()
}

type ReflectiveFn struct {
	arity  int
	name   string
	passVM bool
	fn     reflect.Value
}

func mkReflectiveFn(name string, fn reflect.Value) *ReflectiveFn {
	typ := fn.Type()
	arity := typ.NumIn()
	passVM := false
	if arity > 0 && typ.In(0) == vmType {
		arity--
		passVM = true
	}
	if typ.IsVariadic() {
		arity = -arity
	}
	return &ReflectiveFn{arity, name, passVM, fn}
}

func (fn *ReflectiveFn) FixArity() int {
	if fn.arity < 0 {
		return -fn.arity - 1
	}
	return fn.arity
}

func (fn *ReflectiveFn) ArgType(i int) reflect.Type {
	ftype := fn.fn.Type()
	if i < fn.FixArity() {
		if fn.passVM {
			return ftype.In(i + 1)
		}
		return ftype.In(i)
	}
	return ftype.In(ftype.NumIn() - 1).Elem()
}

func (fn *ReflectiveFn) String() string {
	return fmt.Sprintf("#ReflectiveFn[%s %T]", fn.name, fn.fn.Interface())
}

func arityCheck(arity, given int) {
	if arity < 0 {
		if given < -arity-1 {
			panic(&arityError{given, fmt.Sprint(-arity-1, "+")})
		}
	} else if arity != given {
		panic(&arityError{given, fmt.Sprint(arity)})
	}
}

func (fn *ReflectiveFn) Invoke(vm *VM, args Seq) interface{} {
	arityCheck(fn.arity, RT.Count(args))
	values := make([]reflect.Value, 0, RT.Count(args)+1)
	if fn.passVM {
		values = append(values, reflect.ValueOf(vm))
	}
	for i := 0; args != nil; i, args = i+1, args.Next() {
		values = append(values, massagedValueOf(fn.ArgType(i), args.First()))
	}
	return convertReflectResults(fn.fn.Call(values))
}

func (fn *ReflectiveFn) VM(vm *VM) *VM {
	return vm
}

type Func0 = func(vm *VM) interface{}
type Func1 = func(vm *VM, a interface{}) interface{}
type Func2 = func(vm *VM, a, b interface{}) interface{}
type Func3 = func(vm *VM, a, b, c interface{}) interface{}
type Func4 = func(vm *VM, a, b, c, d interface{}) interface{}
type Func5 = func(vm *VM, a, b, c, d, e interface{}) interface{}
type Func6 = func(vm *VM, a, b, c, d, e, f interface{}) interface{}
type Func7 = func(vm *VM, a, b, c, d, e, f, g interface{}) interface{}
type Func8 = func(vm *VM, a, b, c, d, e, f, g, h interface{}) interface{}
type Func9 = func(vm *VM, a, b, c, d, e, f, g, h, i interface{}) interface{}
type Func10 = func(vm *VM, a, b, c, d, e, f, g, h, i, j interface{}) interface{}

type CompiledFn struct {
	name      *Symbol
	closes    []interface{}
	restArity int
	vm        *VM
	method0   Func0
	method1   Func1
	method2   Func2
	method3   Func3
	method4   Func4
	method5   Func5
	method6   Func6
	method7   Func7
	method8   Func8
	method9   Func9
	method10  Func10
	metadata
}
