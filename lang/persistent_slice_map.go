package lang

import (
	"fmt"
	"strings"
)

type persistentSliceMap struct {
	keys   []interface{}
	values []interface{}
	metadata
	unorderedHashEqCache
}

var (
	EMPTY_MAP *persistentSliceMap = &persistentSliceMap{}
)

func NewPersistentMap(keyVals ...interface{}) *persistentSliceMap {
	if len(keyVals) == 0 {
		return EMPTY_MAP
	}
	if len(keyVals)%2 != 0 {
		panic(fmt.Errorf("NewPersistantMap called with mismatched key/value pairs"))
	}
	keys := make([]interface{}, len(keyVals)/2)
	values := make([]interface{}, len(keyVals)/2)
	for i := 0; i < len(keyVals); i += 2 {
		keys[i/2] = keyVals[i]
		values[i/2] = keyVals[i+1]
	}
	for i, k0 := range keys {
		for _, k1 := range keys[i+1:] {
			if RT.Equiv(k0, k1) {
				panic(fmt.Errorf("NewPersistentMap called with duplicate key: %v", k0))
			}
		}
	}
	return &persistentSliceMap{keys: keys, values: values}
}

func (pm *persistentSliceMap) Conj(x interface{}) PersistentCollection {
	switch x := x.(type) {
	case MapEntry:
		return pm.Assoc(x.Key(), x.Value())
	case PersistentVector:
		if x.Count() != 2 {
			panic(fmt.Errorf("Vector arg to Conj must be pair"))
		}
		return pm.Assoc(x.Nth(0, nil), x.Nth(1, nil))
	default:
		ret := PersistentMap(pm)
		for elms := RT.Seq(x); elms != nil; elms = elms.Next() {
			e := elms.First().(MapEntry)
			ret = ret.AssocM(e.Key(), e.Value())
		}
		return ret
	}
}

func (pm *persistentSliceMap) AssocM(key, value interface{}) PersistentMap {
	var keyIdx int
	for keyIdx = 0; keyIdx < len(pm.values); keyIdx++ {
		k := pm.keys[keyIdx]
		if RT.Equiv(k, key) {
			break
		}
	}
	if keyIdx < len(pm.keys) {
		if RT.Identical(pm.values[keyIdx], value) {
			return pm
		}
		newValues := make([]interface{}, len(pm.values))
		copy(newValues, pm.values)
		newValues[keyIdx] = value
		return &persistentSliceMap{keys: pm.keys, values: newValues, metadata: pm.metadata}
	}
	newKeys := make([]interface{}, len(pm.keys)+1)
	newValues := make([]interface{}, len(pm.values)+1)
	copy(newKeys, pm.keys)
	copy(newValues, pm.values)
	newKeys[len(pm.keys)] = key
	newValues[len(pm.values)] = value
	return &persistentSliceMap{keys: newKeys, values: newValues, metadata: pm.metadata}
}

func (pm *persistentSliceMap) Assoc(key, value interface{}) Associative {
	return pm.AssocM(key, value)
}

func (pm *persistentSliceMap) Dissoc(key interface{}) PersistentMap {
	for ki, k := range pm.keys {
		if RT.Equiv(k, key) {
			return &persistentSliceMap{
				keys:   dropElem(pm.keys, ki),
				values: dropElem(pm.values, ki),
			}
		}
	}
	return pm
}

func dropElem(els []interface{}, idx int) []interface{} {
	newEls := make([]interface{}, len(els)-1)
	copy(newEls, els[0:idx])
	if idx == len(els)-1 {
		return newEls
	}
	copy(newEls[idx:], els[idx+1:])
	return newEls
}

func (pm *persistentSliceMap) Count() int {
	return len(pm.keys)
}

func (pm *persistentSliceMap) FastCount() int {
	return len(pm.keys)
}

func (pm *persistentSliceMap) EntryAt(key interface{}) MapEntry {
	ix := indexOf(pm.keys, key)
	if ix >= 0 {
		return &mapEntry{key: pm.keys[ix], val: pm.values[ix]}
	}
	return nil
}

func (pm *persistentSliceMap) ValAt(key, notfound interface{}) interface{} {
	ix := indexOf(pm.keys, key)
	if ix >= 0 {
		return pm.values[ix]
	}
	return notfound
}

func (pm *persistentSliceMap) ContainsKey(key interface{}) bool {
	return indexOf(pm.keys, key) >= 0
}

func indexOf(keys []interface{}, key interface{}) int {
	for i, k := range keys {
		if RT.Equiv(k, key) {
			return i
		}
	}
	return -1
}

func (pm *persistentSliceMap) Seq() Seq {
	if len(pm.keys) == 0 {
		return nil
	}
	return newPersistentSliceMapSeq(pm, 0)
}

func (pm *persistentSliceMap) Keys() Seq {
	if len(pm.keys) == 0 {
		return nil
	}
	return newPersistentSliceMapKeySeq(pm, 0)
}

func (pm *persistentSliceMap) Values() Seq {
	if len(pm.keys) == 0 {
		return nil
	}
	return newPersistentSliceMapValueSeq(pm, 0)
}

func (pm *persistentSliceMap) WithMeta(newMeta PersistentMap) IObj {
	if pm.meta == newMeta {
		return pm
	}
	newPM := *pm
	newPM.metadata = metadata{newMeta}
	return &newPM
}

func (pm *persistentSliceMap) Equiv(other interface{}) bool {
	if other, ok := other.(PersistentMap); ok {
		if pm == other {
			return true
		}
		if pm.Count() != other.Count() {
			return false
		}
		notfound := new(int)
		for s := pm.Seq(); s != nil; s = s.Next() {
			entry := s.First()
			val := other.ValAt(RT.First(entry), notfound)
			if val == notfound || !RT.Equiv(RT.Second(entry), val) {
				return false
			}
		}
		return true
	}
	return false
}

func (pm *persistentSliceMap) String() string {
	var sb strings.Builder
	sb.WriteString("{")
	for ki, k := range pm.keys {
		sb.WriteString(RT.Print(k))
		sb.WriteString(" ")
		sb.WriteString(RT.Print(pm.values[ki]))
		if ki < len(pm.keys)-1 {
			sb.WriteString(", ")
		}
	}
	sb.WriteString("}")
	return sb.String()
}

func newPersistentSliceMapSeq(pm *persistentSliceMap, i int) *aseqFastCount {
	return &aseqFastCount{aseq{
		first: func() interface{} {
			return &mapEntry{key: pm.keys[i], val: pm.values[i]}
		},
		next: func() Seq {
			if i < pm.Count()-1 {
				return newPersistentSliceMapSeq(pm, i+1)
			}
			return nil
		},
		count: func() int {
			return pm.Count() - i
		},
	}}
}

func newPersistentSliceMapKeySeq(pm *persistentSliceMap, i int) *aseqFastCount {
	return &aseqFastCount{aseq{
		first: func() interface{} {
			return pm.keys[i]
		},
		next: func() Seq {
			if i < pm.Count()-1 {
				return newPersistentSliceMapKeySeq(pm, i+1)
			}
			return nil
		},
		count: func() int {
			return pm.Count() - i
		},
	}}
}

func newPersistentSliceMapValueSeq(pm *persistentSliceMap, i int) *aseqFastCount {
	return &aseqFastCount{aseq{
		first: func() interface{} {
			return pm.values[i]
		},
		next: func() Seq {
			if i < pm.Count()-1 {
				return newPersistentSliceMapValueSeq(pm, i+1)
			}
			return nil
		},
		count: func() int {
			return pm.Count() - i
		},
	}}
}

func (pm *persistentSliceMap) HashEq() uint32 {
	return pm.unorderedHashEq(pm)
}

//// IFn ////

func (pm *persistentSliceMap) Invoke(vm *VM, args Seq) interface{} {
	switch RT.Count(args) {
	case 1:
		return pm.ValAt(args.First(), nil)
	case 2:
		return pm.ValAt(args.First(), RT.Second(args))
	default:
		panic(&arityError{RT.Count(args), "1,2"})
	}
}

func (pm *persistentSliceMap) VM(vm *VM) *VM {
	return vm
}

func (pm *persistentSliceMap) AsTransient() TransientCollection {
	return pm.AsTransientM()
}

func (pm *persistentSliceMap) AsTransientM() TransientMap {
	return newTransientSliceMap(pm)
}

var (
	_ PersistentCollection = &persistentSliceMap{}
	_ Associative          = &persistentSliceMap{}
	_ PersistentMap        = &persistentSliceMap{}
	_ FastCounter          = &persistentSliceMap{}
	_ IMeta                = &persistentSliceMap{}
	_ IObj                 = &persistentSliceMap{}
	_ IFn                  = &persistentSliceMap{}
	_ EditableCollection   = &persistentSliceMap{}
)
