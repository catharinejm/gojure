package lang

type Named interface {
	Namespace() string
	Name() string
}
