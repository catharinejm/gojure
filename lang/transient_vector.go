package lang

import (
	"errors"
	"sync/atomic"
)

type transientVector struct {
	tail []interface{}
	edit int32
}

func newTransientVector(pv *persistentVector) *transientVector {
	newtail := make([]interface{}, len(pv.tail))
	copy(newtail, pv.tail)
	return &transientVector{newtail, 1}
}

func (tv *transientVector) ensureEditable() {
	if atomic.LoadInt32(&tv.edit) == 0 {
		panic(errors.New("Transient used after persistent! call"))
	}
}

func (tv *transientVector) Persistent() PersistentCollection {
	// tv.ensureEditable()
	return tv.PersistentV()
}

func (tv *transientVector) PersistentV() PersistentVector {
	if !atomic.CompareAndSwapInt32(&tv.edit, 1, 0) {
		panic(errors.New("Transient used after persistent! call"))
	}
	return &persistentVector{tail: tv.tail}
}

func (tv *transientVector) Count() int {
	// tv.ensureEditable()
	return tv.FastCount()
}

func (tv *transientVector) FastCount() int {
	tv.ensureEditable()
	return len(tv.tail)
}

func (tv *transientVector) Conj(val interface{}) TransientCollection {
	// tv.ensureEditable()
	return tv.ConjV(val)
}

func (tv *transientVector) ConjV(val interface{}) TransientVector {
	tv.ensureEditable()
	tv.tail = append(tv.tail, val)
	return tv
}

func (tv *transientVector) Assoc(key, val interface{}) TransientAssociative {
	// tv.ensureEditable()
	return tv.AssocV(key, val)
}

func (tv *transientVector) AssocV(key, val interface{}) TransientVector {
	// tv.ensureEditable()
	return tv.AssocN(int(AssertInt(key)), val)
}

func (tv *transientVector) AssocN(i int, val interface{}) TransientVector {
	tv.ensureEditable()
	tv.tail[i] = val
	return tv
}

func (tv *transientVector) Pop() TransientVector {
	tv.ensureEditable()
	if len(tv.tail) == 0 {
		panic(errors.New("Can't pop empty vector"))
	}
	tv.tail[len(tv.tail)-1] = nil
	tv.tail = tv.tail[:len(tv.tail)-1]
	return tv
}

func (tv *transientVector) ContainsKey(key interface{}) bool {
	tv.ensureEditable()
	i, isint := ToNumber(key).(Int)
	return isint && i >= 0 && int(i) < len(tv.tail)
}

func (tv *transientVector) EntryAt(key interface{}) MapEntry {
	tv.ensureEditable()
	if n, isint := ToNumber(key).(Int); isint {
		i := int(n)
		if i >= 0 && i < len(tv.tail) {
			return &mapEntry{key: i, val: tv.tail[i]}
		}
	}
	return nil
}

func (tv *transientVector) ValAt(key, notfound interface{}) interface{} {
	tv.ensureEditable()
	if n, isint := ToNumber(key).(Int); isint {
		return tv.Nth(int(n), notfound)
	}
	return notfound
}

func (tv *transientVector) Nth(i int, notfound interface{}) interface{} {
	tv.ensureEditable()
	if i >= 0 && i < len(tv.tail) {
		return tv.tail[i]
	}
	return notfound
}

var (
	_ TransientCollection  = &transientVector{}
	_ TransientAssociative = &transientVector{}
	_ TransientVector      = &transientVector{}
	_ FastCounter          = &transientVector{}
)
