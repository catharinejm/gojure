package lang

import (
	"sync"
)

type metadata struct {
	meta PersistentMap
}

func (m metadata) Meta() PersistentMap {
	return m.meta
}

type resettableMetadata struct {
	mu   sync.RWMutex
	meta PersistentMap
}

func (m *resettableMetadata) Meta() PersistentMap {
	m.mu.RLock()
	defer m.mu.RUnlock()
	return m.meta
}

func (m *resettableMetadata) ResetMeta(newMeta PersistentMap) PersistentMap {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.meta = newMeta
	return newMeta
}

func (m *resettableMetadata) AlterMeta(vm *VM, alter IFn, args Seq) PersistentMap {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.meta = alter.Invoke(vm, args).(PersistentMap)
	return m.meta
}

type IMeta interface {
	Meta() PersistentMap
}

type IObj interface {
	IMeta
	WithMeta(newMeta PersistentMap) IObj
}

type IReference interface {
	IMeta
	ResetMeta(newMeta PersistentMap) PersistentMap
	AlterMeta(vm *VM, alter IFn, args Seq) PersistentMap
}
