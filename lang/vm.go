package lang

import (
	"io"
	"os"
	"sync"
)

var (
	vminit sync.Once
)

type VM struct {
	tcid     uint64
	bindings PersistentMap
	prev     *VM
}

func FreshVM() *VM {
	return &VM{nextThreadContextID(), EMPTY_MAP, nil}
}

func Eval(vm *VM, form interface{}) interface{} {
	return Compile(vm, form).Invoke(vm, nil)
}

func Compile(vm *VM, form interface{}) IFn {
	return withByteCompiler(vm, func(bc *ByteCompiler) interface{} {
		return bc.Compile(form)
	}).(IFn)
}

func LoadFile(vm *VM, filename string) interface{} {
	f, err := os.Open(filename)
	defer f.Close()
	if err != nil {
		panic(err)
	}
	return LoadReader(vm, f)
}

func LoadReader(vm *VM, rdr interface{}) interface{} {
	var lispreader *LispReader
	switch rdr := rdr.(type) {
	case *LispReader:
		lispreader = rdr
	case *os.File:
		lispreader = NewLispReader(vm, rdr, rdr.Name())
	default:
		lispreader = NewLispReader(vm, rdr.(io.Reader), "<NO FILE>")
	}
	var result interface{}
	for form := lispreader.Read(); form != io.EOF; form = lispreader.Read() {
		// fmt.Fprintf(os.Stderr, "form: %v\n", form)
		fn := Compile(vm, form)
		// fmt.Fprintf(os.Stderr, "fn: %#v\n", fn)
		result = fn.Invoke(vm, nil)
	}
	return result
}

func InNamespace(vm *VM, sym *Symbol) *Namespace {
	ns := FindOrCreateNamespace(sym)
	vCURRENT_NS.Set(vm, ns)
	return ns
}

func (vm *VM) fork() *VM {
	if vm == nil {
		return FreshVM()
	}
	return &VM{nextThreadContextID(), vm.bindings, nil}
}
