package lang

import "reflect"

var (
	tSLICE *Type
	tCHAN  *Type
)

func initDefaults() {
	builtin := NewGoPackage("builtin")
	var bindings PersistentMap = NewPersistentMap(
		ParseSymbol("bool"), NewImport(builtin, ParseSymbol("bool"),
			NewType(reflect.TypeOf(false),
				func(x interface{}) bool { _, isbool := x.(bool); return isbool },
				func(x interface{}) interface{} {
					ret := make([]bool, 0, RT.Count(x))
					for s := RT.Seq(x); s != nil; s = s.Next() {
						ret = append(ret, s.First().(bool))
					}
					return ret
				}, nil)),
		ParseSymbol("rune"), NewImport(builtin, ParseSymbol("rune"),
			NewType(reflect.TypeOf(rune(0)),
				func(x interface{}) bool { _, isrune := x.(rune); return isrune },
				func(x interface{}) interface{} {
					ret := make([]rune, 0, RT.Count(x))
					for s := RT.Seq(x); s != nil; s = s.Next() {
						ret = append(ret, s.First().(rune))
					}
					return ret
				}, nil)),
		ParseSymbol("int64"), NewImport(builtin, ParseSymbol("int64"),
			NewType(reflect.TypeOf(int64(0)),
				func(x interface{}) bool { _, isint64 := x.(int64); return isint64 },
				func(x interface{}) interface{} {
					ret := make([]int64, 0, RT.Count(x))
					for s := RT.Seq(x); s != nil; s = s.Next() {
						ret = append(ret, s.First().(Int).Int64())
					}
					return ret
				}, nil)),
		ParseSymbol("float64"), NewImport(builtin, ParseSymbol("float64"),
			NewType(reflect.TypeOf(float64(0)),
				func(x interface{}) bool { _, isfloat64 := x.(float64); return isfloat64 },
				func(x interface{}) interface{} {
					ret := make([]float64, 0, RT.Count(x))
					for s := RT.Seq(x); s != nil; s = s.Next() {
						ret = append(ret, s.First().(Float).Float64())
					}
					return ret
				}, nil)),
		ParseSymbol("string"), NewImport(builtin, ParseSymbol("string"),
			NewType(reflect.TypeOf(""),
				func(x interface{}) bool { _, isstring := x.(string); return isstring },
				func(x interface{}) interface{} {
					ret := make([]string, 0, RT.Count(x))
					for s := RT.Seq(x); s != nil; s = s.Next() {
						ret = append(ret, s.First().(string))
					}
					return ret
				}, nil)),
		ParseSymbol("error"), NewImport(builtin, ParseSymbol("error"),
			NewType(reflect.TypeOf(""),
				func(x interface{}) bool { _, iserror := x.(error); return iserror },
				func(x interface{}) interface{} {
					ret := make([]error, 0, RT.Count(x))
					for s := RT.Seq(x); s != nil; s = s.Next() {
						ret = append(ret, s.First().(error))
					}
					return ret
				}, nil)),
	)
	tSLICE = NewType(reflect.TypeOf(([]interface{})(nil)),
		func(x interface{}) bool { _, isslice := x.([]interface{}); return isslice },
		func(x interface{}) interface{} {
			ret := make([][]interface{}, 0, RT.Count(x))
			for s := RT.Seq(x); s != nil; s = s.Next() {
				ret = append(ret, RT.ToSlice(s.First()))
			}
			return ret
		}, nil)
	tCHAN = NewType(reflect.TypeOf((chan interface{})(nil)),
		func(x interface{}) bool { _, ischan := x.(chan interface{}); return ischan },
		func(x interface{}) interface{} {
			ret := make([]chan interface{}, 0, RT.Count(x))
			for s := RT.Seq(x); s != nil; s = s.Next() {
				ret = append(ret, s.First().(chan interface{}))
			}
			return ret
		}, nil)
	bindings = bindings.AssocM(
		ParseSymbol("slice"), NewImport(builtin, ParseSymbol("slice"), tSLICE)).AssocM(
		ParseSymbol("chan"), NewImport(builtin, ParseSymbol("chan"), tCHAN))

	InternGoPackage(builtin, ParseSymbol("builtin"), bindings)
	for bs := bindings.Seq(); bs != nil; bs = bs.Next() {
		t := bs.First().(MapEntry).Value().(*Import).Object().(*Type)
		CacheType(t, nil, nil)
	}
}
