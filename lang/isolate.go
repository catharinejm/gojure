package lang

import (
	"errors"
	"fmt"
	"sync/atomic"
)

type Isolate struct {
	tcid  uint64
	value interface{}
}

// IsolateValue returns an isolated value box for safe mutable containment. It
// is intended to be used only on newly constructed objects. The caller is
// responsible for ensuring no other references to the value exist before
// calling.
func IsolateValue(vm *VM, value interface{}) *Isolate {
	if vm == nil {
		panic(errors.New("Can't isolate a value outside of a thread context"))
	}
	return &Isolate{vm.tcid, value}
}

// Alter takes a fn and passes the value to it along with any args. The value is
// assumed to be mutated in place by the given fn. Any value returned by the
// given fn is discarded. Alter returns its receiver.
func (iso *Isolate) Alter(vm *VM, fn IFn, args Seq) *Isolate {
	if vm == nil || vm.tcid != atomic.LoadUint64(&iso.tcid) {
		panic(errors.New("Can't modify isolate outide of owning thread"))
	}
	fn.Invoke(vm, RT.Cons(iso.value, args))
	return iso
}

func (iso *Isolate) Release(vm *VM) *Isolate {
	if vm == nil || !atomic.CompareAndSwapUint64(&iso.tcid, vm.tcid, 0) {
		panic(errors.New("Can't release isolate outside of owning thread"))
	}
	return iso
}

func (iso *Isolate) Claim(vm *VM) *Isolate {
	if vm == nil {
		panic(errors.New("Can't claim an isolate outisde of a thread context"))
	}
	if !atomic.CompareAndSwapUint64(&iso.tcid, 0, vm.tcid) {
		panic(errors.New("Can't claim owned isolate"))
	}
	return iso
}

func (iso *Isolate) Consume(vm *VM) interface{} {
	if vm == nil || !atomic.CompareAndSwapUint64(&iso.tcid, vm.tcid, ^uint64(0)) {
		panic(errors.New("Can't consume isolate outside of owning thread"))
	}
	ret := iso.value
	iso.value = nil
	return ret
}

func (iso *Isolate) Refill(vm *VM, value interface{}) *Isolate {
	if vm == nil {
		panic(errors.New("Can't fill an isolate outisde of a thread context"))
	}
	if !atomic.CompareAndSwapUint64(&iso.tcid, ^uint64(0), vm.tcid) {
		panic(errors.New("Can't fill a filled isolate"))
	}
	iso.value = value
	return iso
}

func (iso *Isolate) String() string {
	return fmt.Sprintf("#iso[%v]", iso.value)
}
