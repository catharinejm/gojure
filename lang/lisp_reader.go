package lang

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"math/big"
	"strings"
	"unicode"
)

type LispReader struct {
	vmHolder
	rdr      *bufio.Reader
	filename string
	line     int
	col      int
	unread   bool
	eof      bool
}

var (
	vARG_ENV = anonymousVarWithRoot(nil).SetDynamic()
)

type readerError struct {
	err  error
	file string
	line int
	col  int
}

func (re *readerError) Error() string {
	return fmt.Sprintf("%s:%d:%d: READ ERROR: %s", re.file, re.line, re.col, re.err)
}

func NewLispReader(vm *VM, r io.Reader, filename string) *LispReader {
	return &LispReader{
		vmHolder: vmHolder{vm},
		rdr:      bufio.NewReader(r),
		filename: filename,
		line:     1,
		col:      0,
		unread:   false,
		eof:      false,
	}
}

func ReadString(vm *VM, s string) interface{} {
	return NewLispReader(vm, strings.NewReader(s), "<NO FILE>").Read()
}

func (lr *LispReader) Read() interface{} {
	var o interface{} = lr
	for o == lr {
		o = lr.read(false)
	}
	return o
}

func (lr *LispReader) readError(sourceErr interface{}) *readerError {
	err := toError(sourceErr)
	return &readerError{err, lr.filename, lr.line, lr.col}
}

func (lr *LispReader) eofError() *readerError {
	return lr.readError("premature EOF")
}

func (lr *LispReader) delimError(r rune) *readerError {
	return lr.readError(fmt.Errorf("unmatched delimiter: %c", r))
}

func (lr *LispReader) readRune(eofIsError bool) rune {
	r, _, err := lr.rdr.ReadRune()
	if !lr.unread {
		if r == '\n' {
			lr.line++
			lr.col = 0
		} else if err == nil {
			lr.col++
		}
	} else {
		lr.unread = false
	}
	if err == io.EOF {
		lr.eof = true
		if eofIsError {
			panic(lr.eofError())
		}
		return -1
	}
	if err != nil {
		panic(lr.readError(err))
	}
	return r
}

func (lr *LispReader) unreadRune() {
	if lr.eof {
		return
	}
	lr.unread = true
	if err := lr.rdr.UnreadRune(); err != nil {
		panic(err)
	}
}

func (lr *LispReader) consumeLine() {
	_, err := lr.rdr.ReadBytes('\n')
	if err != nil && err != io.EOF {
		panic(lr.readError(err))
	}
	lr.line++
	lr.col = 0
}

func (lr *LispReader) consumeWhitespace(eofIsError bool) rune {
	r := lr.readRune(eofIsError)
	for isWhitespace(r) && r != -1 {
		r = lr.readRune(eofIsError)
	}
	return r
}

func (lr *LispReader) demand() interface{} {
	var o interface{} = lr
	for o == lr {
		o = lr.read(true)
	}
	return o
}

func (lr *LispReader) read(eofIsError bool) interface{} {
	r := lr.consumeWhitespace(eofIsError)
	if r == -1 {
		return io.EOF
	}
	switch r {
	case '"':
		return lr.readString()
	case ';':
		lr.consumeLine()
		return lr
	case '\'':
		return NewPersistentList(sQUOTE, lr.demand())
	case '@':
		return NewPersistentList(sDEREF, lr.demand())
	case '^':
		return lr.readMeta()
	case '`':
		return lr.readSyntaxQuote()
	case '~':
		return lr.readUnquote()
	case '(':
		return lr.readList()
	case '[':
		return lr.readVector()
	case '{':
		return lr.readMap()
	case ')', ']', '}':
		panic(lr.delimError(r))
	case '\\':
		return lr.readChar()
	case '%':
		return lr.readArg()
	case '#':
		r := lr.consumeWhitespace(true)
		switch r {
		case '^':
			return lr.readMeta()
		case '#':
			return lr.readSymbolicValue()
		case '\'':
			return NewPersistentList(sVAR, lr.demand())
		case '"':
			panic(lr.readError("regex syntax (#\"\") not supported"))
		case '(':
			return lr.readFn()
		case '{':
			return lr.readSet()
		case '=':
			panic(lr.readError("eval reader syntax (#=) not supported"))
		case '!':
			lr.consumeLine()
			return lr
		case '_':
			lr.read(true)
			return lr
		case '?':
			panic(lr.readError("conditional reader syntax (#?) not supported"))
		case ':':
			panic(lr.readError("namespace map syntax (#:) not supported"))
		default:
			panic(lr.readError(fmt.Errorf("no dispatch macro for: %c", r)))
		}
	case '+', '-':
		r2 := lr.readRune(false)
		lr.unreadRune()
		if unicode.IsDigit(r2) {
			return lr.readNumber(r)
		}
		return lr.readToken(r)
	default:
		if unicode.IsDigit(r) {
			return lr.readNumber(r)
		}
		return lr.readToken(r)
	}
}

func isWhitespace(r rune) bool {
	return unicode.IsSpace(r) || r == ','
}

func (lr *LispReader) readString() string {
	var sb strings.Builder
	for r := lr.readRune(true); r != '"'; r = lr.readRune(true) {
		if r == '\\' {
			r = lr.readRune(true)
			switch r {
			case 't':
				r = '\t'
			case 'r':
				r = '\r'
			case 'n':
				r = '\n'
			case 'b':
				r = '\b'
			case 'f':
				r = '\f'
			case '\\', '"': // literal, no conversion needed
			default:
				// TODO: support unicode escapes
				panic(lr.readError(fmt.Errorf("invalid escape character: \\%c", r)))
			}
		}
		sb.WriteRune(r)
	}
	return sb.String()
}

func (lr *LispReader) readChar() Rune {
	var sb strings.Builder
	r := lr.readRune(true)
	if isTokenEnd(r) {
		return Rune(r)
	}
	lr.unreadRune()
	lr.readToTokenEnd(&sb)
	str := sb.String()
	var ix int
	for ix, r = range str {
		if ix > 0 {
			break
		}
	}
	if ix == 0 {
		return Rune(r)
	}
	switch str {
	case "newline":
		return Rune('\n')
	case "space":
		return Rune(' ')
	case "tab":
		return Rune('\t')
	case "backspace":
		return Rune('\b')
	case "formfeed":
		return Rune('\f')
	case "return":
		return Rune('\r')
	default:
		panic(lr.readError(fmt.Errorf("unsupported character: \\%s", str)))
	}
}

func (lr *LispReader) readList() PersistentList {
	line := lr.line
	col := lr.col
	forms := lr.readDelimited(')')
	return NewPersistentList(forms...).WithMeta(
		NewPersistentMap(kFILE, lr.filename, kLINE, line, kCOLUMN, col),
	).(PersistentList)
}

func (lr *LispReader) readVector() PersistentVector {
	forms := lr.readDelimited(']')
	return PersistentVectorBuilder.CreateOwning(forms...)
}

func (lr *LispReader) readMap() PersistentMap {
	forms := lr.readDelimited('}')
	if len(forms)%2 != 0 {
		panic(lr.readError("map literal has odd number of forms"))
	}
	return NewPersistentMap(forms...)
}

func (lr *LispReader) readSet() PersistentSet {
	forms := lr.readDelimited('}')
	return NewPersistentSet(forms...)
}

func (lr *LispReader) readDelimited(closer rune) []interface{} {
	forms := make([]interface{}, 0, 5)
	for {
		r := lr.readRune(true)
		for isWhitespace(r) {
			r = lr.readRune(true)
		}
		if r == closer {
			return forms
		}
		lr.unreadRune()
		if form := lr.read(true); form != lr {
			forms = append(forms, form)
		}
	}
}

func isTokenEnd(r rune) bool {
	return isWhitespace(r) || r == '(' || r == ')' || r == '[' || r == ']' || r == '{' || r == '}' || r == ';' || r == -1
}

func (lr *LispReader) readToTokenEnd(sb *strings.Builder) {
	r := lr.readRune(false)
	for !isTokenEnd(r) {
		sb.WriteRune(r)
		r = lr.readRune(false)
	}
	lr.unreadRune()
}

func (lr *LispReader) readNumber(r rune) interface{} {
	var sb strings.Builder
	sb.WriteRune(r)
	lr.readToTokenEnd(&sb)
	str := sb.String()
	var bigint big.Int
	if _, ok := bigint.SetString(str, 0); ok {
		if bigint.IsInt64() {
			return Int(bigint.Int64())
		}
		panic(fmt.Errorf("numeric constant overflows: %s", str))
	}
	var bigfloat big.Float
	if _, ok := bigfloat.SetString(str); ok {
		f, _ := bigfloat.Float64()
		return Float(f)
	}
	panic(lr.readError(fmt.Errorf("malformed numeric constant: %s", str)))
}

func (lr *LispReader) readToken(r rune) interface{} {
	var sb strings.Builder
	sb.WriteRune(r)
	lr.readToTokenEnd(&sb)
	switch str := sb.String(); str {
	case "true":
		return true
	case "false":
		return false
	case "nil":
		return nil
	default:
		sym := ParseSymbol(str)
		switch {
		case strings.HasPrefix(sym.Namespace(), "::") ||
			sym.Namespace() != "" && strings.HasPrefix(sym.Name(), ":") ||
			sym.Namespace() == "" && strings.HasPrefix(sym.Name(), ":::"):
			panic(lr.readError(fmt.Errorf("invalid token: %s", str)))
		case strings.HasPrefix(sym.Namespace(), ":"):
			return NewKeyword(ParseSymbol(sym.String()[1:]))
		case strings.HasPrefix(sym.Name(), "::"):
			return NewKeyword(NewSymbol(lr.vm.CurrentNS().name.String(), sym.Name()[2:]))
		case strings.HasPrefix(sym.Name(), ":"):
			return NewKeyword(ParseSymbol(sym.Name()[1:]))
		default:
			return sym
		}
	}
}

func (lr *LispReader) readMeta() interface{} {
	line := lr.line
	col := lr.col

	meta := lr.demand()
	form := lr.demand()
	switch m := meta.(type) {
	case *Symbol, string:
		meta = NewPersistentMap(kTAG, m)
	case *Keyword:
		meta = NewPersistentMap(m, true)
	}
	if meta, ok := meta.(PersistentMap); ok {
		if form, ok := form.(IObj); ok {
			if _, ok := form.(Seq); ok {
				meta = meta.AssocM(kFILE, meta.ValAt(kFILE, lr.filename))
				meta = meta.AssocM(kLINE, meta.ValAt(kLINE, line))
				meta = meta.AssocM(kCOLUMN, meta.ValAt(kCOLUMN, col))
			}
			// if form, ok := form.(IReference); ok {
			// 	form.ResetMeta(meta)
			// 	return form
			// }
			formMeta := form.Meta()
			for s := meta.Seq(); s != nil; s = s.Next() {
				entry := s.First().(MapEntry)
				formMeta = RT.AssocM(formMeta, entry.Key(), entry.Value())
			}
			return form.WithMeta(formMeta)
		}
		panic(lr.readError("Metadata can only be applied to IMetas"))
	}
	panic(lr.readError("Metadata must be Symbol, Keyword, String or Map"))
}

func (lr *LispReader) readSyntaxQuote() interface{} {
	val := lr.demand()
	return NewPersistentList(sSYNTAX_QUOTE, val)
}

func (lr *LispReader) readUnquote() interface{} {
	r := lr.readRune(true)
	if r == '@' {
		return NewPersistentList(sUNQUOTE_SPLICING, lr.demand())
	}
	lr.unreadRune()
	return NewPersistentList(sUNQUOTE, lr.demand())
}

func (lr *LispReader) readSymbolicValue() interface{} {
	switch tok := lr.demand().(type) {
	case *Symbol:
		switch sym := tok.String(); sym {
		case "Inf":
			return math.Inf(1)
		case "-Inf":
			return math.Inf(-1)
		case "NaN":
			return math.NaN()
		default:
			panic(lr.readError(fmt.Errorf("Unknown symbolic value: ##%s", sym)))
		}
	default:
		panic(lr.readError(fmt.Errorf("Invalid token: ##%v", tok)))
	}

}

func (lr *LispReader) argEnv() PersistentMap {
	m, _ := vARG_ENV.Deref(lr.vm).(PersistentMap)
	return m
}

func (lr *LispReader) readArg() interface{} {
	if lr.argEnv() == nil {
		return lr.readToken('%')
	}
	r := lr.readRune(false)
	lr.unreadRune()
	if isTokenEnd(r) {
		return lr.registerArg(1)
	}
	n := lr.demand()
	if RT.Equiv(n, sAMP) {
		return lr.registerArg(-1)
	}
	num, _ := n.(Int)
	if num < 1 {
		panic(lr.readError("arg literal must be %, %& or %positive-integer"))
	}
	return lr.registerArg(int(num))
}

func (lr *LispReader) registerArg(num int) interface{} {
	argsyms := lr.argEnv()
	if argsyms == nil {
		panic(lr.readError("arg literal not in #()"))
	}
	ret := argsyms.ValAt(num, nil)
	if ret == nil {
		ret = garg(num)
		vARG_ENV.Set(lr.vm, argsyms.Assoc(num, ret))
	}
	return ret
}

func garg(num int) *Symbol {
	var sb strings.Builder
	if num == -1 {
		sb.WriteString("rest")
	} else {
		sb.WriteString(fmt.Sprintf("p%d", num))
	}
	sb.WriteString(fmt.Sprintf("__%d#", RT.NextID()))
	return NewSymbol("", sb.String())
}

func (lr *LispReader) readFn() interface{} {
	if lr.argEnv() != nil {
		panic(lr.readError("Nested #()s are not allowed"))
	}
	lr.PushThreadBindings(NewPersistentMap(
		vARG_ENV, EMPTY_MAP,
	))
	defer lr.PopThreadBindings()
	body := lr.readList()
	argsyms := lr.argEnv()
	argvec := PersistentVector(EMPTY_VECTOR)
	if argsyms.Count() > 0 {
		higharg := 1
		for seq := argsyms.Seq(); seq != nil; seq = seq.Next() {
			anum := RT.First(seq.First()).(int)
			if anum >= higharg {
				for i := higharg; i <= anum; i++ {
					sym := argsyms.ValAt(i, nil)
					if sym == nil {
						sym = garg(i)
					}
					argvec = argvec.ConjV(sym)
				}
				higharg = anum + 1
			}
		}
		if restsym := argsyms.ValAt(-1, nil); restsym != nil {
			argvec = argvec.ConjV(sAMP).ConjV(restsym)
		}
	}
	return NewPersistentList(sFN, argvec, body)
}
