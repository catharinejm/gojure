package lang

func newInterfaceSliceSeq(slice []interface{}) *aseqFastCount {
	return &aseqFastCount{aseq{
		first: func() interface{} {
			return slice[0]
		},
		next: func() Seq {
			if len(slice) > 1 {
				return newInterfaceSliceSeq(slice[1:])
			}
			return nil
		},
		count: func() int {
			return len(slice)
		},
	}}
}
