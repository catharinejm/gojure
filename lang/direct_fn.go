package lang

import (
	"fmt"
)

type DirectFn struct {
	name  string
	arity int
	// passVM bool
	recv    *Type
	rettype *Type
	fn      interface{}
}

func NewDirectFn(name string, arity int, recv, rettype *Type, fn interface{}) *DirectFn {
	return &DirectFn{name, arity, recv, rettype, fn}
}

func (fn *DirectFn) Invoke(vm *VM, args Seq) interface{} {
	slice := make([]interface{}, 0, RT.Count(args))
	for ; args != nil; args = args.Next() {
		slice = append(slice, args.First())
	}
	return fn.InvokeDirect(vm, slice...)
}

func (fn *DirectFn) VM(vm *VM) *VM {
	return vm
}

func (fn *DirectFn) AcceptsArity(arity int) bool {
	return fn.arity >= 0 && fn.arity == arity || fn.arity < 0 && -fn.arity-1 <= arity
}

func (fn *DirectFn) ArityString() string {
	if fn.arity >= 0 {
		return fmt.Sprint(fn.arity)
	}
	return fmt.Sprint(-fn.arity-1, "+")
}

func (fn *DirectFn) IsMethodFor(x interface{}) bool {
	return fn.recv != nil && fn.recv.Accepts(x)
}

func (fn *DirectFn) ReturnType() *Type {
	return fn.rettype
}

func (fn *DirectFn) String() string {
	return fmt.Sprintf("#DirectFn[%v]", fn.name)
}

func (fn *DirectFn) InvokeDirect(vm *VM, args ...interface{}) interface{} {
	// fmt.Fprintf(os.Stderr, "InvokeDirect: %v, %v\n", fn.name, args)
	arityCheck(fn.arity, len(args))
	if fn.arity >= 0 {
		switch len(args) {
		case 0:
			return fn.fn.(Func0)(vm)
		case 1:
			return fn.fn.(Func1)(vm, args[0])
		case 2:
			return fn.fn.(Func2)(vm, args[0], args[1])
		case 3:
			return fn.fn.(Func3)(vm, args[0], args[1], args[2])
		case 4:
			return fn.fn.(Func4)(vm, args[0], args[1], args[2], args[3])
		case 5:
			return fn.fn.(Func5)(vm, args[0], args[1], args[2], args[3], args[4])
		case 6:
			return fn.fn.(Func6)(vm, args[0], args[1], args[2], args[3], args[4], args[5])
		case 7:
			return fn.fn.(Func7)(vm, args[0], args[1], args[2], args[3], args[4], args[5], args[6])
		case 8:
			return fn.fn.(Func8)(vm, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7])
		case 9:
			return fn.fn.(Func9)(vm, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8])
		case 10:
			return fn.fn.(Func10)(vm, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9])
		default:
			panic("too many args")
		}
	} else {
		switch -fn.arity {
		case 0:
			panic("0 arity variadic fn")
		case 1:
			return fn.fn.(Func1)(vm, args)
		case 2:
			return fn.fn.(Func2)(vm, args[0], args[1:])
		case 3:
			return fn.fn.(Func3)(vm, args[0], args[1], args[2:])
		case 4:
			return fn.fn.(Func4)(vm, args[0], args[1], args[2], args[3:])
		case 5:
			return fn.fn.(Func5)(vm, args[0], args[1], args[2], args[3], args[4:])
		case 6:
			return fn.fn.(Func6)(vm, args[0], args[1], args[2], args[3], args[4], args[5:])
		case 7:
			return fn.fn.(Func7)(vm, args[0], args[1], args[2], args[3], args[4], args[5], args[6:])
		case 8:
			return fn.fn.(Func8)(vm, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7:])
		case 9:
			return fn.fn.(Func9)(vm, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8:])
		case 10:
			return fn.fn.(Func10)(vm, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9:])
		default:
			panic("too many args")
		}
	}
}

// func (fn *DirectFn) Invoke0(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 0", fn.arity))
// }

// func (fn *DirectFn) Invoke1(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 1", fn.arity))
// }

// func (fn *DirectFn) Invoke2(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 2", fn.arity))
// }

// func (fn *DirectFn) Invoke3(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 3", fn.arity))
// }

// func (fn *DirectFn) Invoke4(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 4", fn.arity))
// }

// func (fn *DirectFn) Invoke5(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 5", fn.arity))
// }

// func (fn *DirectFn) Invoke6(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 6", fn.arity))
// }

// func (fn *DirectFn) Invoke7(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 7", fn.arity))
// }

// func (fn *DirectFn) Invoke8(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 8", fn.arity))
// }

// func (fn *DirectFn) Invoke9(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 9", fn.arity))
// }

// func (fn *DirectFn) Invoke10(vm *VM) interface{} {
// 	panic(fmt.Errorf("invalid arity, expected: %v, got: 10", fn.arity))
// }
