package lang

import (
	"unicode/utf8"
)

func newStringSeq(s string) *aseqFastCount {
	return newStringSeqWithCount(s, utf8.RuneCountInString(s))
}

func newStringSeqWithCount(s string, count int) *aseqFastCount {
	fst, sz := utf8.DecodeRuneInString(s)
	return &aseqFastCount{aseq{
		first: func() interface{} {
			return Rune(fst)
		},
		next: func() Seq {
			if len(s[sz:]) > 0 {
				return newStringSeqWithCount(s[sz:], count-1)
			}
			return nil
		},
		count: func() int {
			return count
		},
	}}
}
