package lang

import (
	"fmt"
	"sync"
)

var (
	pkgcachelock sync.RWMutex
	pkgcache     map[string]*GoPackage = make(map[string]*GoPackage)
)

type GoPackage struct {
	path        string
	defaultName *Symbol
	mappings    PersistentMap
}

func NewGoPackage(path string) *GoPackage {
	return &GoPackage{path: path}
}

func InternGoPackage(pkg *GoPackage, defaultName *Symbol, mappings PersistentMap) *GoPackage {
	pkgcachelock.Lock()
	defer pkgcachelock.Unlock()
	if _, exists := pkgcache[pkg.path]; exists {
		panic(fmt.Errorf("Go package with path %q already exists", pkg.path))
	}
	pkg.defaultName = defaultName.NameSym()
	if mappings == nil {
		mappings = EMPTY_MAP
	}
	pkg.mappings = mappings
	pkgcache[pkg.path] = pkg
	return pkgcache[pkg.path]
}

func FindGoPackage(path string) *GoPackage {
	pkgcachelock.RLock()
	defer pkgcachelock.RUnlock()
	return pkgcache[path]
}

func (gp *GoPackage) GetMappings() PersistentMap {
	return gp.mappings
}

func (gp *GoPackage) Lookup(name *Symbol) *Import {
	return gp.mappings.ValAt(name, (*Import)(nil)).(*Import)
}

func (gp *GoPackage) String() string {
	return fmt.Sprintf("#GoPackage[%v %q]", gp.defaultName, gp.path)
}
