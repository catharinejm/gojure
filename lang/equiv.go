package lang

type Equiv interface {
	Equiv(other interface{}) bool
}
