package lang_test

import (
	"testing"

	. "gitlab.com/catharinejm/gojure/lang"
)

func TestPersistentMap_Assoc(t *testing.T) {
	m := PersistentMap(EMPTY_MAP)
	morig := m
	if m.Count() != 0 {
		t.Errorf("map is not empty")
	}
	m = m.AssocM(NewSymbol("", "foo"), 10)
	if m.ValAt(NewSymbol("", "foo"), nil) != 10 {
		t.Errorf("foo is not ten! (%v)\n", m)
	}
	m = m.AssocM(NewSymbol("", "bar"), 123)
	if m.ValAt(NewSymbol("", "foo"), nil) != 10 {
		t.Errorf("foo is not ten! (%v)\n", m)
	}
	if m.ValAt(NewSymbol("", "bar"), nil) != 123 {
		t.Errorf("bar is not 123! (%v)\n", m)
	}
	m = m.AssocM(NewSymbol("", "foo"), "newval")
	if m.ValAt(NewSymbol("", "foo"), nil) != "newval" {
		t.Errorf("foo is not \"newval\"! (%v)\n", m)
	}
	if morig.Count() != 0 {
		t.Errorf("original map is not empty!")
	}
}

func TestPersistentMap_Dissoc(t *testing.T) {
	m := NewPersistentMap("foo", 10)
	m1 := m.Dissoc("foo")
	if m.Count() == 0 {
		t.Errorf("original map lost its key: %v", m)
	}
	if m1.Count() != 0 {
		t.Errorf("dissoc'd map is not empty: %v", m1)
	}
	_ = m1.AssocM("newkey", 1234)
	if m.Count() != 1 {
		t.Errorf("original map has an extra key: %v", m)
	}
	if !RT.Equiv(m, NewPersistentMap("foo", 10)) {
		t.Errorf("original map has been altered!: %v", m)
	}
	if m1.Count() != 0 {
		t.Errorf("dissoc'd map has an extra key: %v", m1)
	}
}
