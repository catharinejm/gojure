package lang

import (
	"sync/atomic"
	"unsafe"
)

type atomicBox struct {
	val unsafe.Pointer //*interface{}
}

func makeAtomicBox(val interface{}) atomicBox {
	return atomicBox{unsafe.Pointer(&val)}
}

func (ab *atomicBox) Load() interface{} {
	if p := ab.loadPointer(); p != nil {
		return *p
	}
	return nil
}

func (ab *atomicBox) Store(x interface{}) {
	ab.storePointer(&x)
}

func (ab *atomicBox) Swap(f func(old interface{}) interface{}) interface{} {
	for {
		var old interface{}
		oldp := ab.loadPointer()
		if oldp != nil {
			old = *oldp
		}
		new := f(old)
		if ab.cas(oldp, &new) {
			return new
		}
	}
}

// NB. This can have false negatives, but even a proper CAS would require an
// atomic load after a failure to get the current value. I don't believe this
// violates observable atomic behavior.
func (ab *atomicBox) CompareAndSwap(old, new interface{}) bool {
	oldp := ab.loadPointer()
	if oldp == nil {
		if old != nil {
			return false
		}
		return ab.cas(oldp, &new)
	}
	return identical(*oldp, old) && ab.cas(oldp, &new)
}

func (ab *atomicBox) cas(old, new *interface{}) bool {
	return atomic.CompareAndSwapPointer(&ab.val, unsafe.Pointer(old), unsafe.Pointer(new))
}

func (ab *atomicBox) loadPointer() *interface{} {
	return (*interface{})(atomic.LoadPointer(&ab.val))
}

func (ab *atomicBox) storePointer(x *interface{}) {
	atomic.StorePointer(&ab.val, unsafe.Pointer(x))
}
