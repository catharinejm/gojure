package lang

import "fmt"

type MapEntry interface {
	Key() interface{}
	Value() interface{}
}

type mapEntry struct {
	key, val interface{}
	orderedHashEqCache
}

func (e *mapEntry) Key() interface{} {
	return e.key
}

func (e *mapEntry) Value() interface{} {
	return e.val
}

func (e *mapEntry) Count() int {
	return 2
}

func (e *mapEntry) FastCount() int {
	return 2
}

func (e *mapEntry) Nth(i int, notfound interface{}) interface{} {
	switch i {
	case 0:
		return e.Key()
	case 1:
		return e.Value()
	default:
		return notfound
	}
}

func (e *mapEntry) asVector() PersistentVector {
	return PersistentVectorBuilder.CreateOwning(e.Key(), e.Value())
}

func (e *mapEntry) Seq() Seq {
	return e.asVector().Seq()
}

func (e *mapEntry) RSeq() Seq {
	return e.asVector().RSeq()
}

func (e *mapEntry) ConjV(x interface{}) PersistentVector {
	return e.asVector().ConjV(x)
}

func (e *mapEntry) Conj(x interface{}) PersistentCollection {
	return e.asVector().Conj(x)
}

func (e *mapEntry) AssocN(i int, x interface{}) PersistentVector {
	return e.asVector().AssocN(i, x)
}

func (e *mapEntry) AssocV(key, val interface{}) PersistentVector {
	return e.asVector().AssocV(key, val)
}

func (e *mapEntry) Assoc(key, val interface{}) Associative {
	return e.asVector().Assoc(key, val)
}

func (e *mapEntry) ContainsKey(key interface{}) bool {
	i, isint := RT.IntCast(key)
	return isint && (i == 0 || i == 1)
}

func (e *mapEntry) EntryAt(key interface{}) MapEntry {
	return e.asVector().EntryAt(key)
}

func (e *mapEntry) ValAt(key, notfound interface{}) interface{} {
	if i, isint := RT.IntCast(key); isint {
		return e.Nth(i, notfound)
	}
	return notfound
}

func (e *mapEntry) Meta() PersistentMap {
	return nil
}

func (e *mapEntry) Subvec(lo, hi int) PersistentVector {
	return e.asVector().Subvec(lo, hi)
}

func (e *mapEntry) Peek() interface{} {
	return e.Value()
}

func (e *mapEntry) Pop() PersistentStack {
	return PersistentVectorBuilder.CreateOwning(e.Key())
}

func (e *mapEntry) Equiv(other interface{}) bool {
	switch other := other.(type) {
	case MapEntry:
		return RT.Equiv(e.Key(), other.Key()) && RT.Equiv(e.Value(), other.Value())
	case PersistentVector:
		return e.asVector().Equiv(other)
	case Seq:
		return SeqEquiv(e.Seq(), other)
	default:
		return false
	}
}

func (e *mapEntry) HashEq() uint32 {
	return e.orderedHashEq(e)
}

//// IFn ////
func (e *mapEntry) Invoke(vm *VM, args Seq) interface{} {
	return e.asVector().Invoke(vm, args)
}

func (e *mapEntry) VM(vm *VM) *VM {
	return e.asVector().VM(vm)
}

//// Comparable ////
func (e *mapEntry) CompareTo(other interface{}) int {
	return e.asVector().CompareTo(other)
}

func (e *mapEntry) AsTransient() TransientCollection {
	return e.asVector().AsTransient()
}

func (e *mapEntry) AsTransientV() TransientVector {
	return e.asVector().AsTransientV()
}

func (e *mapEntry) String() string {
	return fmt.Sprintf("[%v %v]", e.Key(), e.Value())
}

var (
	_ PersistentCollection = &mapEntry{}
	_ PersistentVector     = &mapEntry{}
	_ PersistentStack      = &mapEntry{}
)
