package lang

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"sync/atomic"
	"unicode/utf8"
)

type RTType struct{}

var (
	RT        *RTType = &RTType{}
	idcounter int64
)

// NextID returns monotonically increasing int64 values, starting at 1
func (_ *RTType) NextID() int64 {
	return atomic.AddInt64(&idcounter, 1)
}

func (_ *RTType) Count(x interface{}) int {
	switch x := x.(type) {
	case FastCounter:
		return x.FastCount()
	case nil:
		return 0
	case PersistentCollection:
		return x.Count()
	case string:
		return utf8.RuneCountInString(x)
	case []interface{}:
		return len(x)
	default:
		xval := reflect.Indirect(reflect.ValueOf(x))
		switch xval.Kind() {
		case reflect.Slice, reflect.Array, reflect.Chan, reflect.Map:
			return xval.Len()
		}
		panic(fmt.Errorf("count not supported on this type: %T", x))
	}
}

func (_ *RTType) Seq(x interface{}) Seq {
	switch x := x.(type) {
	case Seqable:
		return x.Seq()
	case nil:
		return nil
	case string:
		if len(x) == 0 {
			return nil
		}
		return newStringSeq(x)
	case []interface{}:
		if len(x) == 0 {
			return nil
		}
		return newInterfaceSliceSeq(x)
	default:
		xval := reflect.Indirect(reflect.ValueOf(x))
		switch xval.Kind() {
		case reflect.Map:
			if xval.Len() == 0 {
				return nil
			}
			ifaces := make([]interface{}, 0, xval.Len())
			iter := xval.MapRange()
			for iter.Next() {
				ifaces = append(ifaces, &mapEntry{key: iter.Key().Interface(), val: iter.Value().Interface()})
			}
			return newInterfaceSliceSeq(ifaces)
		case reflect.Slice, reflect.Array:
			if xval.Len() == 0 {
				return nil
			}
			ifaces := make([]interface{}, 0, xval.Len())
			for i := 0; i < xval.Len(); i++ {
				ifaces = append(ifaces, xval.Index(i).Interface())
			}
			return newInterfaceSliceSeq(ifaces)
		default:
			panic(fmt.Errorf("cannot create a seq from type %T", x))
		}
	}
}

func (_ *RTType) CanSeq(x interface{}) bool {
	switch x := x.(type) {
	case nil:
		return true
	case Seqable:
		return true
	case string:
		return true
	case []interface{}:
		return true
	default:
		k := reflect.TypeOf(x).Kind()
		return k == reflect.Map || k == reflect.Slice || k == reflect.Array
	}
}

func (_ *RTType) IsSequential(x interface{}) bool {
	switch x := x.(type) {
	case Seq, PersistentVector, []interface{}, string:
		return true
	default:
		k := reflect.TypeOf(x).Kind()
		return k == reflect.Slice || k == reflect.Array
	}
}

func (_ *RTType) Equiv(x, y interface{}) bool {
	if identical(x, y) {
		return true
	}
	if x, ok := x.(Equiv); ok {
		return x.Equiv(y)
	}
	if y, ok := y.(Equiv); ok {
		return y.Equiv(x)
	}
	if x, y := ToNumber(x), ToNumber(y); x != nil && y != nil {
		return x.Equiv(y)
	}
	if reflect.TypeOf(x).Comparable() && reflect.TypeOf(y).Comparable() {
		return x == y
	}
	return false
}

func (_ *RTType) Compare(x, y interface{}) int {
	switch {
	case identical(x, y):
		return 0
	case x != nil:
		if y == nil {
			return 1
		}
		return ComparableFor(x).CompareTo(y)
	default: // x == nil && y != nil
		return -1
	}
}

func (_ *RTType) Instance(c, x interface{}) bool {
	return c.(*Type).Accepts(x)
}

func (_ *RTType) Identical(x, y interface{}) bool {
	return identical(x, y)
}

func (_ *RTType) Cons(x, coll interface{}) Seq {
	if coll == nil {
		return NewPersistentList(x)
	}
	if seq, ok := coll.(Seq); ok {
		return newCons(x, seq)
	}
	return newCons(x, RT.Seq(coll))
}

func (_ *RTType) Conj(coll PersistentCollection, x interface{}) PersistentCollection {
	if coll == nil {
		return NewPersistentList(x)
	}
	return coll.Conj(x)
}

func (_ *RTType) First(x interface{}) interface{} {
	if seq, ok := x.(Seq); ok {
		return seq.First()
	}
	if seq := RT.Seq(x); seq != nil {
		return seq.First()
	}
	return nil
}

func (_ *RTType) Second(x interface{}) interface{} {
	return RT.First(RT.Next(x))
}

func (_ *RTType) Next(x interface{}) Seq {
	if seq, ok := x.(Seq); ok {
		return seq.Next()
	}
	if seq := RT.Seq(x); seq != nil {
		return seq.Next()
	}
	return nil
}

func (_ *RTType) Rest(x interface{}) Seq {
	next := RT.Next(x)
	if next == nil {
		return EMPTY_LIST
	}
	return next
}

func (_ *RTType) Nth(x interface{}, i int, notfound interface{}) interface{} {
	switch x := x.(type) {
	case Indexer:
		return x.Nth(i, notfound)
	}
	return SeqNth(RT.Seq(x), i, notfound)
}

func (_ *RTType) AssocM(coll, key, val interface{}) PersistentMap {
	if coll == nil {
		return NewPersistentMap(key, val)
	}
	return coll.(PersistentMap).AssocM(key, val)
}

func (_ *RTType) Assoc(coll, key, val interface{}) Associative {
	if coll == nil {
		return NewPersistentMap(key, val)
	}
	return coll.(Associative).Assoc(key, val)
}

func (_ *RTType) Dissoc(coll, key interface{}) PersistentMap {
	if coll == nil {
		return nil
	}
	return coll.(PersistentMap).Dissoc(key)
}

func (_ *RTType) Meta(x interface{}) PersistentMap {
	if x, ok := x.(IMeta); ok {
		return x.Meta()
	}
	return nil
}

func (_ *RTType) ValAt(coll, key, notfound interface{}) interface{} {
	if coll, ok := coll.(ILookup); ok {
		return coll.ValAt(key, notfound)
	}
	return notfound
}

func (_ *RTType) Contains(coll, key interface{}) bool {
	switch coll := coll.(type) {
	case nil:
		return false
	case Associative:
		return coll.ContainsKey(key)
	case PersistentSet:
		return coll.Contains(key)
	case string:
		ix, isint := RT.IntCast(key)
		return isint && ix >= 0 && ix < utf8.RuneCountInString(coll)
	default:
		panic(fmt.Errorf("contains? not supported on type: %T", coll))
	}
}

func (_ *RTType) Get(coll, key, notfound interface{}) interface{} {
	switch coll := coll.(type) {
	case nil:
		return notfound
	case ILookup:
		return coll.ValAt(key, notfound)
	case PersistentSet:
		if coll.Contains(key) {
			return coll.Get(key)
		}
		return notfound
	case string:
		if i, isint := RT.IntCast(key); isint {
			return RT.Nth(coll, i, notfound)
		}
		return notfound
	default:
		return notfound
	}
}

func (_ *RTType) Find(coll, key interface{}) interface{} {
	switch coll := coll.(type) {
	case nil:
		return nil
	case Associative:
		return coll.EntryAt(key)
	default:
		panic(fmt.Errorf("find not supported on type: %T", coll))
	}
}

func (_ *RTType) IntCast(x interface{}) (int, bool) {
	switch x := x.(type) {
	case int:
		return x, true
	case nil:
		return 0, false
	default:
		xval := reflect.ValueOf(x)
		switch {
		case kindIsInt(xval.Kind()):
			return int(xval.Int()), true
		case kindIsUint(xval.Kind()):
			// TODO: Handle overflow?
			return int(xval.Uint()), true
		default:
			return 0, false
		}
	}
}

func (_ *RTType) BoolCast(x interface{}) bool {
	switch x := x.(type) {
	case bool:
		return x
	case nil:
		return false
	default:
		return true
	}
}

func (_ *RTType) Vec(x interface{}) interface{} {
	if v, ok := x.(PersistentVector); ok {
		return v
	}
	return PersistentVectorBuilder.FromSeq(RT.Seq(x))
}

func (_ *RTType) StrCat(s string, ss ...string) string {
	var sb strings.Builder
	sb.WriteString(s)
	for _, s := range ss {
		sb.WriteString(s)
	}
	return sb.String()
}

func (_ *RTType) Flush(x interface{}) error {
	switch x := x.(type) {
	case *os.File:
		return x.Sync()
	case *bufio.Writer:
		return x.Flush()
	case io.Writer:
		return nil
	default:
		panic(fmt.Errorf("Can't flush %T", x))
	}
}

func (_ *RTType) PrintHuman(x interface{}) string {
	switch x := x.(type) {
	case string:
		return x
	default:
		return RT.Print(x)
	}
}

func (_ *RTType) Print(x interface{}) string {
	switch x := x.(type) {
	case nil:
		return "nil"
	case string:
		return fmt.Sprintf("%q", x)
	case bool:
		return fmt.Sprintf("%t", x)
	case fmt.Stringer:
		return x.String()
	default:
		return fmt.Sprintf("#object[%T]", x)
	}
}

func (_ *RTType) ToSlice(x interface{}) []interface{} {
	switch x := x.(type) {
	case nil:
		return nil
	case []interface{}:
		return x
	case Seqable:
		c := 4
		if fc, ok := x.(FastCounter); ok {
			c = fc.FastCount()
		}
		slice := make([]interface{}, 0, c)
		for seq := x.Seq(); seq != nil; seq = seq.Next() {
			slice = append(slice, seq.First())
		}
		return slice
	default:
		return RT.ToSlice(RT.Seq(x))
	}
}

func (_ *RTType) SeqToTypedSlice(typ *Type, seq interface{}) interface{} {
	if typ == nil || typ == niltype {
		return RT.ToSlice(seq)
	}
	return typ.SliceOf(seq)
}

func (_ *RTType) SeqToTypedValueSlice(typ *Type, seq interface{}) interface{} {
	if typ == nil || typ == niltype {
		return RT.ToSlice(seq)
	}
	return typ.ValueSliceOf(seq)
}

func (_ *RTType) TypeOf(x interface{}) *Type {
	switch x := x.(type) {
	case nil:
		return niltype
	case *ReflectiveFn:
		return WrapType(x.fn.Type())
	default:
		return TypeOf(x)
	}
}

func (_ *RTType) Cast(t *Type, x interface{}) interface{} {
	if t.Accepts(x) {
		return x
	}
	panic(fmt.Errorf("Cannot cast %T to %v", x, t))
}

func (_ *RTType) Peek(x interface{}) interface{} {
	if x == nil {
		return nil
	}
	return x.(PersistentStack).Peek()
}

func (_ *RTType) Pop(x interface{}) PersistentStack {
	if x == nil {
		return nil
	}
	return x.(PersistentStack).Pop()
}

func (_ *RTType) Keys(x interface{}) Seq {
	return x.(PersistentMap).Keys()
}

func (_ *RTType) Values(x interface{}) Seq {
	return x.(PersistentMap).Values()
}

func (_ *RTType) ReadChan(ch chan interface{}) (interface{}, bool) {
	x, ok := <-ch
	return x, ok
}

func (_ *RTType) WriteChan(ch chan interface{}, x interface{}) (ok bool) {
	defer func() {
		if !ok {
			recover()
		}
	}()
	ch <- x
	ok = true
	return
}

func (_ *RTType) HashEq(x interface{}) uint32 {
	switch x := x.(type) {
	case nil:
		return 0
	case HashEq:
		return x.HashEq()
	case string:
		return HashUnencodedChars(x)
	default:
		if n := ToNumber(x); n != nil {
			return n.HashEq()
		}
		if RT.IsSequential(x) {
			return HashOrdered(RT.Seq(x))
		}
		if RT.CanSeq(x) {
			return HashUnordered(RT.Seq(x))
		}
		return HashIdentity(x)
	}
}

func (_ *RTType) PrintNative() string {
	return "lang.RT"
}

func (_ *RTType) String() string {
	return "#RT"
}
