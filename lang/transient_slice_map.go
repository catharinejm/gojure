package lang

import (
	"errors"
	"sync/atomic"
)

type transientSliceMap struct {
	keys   []interface{}
	values []interface{}
	edit   int32
}

func newTransientSliceMap(pm *persistentSliceMap) *transientSliceMap {
	newkeys := make([]interface{}, len(pm.keys))
	copy(newkeys, pm.keys)
	newvals := make([]interface{}, len(pm.values))
	copy(newvals, pm.values)
	return &transientSliceMap{newkeys, newvals, 1}
}

func (tm *transientSliceMap) ensureEditable() {
	if atomic.LoadInt32(&tm.edit) == 0 {
		panic(errors.New("Transient used after persistent! call"))
	}
}

func (tm *transientSliceMap) Persistent() PersistentCollection {
	return tm.PersistentM()
}

func (tm *transientSliceMap) PersistentM() PersistentMap {
	if !atomic.CompareAndSwapInt32(&tm.edit, 1, 0) {
		panic(errors.New("Transient used after persistent! call"))
	}
	return &persistentSliceMap{keys: tm.keys, values: tm.values}
}

func (tm *transientSliceMap) Count() int {
	return tm.FastCount()
}

func (tm *transientSliceMap) FastCount() int {
	tm.ensureEditable()
	return len(tm.keys)
}

func (tm *transientSliceMap) Conj(val interface{}) TransientCollection {
	// relies on Assoc for ensureEditable
	// tm.ensureEditable()
	switch val := val.(type) {
	case MapEntry:
		return tm.Assoc(val.Key(), val.Value())
	case PersistentVector:
		if val.Count() != 2 {
			panic(errors.New("Vetor arg to Conj just be a pair"))
		}
		return tm.Assoc(val.Nth(0, nil), val.Nth(1, nil))
	default:
		for elms := RT.Seq(val); elms != nil; elms = elms.Next() {
			e := elms.First().(MapEntry)
			tm.Assoc(e.Key(), e.Value())
		}
		return tm
	}
}

func (tm *transientSliceMap) ValAt(key, notfound interface{}) interface{} {
	tm.ensureEditable()
	ix := indexOf(tm.keys, key)
	if ix >= 0 {
		return tm.values[ix]
	}
	return notfound
}

func (tm *transientSliceMap) Assoc(key, value interface{}) TransientAssociative {
	return tm.AssocM(key, value)
}

func (tm *transientSliceMap) AssocM(key, value interface{}) TransientMap {
	tm.ensureEditable()
	ix := indexOf(tm.keys, key)
	if ix >= 0 {
		if !identical(tm.values[ix], value) {
			tm.values[ix] = value
		}
	} else {
		tm.keys = append(tm.keys, key)
		tm.values = append(tm.values, value)
	}
	return tm
}

func (tm *transientSliceMap) ContainsKey(key interface{}) bool {
	tm.ensureEditable()
	return indexOf(tm.keys, key) >= 0
}

func (tm *transientSliceMap) EntryAt(key interface{}) MapEntry {
	tm.ensureEditable()
	ix := indexOf(tm.keys, key)
	if ix >= 0 {
		return &mapEntry{key: tm.keys[ix], val: tm.values[ix]}
	}
	return nil
}

func (tm *transientSliceMap) Dissoc(key interface{}) TransientMap {
	tm.ensureEditable()
	ix := indexOf(tm.keys, key)
	if ix >= 0 {
		if ix < len(tm.keys)-1 {
			tm.keys[ix] = tm.keys[len(tm.keys)-1]
			tm.values[ix] = tm.values[len(tm.values)-1]
		}
		tm.keys[len(tm.keys)-1] = nil
		tm.values[len(tm.values)-1] = nil
		tm.keys = tm.keys[:len(tm.keys)-1]
		tm.values = tm.values[:len(tm.values)-1]
	}
	return tm
}

var (
	_ TransientCollection  = &transientSliceMap{}
	_ TransientAssociative = &transientSliceMap{}
	_ TransientMap         = &transientSliceMap{}
	_ FastCounter          = &transientSliceMap{}
)
