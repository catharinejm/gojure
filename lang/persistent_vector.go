package lang

import (
	"errors"
	"strings"
)

type PersistentVector interface {
	Associative // implies PersistentCollection
	Indexer
	Reversible
	IFn
	PersistentStack
	Comparable
	EditableCollection
	AsTransientV() TransientVector
	FastCount() int
	AssocN(i int, x interface{}) PersistentVector
	AssocV(key, val interface{}) PersistentVector
	ConjV(x interface{}) PersistentVector
	Subvec(lo, hi int) PersistentVector
}

type persistentVector struct {
	tail []interface{}
	metadata
	orderedHashEqCache
}

var (
	EMPTY_VECTOR *persistentVector = &persistentVector{}
)

func NewPersistentVector(elems ...interface{}) *persistentVector {
	if len(elems) == 0 {
		return EMPTY_VECTOR
	}
	elemcopy := make([]interface{}, len(elems))
	copy(elemcopy, elems)
	return &persistentVector{tail: elemcopy}
}

func (pv *persistentVector) Count() int {
	return len(pv.tail)
}

func (pv *persistentVector) FastCount() int {
	return len(pv.tail)
}

func (pv *persistentVector) ConjV(x interface{}) PersistentVector {
	// fmt.Printf("pv.Conj(%v): len = %d, cap = %d\n", x, len(pv.tail), cap(pv.tail))
	newTail := make([]interface{}, len(pv.tail)+1)
	copy(newTail, pv.tail)
	newTail[len(pv.tail)] = x
	return &persistentVector{tail: newTail, metadata: pv.metadata}
}

func (pv *persistentVector) Conj(x interface{}) PersistentCollection {
	return pv.ConjV(x)
}

func (pv *persistentVector) AssocN(i int, x interface{}) PersistentVector {
	if i < 0 || i >= len(pv.tail) {
		_ = pv.tail[i] // panic
	}
	newTail := make([]interface{}, len(pv.tail))
	copy(newTail, pv.tail)
	newTail[i] = x
	return &persistentVector{tail: newTail, metadata: pv.metadata}
}

func (pv *persistentVector) AssocV(key, val interface{}) PersistentVector {
	return pv.AssocN(int(AssertInt(key)), val)
}

func (pv *persistentVector) Assoc(key, val interface{}) Associative {
	return pv.AssocV(key, val)
}

func (pv *persistentVector) Nth(i int, notFound interface{}) interface{} {
	if i >= 0 && i < len(pv.tail) {
		return pv.tail[i]
	}
	return notFound
}

func (pv *persistentVector) ContainsKey(key interface{}) bool {
	i, isint := RT.IntCast(key)
	return isint && i >= 0 && i < pv.Count()
}

func (pv *persistentVector) EntryAt(key interface{}) MapEntry {
	if i, isint := RT.IntCast(key); isint {
		if i >= 0 && i < pv.Count() {
			return &mapEntry{key: i, val: pv.Nth(i, nil)}
		}
	}
	return nil
}

func (pv *persistentVector) ValAt(key, notfound interface{}) interface{} {
	if i, isint := RT.IntCast(key); isint {
		return pv.Nth(i, notfound)
	}
	return notfound
}

func (pv *persistentVector) Seq() Seq {
	if pv.Count() == 0 {
		return nil
	}
	return newPersistentVectorSeq(pv, 0)
}

func (pv *persistentVector) RSeq() Seq {
	if pv.Count() == 0 {
		return nil
	}
	return newPersistentVectorRSeq(pv, pv.Count()-1)
}

func (pv *persistentVector) Subvec(lo, hi int) PersistentVector {
	return PersistentVectorBuilder.CreateOwning(pv.tail[lo:hi]...)
}

func (pv *persistentVector) Peek() interface{} {
	if pv.Count() == 0 {
		return nil
	}
	return pv.Nth(pv.Count()-1, nil)
}

func (pv *persistentVector) Pop() PersistentStack {
	if pv.Count() == 0 {
		panic(errors.New("Can't pop empty vector"))
	}
	if pv.Count() == 1 {
		return EMPTY_VECTOR.WithMeta(pv.meta).(PersistentStack)
	}
	return pv.Subvec(0, pv.Count()-1).(IObj).WithMeta(pv.meta).(PersistentStack)
}

func (pv *persistentVector) WithMeta(newMeta PersistentMap) IObj {
	if pv.meta == newMeta {
		return pv
	}
	newPV := *pv
	newPV.metadata = metadata{newMeta}
	return &newPV
}

func (pv *persistentVector) Equiv(other interface{}) bool {
	switch other := other.(type) {
	case PersistentVector:
		if pv == other {
			return true
		}
		if pv.Count() != other.Count() {
			return false
		}
		for i := 0; i < pv.Count(); i++ {
			if !RT.Equiv(pv.Nth(i, nil), other.Nth(i, nil)) {
				return false
			}
		}
		return true
	case Seq:
		return SeqEquiv(pv.Seq(), other)
	default:
		return false
	}
}

func (pv *persistentVector) String() string {
	var sb strings.Builder
	sb.WriteString("[")
	for i, x := range pv.tail {
		sb.WriteString(RT.Print(x))
		if i < len(pv.tail)-1 {
			sb.WriteString(" ")
		}
	}
	sb.WriteString("]")
	return sb.String()
}

func newPersistentVectorSeq(v *persistentVector, i int) *aseqFastCount {
	return &aseqFastCount{aseq{
		first: func() interface{} {
			return v.Nth(i, nil)
		},
		next: func() Seq {
			if i < v.Count()-1 {
				return newPersistentVectorSeq(v, i+1)
			}
			return nil
		},
		count: func() int {
			return v.Count() - i
		},
	}}
}

func newPersistentVectorRSeq(v *persistentVector, i int) *aseqFastCount {
	return &aseqFastCount{aseq{
		first: func() interface{} {
			return v.Nth(i, nil)
		},
		next: func() Seq {
			if i > 0 {
				return newPersistentVectorRSeq(v, i-1)
			}
			return nil
		},
		count: func() int {
			return i + 1
		},
	}}
}

//// Comparable ////

func (pv *persistentVector) CompareTo(other interface{}) int {
	v := other.(PersistentVector)
	switch {
	case pv.Count() < v.Count():
		return -1
	case pv.Count() > v.Count():
		return 1
	default:
		for i := 0; i < pv.Count(); i++ {
			c := RT.Compare(pv.Nth(i, nil), v.Nth(i, nil))
			if c != 0 {
				return c
			}
		}
		return 0
	}
}

func (pv *persistentVector) AsTransient() TransientCollection {
	return pv.AsTransientV()
}

func (pv *persistentVector) AsTransientV() TransientVector {
	return newTransientVector(pv)
}

func (pv *persistentVector) HashEq() uint32 {
	return pv.orderedHashEq(pv)
}

//// IFn ////

func (pv *persistentVector) Invoke(vm *VM, args Seq) interface{} {
	switch RT.Count(args) {
	case 1:
		return pv.ValAt(args.First(), nil)
	case 2:
		return pv.ValAt(args.First(), RT.Second(args))
	default:
		panic(&arityError{RT.Count(args), "1,2"})
	}
}

func (pv *persistentVector) VM(vm *VM) *VM {
	return vm
}

var (
	_ PersistentCollection = &persistentVector{}
	_ PersistentVector     = &persistentVector{}
	_ PersistentStack      = &persistentVector{}
	_ Associative          = &persistentVector{}
	_ FastCounter          = &persistentVector{}
	_ Indexer              = &persistentVector{}
	_ IMeta                = &persistentVector{}
	_ IObj                 = &persistentVector{}
	_ IFn                  = &persistentVector{}
	_ Comparable           = &persistentVector{}
	_ EditableCollection   = &persistentVector{}
	_ HashEq               = &persistentVector{}
)
