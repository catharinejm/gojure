package lang

import (
	"runtime"
	"strings"
)

func runtime_loadFrames(skip, take int) (fs []runtime.Frame) {
	pcs := make([]uintptr, take+8) // 8 extra for skipped runtime frames
	n := runtime.Callers(skip+2, pcs)
	if n == 0 {
		return nil
	}
	fs = make([]runtime.Frame, 0, take)
	frames := runtime.CallersFrames(pcs[:n])
	for {
		frame, more := frames.Next()
		if !strings.HasPrefix(frame.Function, "runtime.") {
			fs = append(fs, frame)
		}
		if !more || len(fs) == take {
			return
		}
	}
}
