package lang

type PersistentMap interface {
	Associative // implies PersistentCollection
	FastCount() int
	AssocM(key, val interface{}) PersistentMap
	Dissoc(key interface{}) PersistentMap
	Keys() Seq
	Values() Seq
	AsTransientM() TransientMap
}
