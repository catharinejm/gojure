package lang

import (
	"fmt"
	"sync"
)

type Delay struct {
	once sync.Once
	val  interface{}
	err  error
	fn   IFn
}

func NewDelay(fn IFn) *Delay {
	return &Delay{fn: fn}
}

func ForceDelay(vm *VM, x interface{}) interface{} {
	if d, ok := x.(*Delay); ok {
		return d.Deref(vm)
	}
	return x
}

func (d *Delay) Deref(vm *VM) interface{} {
	d.once.Do(func() {
		defer func() {
			if e := recover(); e != nil {
				d.err = toError(e)
			}
			d.fn = nil
		}()
		d.val = d.fn.Invoke(vm, nil)
	})
	if d.err != nil {
		panic(d.err)
	}
	return d.val
}

func (d *Delay) IsRealized() bool {
	return d.fn == nil
}

func (d *Delay) String() string {
	if d.IsRealized() {
		return fmt.Sprintf("#Delay[{:status :ready, :val %v}]", d.val)
	}
	return "#Delay[{:status :pending, :val nil}]"
}
