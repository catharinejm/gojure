package lang

var (
	TH testHelpers = testHelpers{}
)

type testHelpers struct{}

func (_ testHelpers) Compile(vm *VM, input string) []interface{} {
	return Compile(vm, ReadString(vm, input)).(*Fn).bodies[0]
}

func (_ testHelpers) Quoted(val interface{}) *bc_Quoted {
	return &bc_Quoted{val}
}

// func (_ testHelpers) Push(val interface{}) *bc_PushReturn {
// 	return &bc_PushReturn{val}
// }

func (_ testHelpers) PopReturn() *bc_PopReturn {
	return &bc_PopReturn{}
}

func (_ testHelpers) BC_Vector(count int, hasmeta bool) *bc_Vector {
	return &bc_Vector{count, hasmeta}
}

func (_ testHelpers) BC_Map(count int, hasmeta bool) *bc_Map {
	return &bc_Map{count, hasmeta}
}

func (_ testHelpers) BC_Set(count int, hasmeta bool) *bc_Set {
	return &bc_Set{count, hasmeta}
}

func (_ testHelpers) BC_Invoke(count int, file string, line, col int) *bc_Invoke {
	return &bc_Invoke{count, file, line, col}
}

func (_ testHelpers) BC_PushLocal(sym string) *bc_PushLocal {
	return &bc_PushLocal{ParseSymbol(sym)}
}

func (_ testHelpers) BC_PopLocals(count int) *bc_PopLocals {
	return &bc_PopLocals{count}
}

func (_ testHelpers) BC_Param(idx int, sym string) *bc_Param {
	return &bc_Param{idx, ParseSymbol(sym), nil}
}

func (_ testHelpers) BC_DerefVar(v *Var) *bc_DerefVar {
	return &bc_DerefVar{v}
}

// func (_ testHelpers) Depush(x interface{}) (interface{}, bool) {
// 	if x, ispush := x.(*bc_PushReturn); ispush {
// 		return x.value, true
// 	}
// 	return x, false
// }
