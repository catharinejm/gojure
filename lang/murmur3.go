/*
 * Copyright (C) 2011 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

/*
 * MurmurHash3 was written by Austin Appleby, and is placed in the public
 * domain. The author hereby disclaims copyright to this source code.
 */

/*
 * Original Source:
 * http://code.google.com/p/smhasher/source/browse/trunk/MurmurHash3.cpp
 */

/**
 * This version adapted from:
 * https://github.com/clojure/clojure/blob/master/src/jvm/clojure/lang/Murmur3.java
 */

package lang

import (
	"math/bits"
	"unsafe"
)

/**
 * See http://smhasher.googlecode.com/svn/trunk/MurmurHash3.cpp
 * MurmurHash3_x86_32
 *
 * @author Austin Appleby
 * @author Dimitris Andreou
 * @author Kurt Alfred Kluever
 */

const (
	mm3seed uint32 = 0
	mm3C1   uint32 = 0xcc9e2d51
	mm3C2   uint32 = 0x1b873593
)

func HashInt32(input int32) uint32 {
	if input == 0 {
		return 0
	}
	k1 := mixK1(uint32(input))
	h1 := mixH1(mm3seed, k1)

	return fmix(h1, 4)
}

func HashInt64(input int64) uint32 {
	if input == 0 {
		return 0
	}
	low := uint32(input)
	high := uint32(input >> 32)

	k1 := mixK1(low)
	h1 := mixH1(mm3seed, k1)

	k1 = mixK1(high)
	h1 = mixH1(h1, k1)

	return fmix(h1, 8)
}

func HashFunc(f func() (uint32, int, bool)) uint32 {
	var (
		h1    uint32 = mm3seed
		k1, c uint32
		more  = true
	)
	for more {
		var n int
		k1, n, more = f()
		k1 = mixK1(k1)
		c += uint32(n)
		if n == 4 {
			h1 = mixH1(h1, k1)
		} else {
			h1 ^= k1
		}
	}
	return fmix(h1, c)
}

func HashBytes(bytes []byte) uint32 {
	return HashFunc(func() (word uint32, n int, more bool) {
		for ; n < 4 && n < len(bytes); n++ {
			word |= uint32(bytes[n] << (8 * n))
		}
		bytes = bytes[n:]
		more = len(bytes) > 0
		return
	})
}

func HashUnencodedChars(input string) uint32 {
	return HashFunc(func() (word uint32, n int, more bool) {
		for ; n < 4 && n < len(input); n++ {
			word |= uint32(input[n] << (8 * n))
		}
		input = input[n:]
		more = len(input) > 0
		return
	})
}

func HashIdentity(x interface{}) uint32 {
	return HashBytes((*[2 * unsafe.Sizeof(uintptr(0))]byte)(unsafe.Pointer(&x))[:])
}

func MixCollHash(hash, count uint32) uint32 {
	h1 := mm3seed
	k1 := mixK1(hash)
	h1 = mixH1(h1, k1)
	return fmix(h1, count)
}

func HashOrdered(xs Seq) uint32 {
	var n, hash uint32 = 0, 1

	for xs = RT.Seq(xs); xs != nil; xs = xs.Next() {
		hash = 31*hash + RT.HashEq(xs.First())
		n++
	}

	return MixCollHash(hash, n)
}

func HashUnordered(xs Seq) uint32 {
	var n, hash uint32
	for xs = RT.Seq(xs); xs != nil; xs = xs.Next() {
		hash += RT.HashEq(xs.First())
		n++
	}

	return MixCollHash(hash, n)
}

func mixK1(k1 uint32) uint32 {
	k1 *= mm3C1
	k1 = bits.RotateLeft32(k1, 15)
	k1 *= mm3C2
	return k1
}

func mixH1(h1, k1 uint32) uint32 {
	h1 ^= k1
	h1 = bits.RotateLeft32(h1, 13)
	h1 = h1*5 + 0xe6546b64
	return h1
}

// Finalization mix - force all bits of a hash block to avalanche
func fmix(h1, length uint32) uint32 {
	h1 ^= length
	h1 ^= h1 >> 16
	h1 *= 0x85ebca6b
	h1 ^= h1 >> 13
	h1 *= 0xc2b2ae35
	h1 ^= h1 >> 16
	return h1
}
