package lang

import (
	"errors"
	"fmt"
	"reflect"
	"unsafe"
)

func kindIsInt(k reflect.Kind) bool {
	return k >= reflect.Int && k <= reflect.Int64
}

func kindIsUint(k reflect.Kind) bool {
	return k >= reflect.Uint && k <= reflect.Uintptr
}

func kindIsFloat(k reflect.Kind) bool {
	return k == reflect.Float32 || k == reflect.Float64
}

func toError(e interface{}) error {
	switch e := e.(type) {
	case error:
		return e
	case string:
		return errors.New(e)
	default:
		return fmt.Errorf("%v", e)
	}
}

func identical(x, y interface{}) bool {
	return *(*[2]uintptr)(unsafe.Pointer(&x)) == *(*[2]uintptr)(unsafe.Pointer(&y))
}
