package lang

type Cons struct {
	first interface{}
	next  Seq
	metadata
}

func newCons(first interface{}, next Seq) *Cons {
	return &Cons{first, next, metadata{}}
}

func (c *Cons) Seq() Seq {
	return c
}

func (c *Cons) First() interface{} {
	return c.first
}

func (c *Cons) Next() Seq {
	if c.next == nil {
		return nil
	}
	return c.next.Seq()
}

func (c *Cons) Count() int {
	return 1 + RT.Count(c.next)
}

func (c *Cons) WithMeta(meta PersistentMap) IObj {
	if c.meta == meta {
		return c
	}
	return &Cons{c.first, c.next, metadata{meta}}
}

func (c *Cons) Conj(x interface{}) PersistentCollection {
	return newCons(x, c)
}

func (c *Cons) Equiv(other interface{}) bool {
	return SeqEquiv(c, other)
}

func (c *Cons) String() string {
	return SeqString(c)
}

var (
	_ Seq                  = &Cons{}
	_ PersistentCollection = &Cons{}
	_ IMeta                = &Cons{}
	_ IObj                 = &Cons{}
)
