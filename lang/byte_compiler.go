package lang

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"strings"
	"sync"
)

type ByteCompiler struct {
	vmHolder
	baseIndex int
	curIndex  int
	locals    map[Symbol]localBinding
	closes    []*Symbol
	dbg       bool
	optimize  bool
}

type localBinding struct {
	sym   *Symbol
	index int
	tag   *Type
}

type fnMethod struct {
	name       *Symbol
	arity      int
	isVariadic bool
	params     PersistentVector
	source     Seq
	code       []interface{}
}

var (
	vCURRENT_FN      *Var = DetachedVarWithRoot("current-fn", nil).SetDynamic()
	vCURRENT_FN_NAME *Var = DetachedVarWithRoot("current-fn-name", nil).SetDynamic()
	vCURRENT_DEF     *Var = DetachedVarWithRoot("current-def", nil).SetDynamic()
	vRECUR_PC        *Var = DetachedVarWithRoot("recur-pc", nil).SetDynamic()
	vRECUR_LOCAL_IX  *Var = DetachedVarWithRoot("recur-local-index", nil).SetDynamic()
	vRECUR_ARITY     *Var = DetachedVarWithRoot("recur-arity", nil).SetDynamic()
	vGENSYM_ENV      *Var = DetachedVarWithRoot("gensym-env", nil).SetDynamic()

	kRETURN    *Keyword = ParseKeyword("return")
	kEXPR      *Keyword = ParseKeyword("expr")
	kSTATEMENT *Keyword = ParseKeyword("statement")

	vCONTEXT      *Var = DetachedVarWithRoot("context", kEXPR).SetDynamic()
	vCURRENT_BIND *Var = DetachedVarWithRoot("current-bind", nil).SetDynamic()

	compilerPool sync.Pool = sync.Pool{New: func() interface{} {
		return &ByteCompiler{}
	}}
)

type compileError struct {
	err   error
	forms []interface{}
}

func (e *compileError) Error() string {
	formStrs := make([]string, 0, len(e.forms))
	for _, f := range e.forms {
		if RT.Meta(f) != nil && RT.Meta(f).ValAt(kFILE, nil) != nil {
			file := RT.Meta(f).ValAt(kFILE, nil).(string)
			line := RT.Meta(f).ValAt(kLINE, nil).(int)
			column := RT.Meta(f).ValAt(kCOLUMN, nil).(int)
			formStrs = append(formStrs, fmt.Sprintf("%s\n\t\t%s:%d:%d", RT.Print(f), file, line, column))
		} else {
			formStrs = append(formStrs, RT.Print(f))
		}
	}
	return fmt.Sprintf("COMPILE ERROR: %s\n\t%s", e.err, strings.Join(formStrs, "\n\t"))
}

func newByteCompiler(vm *VM) *ByteCompiler {
	bc := unpoolByteCompiler()
	bc.vm = vm
	bc.locals = make(map[Symbol]localBinding)
	bc.dbg = RT.BoolCast(vCOMPILE_DEBUG.Deref(bc.vm))
	bc.optimize = RT.BoolCast(vOPTIMIZE.Deref(bc.vm))
	return bc
}

func unpoolByteCompiler() *ByteCompiler {
	return compilerPool.Get().(*ByteCompiler)
}

func repoolByteCompiler(bc *ByteCompiler) {
	*bc = ByteCompiler{}
	compilerPool.Put(bc)
}

func withByteCompiler(vm *VM, f func(*ByteCompiler) interface{}) interface{} {
	bc := newByteCompiler(vm)
	defer repoolByteCompiler(bc)
	return f(bc)
}

func cloneLocals(locals map[Symbol]localBinding) map[Symbol]localBinding {
	newlocals := make(map[Symbol]localBinding, len(locals))
	for s, b := range locals {
		newlocals[s] = b
	}
	return newlocals
}

func (bc *ByteCompiler) Compile(form interface{}) (fn *Fn) {
	form = bc.Macroexpand(form)
	methods := []*fnMethod{&fnMethod{source: NewPersistentList(form)}}
	bcfn := bc.doCompileFn(methods, nil, false)
	fn = &Fn{}
	fn.bodies[0] = bcfn.bodies[0]
	return
}

func (bc *ByteCompiler) debug(msg string, args ...interface{}) {
	if bc.dbg {
		fmt.Fprintf(os.Stderr, msg+"\n", args...)
	}
}

func (bc *ByteCompiler) setLocal(sym *Symbol, idx int) {
	key := sym.WithMeta(nil).(*Symbol)
	bc.locals[*key] = localBinding{sym, idx, bc.resolveTag(RT.Meta(sym))}
}

func (bc *ByteCompiler) getLocal(sym *Symbol) (lb localBinding, ok bool) {
	if sym.Meta() != nil {
		sym = sym.WithMeta(nil).(*Symbol)
	}
	lb, ok = bc.locals[*sym]
	return
}

func (bc *ByteCompiler) addParams(name *Symbol, params Seq) {
	if name != nil {
		params = RT.Cons(name, params)
	} else {
		bc.curIndex++
	}
	for ; params != nil; params = params.Next() {
		p := params.First().(*Symbol)
		if !RT.Equiv(p, sAMP) {
			bc.setLocal(p, bc.curIndex)
			bc.curIndex++
		}
	}
}

func (bc *ByteCompiler) analyzeSymbol(sym *Symbol) interface{} {
	if sym.Namespace() == "" {
		if lb, ok := bc.getLocal(sym); ok {
			if lb.index < bc.baseIndex {
				for i, s := range bc.closes {
					if s.Equiv(sym) {
						// fmt.Printf("found close: %v\n", sym)
						return &bc_ClosedOver{i, s, lb.tag}
					}
				}
				bc.closes = append(bc.closes, sym)
				// fmt.Printf("added close: %v, (bc = %p, bc.closes: %#v)\n", sym, bc, bc.closes)
				return &bc_ClosedOver{len(bc.closes) - 1, sym, lb.tag}
			}
			return &bc_Param{lb.index - bc.baseIndex, sym, lb.tag}
		}
	}
	switch obj := ResolveName(bc.vm.CurrentNS(), sym).(type) {
	case nil:
		panic(fmt.Errorf("cannot resolve symbol: %v in this context", sym))
	case *Var:
		return &bc_DerefVar{obj}
	case *Import:
		return &bc_Quoted{obj.Object()}
	default:
		panic(fmt.Errorf("INTERNAL ERROR: got invalid interned object: %v (%T)", obj, obj))
	}
}

func (bc *ByteCompiler) emit(form interface{}) {
	// fmt.Printf("ByteCompile: %v (bc = %p)\n", form, bc)
	defer func() {
		if e := recover(); e != nil {
			switch e := e.(type) {
			case *compileError:
				e.forms = append(e.forms, form)
				panic(e)
			default:
				panic(&compileError{toError(e), []interface{}{form}})
			}
		}
	}()
	form = bc.Macroexpand(form)
	switch form := form.(type) {
	case nil, *Var, *Keyword, string, bool, Rune, Int, Float:
		// Self-quoting values
		bc.injectBytecode(&bc_Quoted{form})
	case int, int8, int16, int32, int64:
		// coerce all ints to int64
		bc.injectBytecode(&bc_Quoted{Int(reflect.ValueOf(form).Int())})
	case uint, uint8, uint16, uint32, uint64, uintptr:
		// coerce all uints to int64
		// TODO: handle overflow?
		bc.injectBytecode(&bc_Quoted{Int(int64(reflect.ValueOf(form).Uint()))})
	case float32, float64:
		bc.injectBytecode(&bc_Quoted{Float(reflect.ValueOf(form).Float())})
	case *Symbol:
		bc.injectBytecode(bc.analyzeSymbol(form))
	case PersistentVector:
		hasMeta := false
		if form, ok := form.(IObj); ok && form.Meta() != nil {
			hasMeta = true
			bc.asExprs(func() {
				bc.emit(form.Meta())
			})
		}
		bc.asExprs(func() {
			for i := 0; i < form.Count(); i++ {
				bc.emit(form.Nth(i, nil))
			}
		})
		bc.injectBytecode(&bc_Vector{form.Count(), hasMeta})
	case PersistentMap:
		hasMeta := false
		if form, ok := form.(IObj); ok && form.Meta() != nil {
			hasMeta = true
			bc.asExprs(func() {
				bc.emit(form.Meta())
			})
		}
		bc.asExprs(func() {
			seq := form.Seq()
			for seq != nil {
				keyval := seq.First().(MapEntry)
				bc.emit(keyval.Key())
				bc.emit(keyval.Value())
				seq = seq.Next()
			}
		})
		bc.injectBytecode(&bc_Map{form.Count(), hasMeta})
	case PersistentSet:
		hasMeta := false
		if form, ok := form.(IObj); ok && form.Meta() != nil {
			hasMeta = true
			bc.asExprs(func() {
				bc.emit(form.Meta())
			})
		}
		bc.asExprs(func() {
			for seq := form.Seq(); seq != nil; seq = seq.Next() {
				bc.emit(seq.First())
			}
		})
		bc.injectBytecode(&bc_Set{form.Count(), hasMeta})
	case Seq:
		switch {
		case RT.Count(form) == 0:
			hasMeta := false
			if form, ok := form.(IObj); ok && form.Meta() != nil {
				meta := form.Meta().Dissoc(kFILE).Dissoc(kLINE).Dissoc(kCOLUMN)
				if RT.Count(meta) > 0 {
					hasMeta = true
					bc.asExprs(func() {
						bc.emit(meta)
					})
				}
			}
			bc.injectBytecode(&bc_EmptyList{hasMeta})
		case RT.Equiv(form.First(), sQUOTE):
			bc.compileQuote(form.Next())
		case RT.Equiv(form.First(), sDEF):
			bc.compileDef(form.Next())
		case RT.Equiv(form.First(), sFN):
			bc.compileFn(form.Next(), fnMeta(form))
		case RT.Equiv(form.First(), sIF):
			bc.compileIf(form.Next())
		case RT.Equiv(form.First(), sLET):
			bc.compileLet(form.Next(), false)
		case RT.Equiv(form.First(), sLOOP):
			bc.compileLet(form.Next(), true)
		case RT.Equiv(form.First(), sRECUR):
			bc.compileRecur(form.Next())
		case RT.Equiv(form.First(), sDO):
			bc.compileDo(form.Next())
		case RT.Equiv(form.First(), sVAR):
			bc.compileTheVar(form.Next())
		case RT.Equiv(form.First(), sGO):
			bc.compileGo(form.Next())
		case RT.Equiv(form.First(), sDOT):
			bc.compileDot(form.Next(), RT.Meta(form))
		case RT.Equiv(form.First(), sMAKE):
			bc.compileMake(form.Next())
		case RT.Equiv(form.First(), sNEW):
			bc.compileNew(form.Next())
		case RT.Equiv(form.First(), sPUSH_THREAD_BINDINGS):
			if RT.Count(form) != 2 {
				panic(&arityError{RT.Count(form) - 1, "1"})
			}
			bc.asExprs(func() {
				bc.emit(RT.Second(form))
			})
			bc.injectBytecode(&bc_PushThreadBindings{})
			if bc.context() != kSTATEMENT {
				bc.injectBytecode(&bc_Quoted{nil})
			}
		case RT.Equiv(form.First(), sPOP_THREAD_BINDINGS):
			if form.Next() != nil {
				panic(&arityError{RT.Count(form) - 1, "0"})
			}
			bc.injectBytecode(&bc_PopThreadBindings{})
			if bc.context() != kSTATEMENT {
				bc.injectBytecode(&bc_Quoted{nil})
			}
		case RT.Equiv(form.First(), sASSIGN):
			bc.compileAssign(form.Next(), RT.Meta(form))
		default:
			bc.compileInvoke(form)
		}
	default:
		// quote everything else
		bc.injectBytecode(&bc_Quoted{form})
		// panic(fmt.Errorf("cannot compile type: %T", form))
	}
}

func fnMeta(fn Seq) PersistentMap {
	formMeta := RT.Meta(fn)
	nameMeta := RT.Meta(fn.First())
	if formMeta == nil {
		return nameMeta
	}
	return RT.Conj(formMeta, nameMeta).(PersistentMap)
}

func (bc *ByteCompiler) Macroexpand(form interface{}) interface{} {
	// fmt.Printf("macroexpand(%v)\n", form)
	exp := bc.Macroexpand1(form)
	for !identical(exp, form) {
		form = exp
		// fmt.Printf("\tmacroexpand(%v)\n", form)
		exp = bc.Macroexpand1(form)
	}
	// fmt.Printf("result: %v\n", exp)
	return exp
}

func (bc *ByteCompiler) Macroexpand1(x interface{}) interface{} {
	if form, ok := x.(Seq); ok {
		op, _ := form.First().(*Symbol)
		switch {
		case op == nil || IsSpecialSymbol(op) || bc.isLocal(op):
			return x
		case RT.Equiv(op, sSYNTAX_QUOTE):
			bc.PushThreadBindings(NewPersistentMap(
				vGENSYM_ENV, EMPTY_MAP,
			))
			defer bc.PopThreadBindings()
			sqBody := bc.syntaxQuote(RT.Second(form))
			// fmt.Printf("syntax-quote result: %v\n", sqBody)
			if sb, ok := sqBody.(IObj); ok && RT.Meta(x) != nil {
				sqBody = sb.WithMeta(RT.Meta(x))
			}
			return sqBody
		case op.Namespace() == "" && op.Name()[0] == '.':
			if RT.Count(form) < 2 {
				panic(errors.New("host expression requires receiver"))
			}

			return RT.Cons(sDOT, RT.Cons(RT.Second(form),
				RT.Cons(NewSymbol("", op.Name()[1:]), form.Next().Next())),
			).(IObj).WithMeta(RT.Meta(form))
		default:
			v := FindVar(bc.vm.CurrentNS(), op)
			if v == nil || !v.IsMacro() {
				return x
			}
			fn := v.Deref(bc.vm).(IFn)
			return fn.Invoke(bc.vm, RT.Cons(form, RT.Cons(nil, form.Next()))) // TODO: pass locals
		}
	}
	return x
}

func (bc *ByteCompiler) syntaxQuote(form interface{}) (result interface{}) {
	var sqmeta interface{}
	defer func() {
		if sqmeta != nil {
			result = NewPersistentList(sWITH_META, result, sqmeta)
		}
	}()
	if meta := RT.Meta(form); meta != nil {
		sqmeta = bc.syntaxQuote(meta)
	}
	switch form := form.(type) {
	case *Symbol:
		switch {
		case form.Namespace() != "" || IsSpecialSymbol(form) || isHostSymbol(form):
			return NewPersistentList(sQUOTE, form)
		case strings.HasSuffix(form.Name(), "#"):
			gmap, _ := vGENSYM_ENV.Deref(bc.vm).(PersistentMap)
			if gmap == nil {
				panic(errors.New("gensym literal outside of syntax-quote"))
			}
			name := strings.TrimSuffix(form.Name(), "#")
			sym, _ := gmap.ValAt(name, nil).(*Symbol)
			if sym == nil {
				sym = NewGensym(name)
				vGENSYM_ENV.Set(bc.vm, gmap.Assoc(name, sym))
			}
			return NewPersistentList(sQUOTE, sym.WithMeta(form.Meta()))
		default:
			return NewPersistentList(sQUOTE, NewSymbol(bc.vm.CurrentNS().name.String(), form.Name()))
		}
	case Seq:
		switch {
		case RT.Equiv(form.First(), sUNQUOTE):
			return RT.Second(form)
		case RT.Equiv(form.First(), sUNQUOTE_SPLICING):
			panic(errors.New("unquote-splicing outside of collection"))
		default:
			return bc.syntaxQuoteSeq(form)
		}
	case Seqable:
		return bc.syntaxQuoteSeq(form)
	default:
		return NewPersistentList(sQUOTE, form)
	}
}

func (bc *ByteCompiler) syntaxQuoteSeq(form Seqable) interface{} {
	seq := form.Seq()
	if seq == nil {
		return form
	}
	seqs := []interface{}{sCONCAT}
	curSeq := []interface{}{sLIST}
	for ; seq != nil; seq = seq.Next() {
		switch elem := seq.First().(type) {
		case Seq:
			if RT.Equiv(elem.First(), sUNQUOTE_SPLICING) {
				if len(curSeq) > 1 {
					seqs = append(seqs, NewPersistentList(curSeq...))
				}
				seqs = append(seqs, RT.Second(elem))
				curSeq = []interface{}{sLIST}
			} else {
				curSeq = append(curSeq, bc.syntaxQuote(elem))
			}
		default:
			curSeq = append(curSeq, bc.syntaxQuote(elem))
		}
	}
	if len(curSeq) > 1 {
		seqs = append(seqs, NewPersistentList(curSeq...))
	}
	catseq := NewPersistentList(seqs...)
	switch form.(type) {
	case PersistentVector:
		return NewPersistentList(sVEC, catseq)
	case PersistentMap:
		return NewPersistentList(sAPPLY, sCONJ, EMPTY_MAP, catseq)
	default:
		return catseq
	}
}

func (bc *ByteCompiler) isLocal(sym *Symbol) bool {
	if sym.Namespace() != "" {
		return false
	}
	_, islocal := bc.getLocal(sym)
	return islocal
}

func isHostSymbol(sym *Symbol) bool {
	return sym.Namespace() == "" && sym.Name()[0] == '.'
}

func (bc *ByteCompiler) compileQuote(form Seq) {
	if form == nil || form.Next() != nil {
		panic(fmt.Errorf("quote requires exactly one argument"))
	}
	bc.injectBytecode(&bc_Quoted{form.First()})
}

func (bc *ByteCompiler) compileDef(form Seq) {
	if RT.Count(form) < 1 {
		panic(errors.New("too few arguments to def"))
	}
	if RT.Count(form) > 2 {
		panic(errors.New("too many arguments to def"))
	}
	name := form.First().(*Symbol)
	if name.Namespace() != "" {
		panic(fmt.Errorf("cannot def a namespace-qualified symbol: %v", name))
	}
	dynamic := RT.BoolCast(RT.ValAt(name.Meta(), kDYNAMIC, false))
	v := bc.vm.CurrentNS().Intern(name)
	bc.PushThreadBindings(NewPersistentMap(
		vCURRENT_DEF, v,
	))
	defer bc.PopThreadBindings()
	v.SetDynamicTo(dynamic)
	hasMeta := false
	if name.Meta() != nil {
		hasMeta = true
		bc.asExprs(func() {
			bc.emit(name.Meta())
		})
	}
	hasValue := RT.Count(form) == 2
	if hasValue {
		bc.asExprs(func() {
			bc.emit(RT.Second(form))
		})
	}
	bc.injectBytecode(&bc_Def{v, hasValue, hasMeta})
}

func (bc *ByteCompiler) compileTheVar(form Seq) {
	sym, _ := form.First().(*Symbol)
	if sym == nil {
		panic(fmt.Errorf("var takes one symbol argument"))
	}
	bc.injectBytecode(&bc_Quoted{FindVarOrFail(bc.vm.CurrentNS(), sym)})
}

func (bc *ByteCompiler) compileGo(form Seq) {
	if RT.Count(form) == 1 {
		if fndef, _ := form.First().(Seq); fndef != nil {
			fndef = fndef.(IObj).WithMeta(RT.AssocM(RT.Meta(fndef), kCAPTURE_ENV, true)).(Seq)
			methods := validateFn(fndef)
			if len(methods) == 1 && methods[0].arity == 0 {
				bc.asExprs(func() { bc.injectBytecode(bc.doCompileFn(methods, RT.Meta(fndef), true)) })
				bc.injectBytecode(&bc_Go{})
				return
			}
		}
	}
	panic(errors.New("go* takes a single 0-arg function"))
}

func (bc *ByteCompiler) compileMake(form Seq) {
	switch typ := bc.resolveType(RT.First(form)); typ {
	case nil:
		panic(errors.New("first argument to make must be a type"))
	case tSLICE:
		if RT.Count(form) < 2 {
			panic(errors.New("make slice requires len"))
		}
		if RT.Count(form) > 3 {
			panic(errors.New("too many arguments to make slice"))
		}
		bc.asExprs(func() {
			bc.emit(form.Next().First())
			if form.Next().Next() != nil {
				bc.emit(form.Next().Next().First())
			}
		})
		bc.injectBytecode(&bc_Make{typ, RT.Count(form.Next())})
	case tCHAN:
		if RT.Count(form) > 2 {
			panic(errors.New("too many arguments to make chan"))
		}
		if form.Next() != nil {
			bc.asExprs(func() {
				bc.emit(form.Next().First())
			})
		}
		bc.injectBytecode(&bc_Make{typ, RT.Count(form.Next())})
	default:
		panic(fmt.Errorf("Can only make slice and chan types, got: %v", typ))
	}
}

func validateFn(form Seq) (methods []*fnMethod) {
	// fmt.Printf("validateFn: %v\n", form)
	var name *Symbol
	if sym, ok := RT.First(form).(*Symbol); ok {
		name = sym
		form = form.Next()
	} else {
		name = simpleGensym("anonfn")
	}
	if _, ok := RT.First(form).(PersistentVector); ok {
		form = NewPersistentList(form)
	}

	variadicArity := 0
	largestArity := -1
	arities := make(map[int]bool)

	for ; form != nil; form = form.Next() {
		body := RT.First(form).(Seq)
		params, _ := RT.First(body).(PersistentVector)
		isVariadic := false
		if params == nil {
			panic(fmt.Errorf("bad arglist, expected vector"))
		}
		arity := params.Count()
		for i := 0; i < params.Count(); i++ {
			p, _ := params.Nth(i, nil).(*Symbol)
			if p == nil || p.Namespace() != "" {
				panic(fmt.Errorf("fn params must be unqualified symbols, got: %v (%T)", p, p))
			}
			if RT.Equiv(p, sAMP) {
				if i != params.Count()-2 {
					panic(fmt.Errorf("& must appear before final param"))
				}
				if variadicArity > 0 {
					panic(fmt.Errorf("Can't have two variadic overloads"))
				}
				isVariadic = true
				arity--
				variadicArity = arity
			}
		}

		if arity > MAX_ARITY {
			panic(fmt.Errorf("%d is max arity for now", MAX_ARITY))
		}
		if isVariadic && arity <= largestArity || !isVariadic && variadicArity > 0 && arity >= variadicArity {
			panic(fmt.Errorf("Can't have fixed arity with more params than variadic function"))
		}
		if arities[arity] {
			panic(fmt.Errorf("Can't have 2 overloads with the same arity"))
		}
		if !isVariadic && arity > largestArity {
			largestArity = arity
		}
		arities[arity] = true

		methods = append(methods, &fnMethod{name, arity, isVariadic, params, body.Next(), nil})
	}

	return methods
}

func (bc *ByteCompiler) compileFn(form Seq, meta PersistentMap) {
	// fmt.Printf("compileFn: %v\n", RT.Cons(sFN, form))

	methods := validateFn(form)

	bc.injectBytecode(bc.doCompileFn(methods, meta, true))
}

func (bc *ByteCompiler) doCompileFn(methods []*fnMethod, meta PersistentMap, allowRecur bool) *bc_Fn {
	bcfn := bc_Fn{}

	if len(methods) > 0 {
		bcfn.name = methods[0].name
	}

	if meta != nil {
		bcfn.hasMeta = true
		bc.asExprs(func() {
			bc.emit(meta)
		})
		bcfn.captureEnv = RT.BoolCast(meta.ValAt(kCAPTURE_ENV, false))
		bcfn.rettag = bc.resolveTag(meta)
	}

	closes := bc.closes
	subc := unpoolByteCompiler()
	defer repoolByteCompiler(subc)
	for _, m := range methods {
		*subc = ByteCompiler{
			bc.vmHolder,
			bc.curIndex,
			bc.curIndex,
			cloneLocals(bc.locals),
			closes,
			bc.dbg,
			bc.optimize,
		}
		subc.compileMethod(&bcfn, m, allowRecur)
		closes = subc.closes
	}
	bc.asExprs(func() {
		for _, c := range closes {
			bc.emit(c)
		}
	})
	bcfn.closes = len(closes)
	if bc.optimize {
		return (&optimizer{bc: bc}).optimize(&bcfn)
	} else {
		return &bcfn
	}
}

func (bc *ByteCompiler) compileMethod(bcfn *bc_Fn, m *fnMethod, allowRecur bool) {
	bc.addParams(m.name, RT.Seq(m.params))
	if m.isVariadic {
		bcfn.restArity = m.arity
	}
	m.code = make([]interface{}, 0)
	curname, _ := vCURRENT_FN_NAME.Deref(bc.vm).(string)
	if m.name != nil {
		if curname == "" {
			curname = m.name.String()
		} else {
			curname += "$" + m.name.String()
		}
	}
	if allowRecur {
		bc.PushThreadBindings(NewPersistentMap(
			vCURRENT_FN, &m.code,
			vCURRENT_FN_NAME, curname,
			vRECUR_PC, 0,
			vRECUR_LOCAL_IX, bc.baseIndex+1,
			vRECUR_ARITY, m.arity,
		))
	} else {
		bc.PushThreadBindings(NewPersistentMap(
			vCURRENT_FN, &m.code,
			vCURRENT_FN_NAME, curname,
		))
	}
	defer bc.PopThreadBindings()
	body := RT.Seq(m.source)
	if body == nil {
		bc.asReturn(func() {
			bc.emit(nil)
		})
	} else {
		bc.asStatements(func() {
			for ; body.Next() != nil; body = body.Next() {
				bc.emit(body.First())
			}
		})
		bc.asReturn(func() {
			bc.emit(body.First())
		})
	}
	bcfn.bodies[m.arity] = m.code
}

func (bc *ByteCompiler) currentFnBody() *[]interface{} {
	return vCURRENT_FN.Deref(bc.vm).(*[]interface{})
}

func (bc *ByteCompiler) currentPC() int {
	return len(*bc.currentFnBody())
}

func (bc *ByteCompiler) context() *Keyword {
	return vCONTEXT.Deref(bc.vm).(*Keyword)
}

// func (bc *ByteCompiler) emit(form interface{}) (bytecode interface{}) {
// 	bytecode = bc.ByteCompile(form)
// 	bc.injectBytecode(bytecode)
// 	return
// }

func (bc *ByteCompiler) asExprs(f func()) {
	bc.PushThreadBindings(NewPersistentMap(vCONTEXT, kEXPR))
	defer bc.PopThreadBindings()
	f()
}

func (bc *ByteCompiler) asStatements(f func()) {
	bc.PushThreadBindings(NewPersistentMap(vCONTEXT, kSTATEMENT))
	defer bc.PopThreadBindings()
	f()
}

func (bc *ByteCompiler) asReturn(f func()) {
	bc.PushThreadBindings(NewPersistentMap(vCONTEXT, kRETURN))
	defer bc.PopThreadBindings()
	f()
}

func isControlFlow(bytecode interface{}) bool {
	switch bytecode.(type) {
	case *bc_PushLocal, *bc_PopLocals, *bc_PopReturn,
		*bc_Jump, *bc_Branch, *bc_Recur,
		*bc_PushThreadBindings, *bc_PopThreadBindings:
		return true
	default:
		return false
	}
}

func (bc *ByteCompiler) injectBytecode(bytecode interface{}) bool {
	fnbody := bc.currentFnBody()
	if fnbody == nil {
		panic(errors.New("INTERNAL ERROR: no current fn"))
	}
	if bytecode == nil {
		return false
	}
	if isControlFlow(bytecode) || bc.context() != kSTATEMENT {
		*fnbody = append(*fnbody, bytecode)
	} else {
		*fnbody = append(*fnbody, bytecode, &bc_PopReturn{})
	}
	return true
}

func (bc *ByteCompiler) sourceInfo(meta PersistentMap) sourceInfo {
	file := RT.ValAt(meta, kFILE, "<unknown file>").(string)
	line := RT.ValAt(meta, kLINE, 0).(int)
	col := RT.ValAt(meta, kCOLUMN, 0).(int)
	fnname, _ := vCURRENT_FN_NAME.Deref(bc.vm).(string)
	if fnname == "" {
		fnname = "<top level>"
	} else if def, ok := vCURRENT_DEF.Deref(bc.vm).(*Var); ok {
		fnname = def.ns.name.String() + "/" + fnname
	}
	return sourceInfo{file, line, col, fnname}
}

func (bc *ByteCompiler) compileInvoke(form Seq) {
	meta := form.(IMeta).Meta()
	bc.asExprs(func() {
		for pseq := RT.Seq(form); pseq != nil; pseq = pseq.Next() {
			bc.emit(pseq.First())
		}
	})
	bc.injectBytecode(&bc_Invoke{RT.Count(form), bc.sourceInfo(meta)})
}

func (bc *ByteCompiler) compileDot(form Seq, meta PersistentMap) {
	if RT.Count(form) < 2 {
		panic(errors.New("dot special form requires a receiver and field/method"))
	}
	methodcall, _ := RT.Second(form).(Seq)
	if methodcall == nil && RT.Count(form) > 2 {
		methodcall = form.Next()
	}
	var methodOrField *Symbol
	var methodArgs Seq
	if methodcall != nil {
		methodOrField, _ = methodcall.First().(*Symbol)
		methodArgs = methodcall.Next()
	} else {
		methodOrField, _ = RT.Second(form).(*Symbol)
	}
	if methodOrField == nil {
		panic(fmt.Errorf("field/method name must be a symbol"))
	}
	bc.asExprs(func() {
		bc.emit(RT.First(form))
		for args := methodArgs; args != nil; args = args.Next() {
			bc.emit(args.First())
		}
	})
	tag := bc.resolveTag(RT.Meta(RT.First(form)))
	bc.injectBytecode(&bc_FieldOrMethodCall{
		RT.Count(methodArgs) + 1,
		NewKeyword(methodOrField.NameSym()),
		tag,
		bc.sourceInfo(meta),
	})

}

func (bc *ByteCompiler) resolveType(x interface{}) *Type {
	// fmt.Fprintf(os.Stderr, "resolveType: %v (%T)\n", x, x)
	switch x := x.(type) {
	case *Type:
		return x
	case *Symbol:
		if imp, _ := ResolveName(bc.vm.CurrentNS(), x).(*Import); imp != nil {
			typ, _ := imp.Object().(*Type)
			return typ
		}
	}
	return nil
}

func (bc *ByteCompiler) resolveTag(meta PersistentMap) *Type {
	return bc.resolveType(RT.ValAt(meta, kTAG, nil))
}

func (bc *ByteCompiler) compileIf(form Seq) {
	if RT.Count(form) < 2 {
		panic(errors.New("too few arguments to if"))
	}
	if RT.Count(form) > 3 {
		panic(errors.New("too many arguments to if"))
	}
	branch := &bc_Branch{}
	jump := &bc_Jump{}
	bc.asExprs(func() {
		bc.emit(RT.Nth(form, 0, nil))
	})
	var emitAs func(func())
	if bc.context() == kRETURN {
		emitAs = bc.asReturn
	} else {
		emitAs = bc.asExprs
	}
	bc.injectBytecode(branch)
	emitAs(func() {
		bc.emit(RT.Nth(form, 1, nil))
	})
	bc.injectBytecode(jump)
	branch.elsePC = bc.currentPC()
	emitAs(func() {
		bc.emit(RT.Nth(form, 2, nil))
	})
	jump.pc = bc.currentPC()
	if bc.context() == kSTATEMENT {
		bc.injectBytecode(&bc_PopReturn{})
	}
}

func (bc *ByteCompiler) validateLet(form Seq, isbinding bool) (bindings PersistentVector, body Seq) {
	binds, _ := RT.First(form).(PersistentVector)
	if binds == nil {
		panic(errors.New("bad binding form, expected vector"))
	}
	if binds.Count()%2 != 0 {
		panic(errors.New("bad binding form, expected matched symbol expression pairs"))
	}
	for i := 0; i < binds.Count(); i += 2 {
		sym, _ := binds.Nth(i, nil).(*Symbol)
		if sym == nil {
			panic(fmt.Errorf("bad binding form, expected symbol, got: %v", binds.Nth(i, nil)))
		}
		if !isbinding && sym.Namespace() != "" {
			panic(fmt.Errorf("can't let qualified name: %v", sym))
		}
	}
	return binds, form.Next()
}

func (bc *ByteCompiler) compileLet(form Seq, isloop bool) {
	binds, body := bc.validateLet(form, false)
	subc := unpoolByteCompiler()
	defer repoolByteCompiler(subc)
	*subc = *bc
	subc.locals = cloneLocals(bc.locals)
	// subc.closes = bc.closes

	subc.asExprs(func() {
		for i := 0; i < binds.Count(); i += 2 {
			bind := binds.Nth(i, nil).(*Symbol)
			subc.emit(binds.Nth(i+1, nil))
			subc.injectBytecode(&bc_PushLocal{bind})
			subc.setLocal(bind, subc.curIndex)
			subc.curIndex++
		}
	})

	if isloop {
		subc.PushThreadBindings(NewPersistentMap(
			vRECUR_PC, subc.currentPC(),
			vRECUR_LOCAL_IX, subc.curIndex-binds.Count()/2,
			vRECUR_ARITY, binds.Count()/2,
		))
		defer subc.PopThreadBindings()
	}

	if body == nil {
		if bc.context() != kSTATEMENT {
			subc.emit(nil)
		}
		if binds.Count() > 0 {
			subc.injectBytecode(&bc_PopLocals{binds.Count() / 2})
		}
	} else {
		subc.asStatements(func() {
			for ; body.Next() != nil; body = body.Next() {
				subc.emit(body.First())
			}
		})
		if isloop {
			subc.asReturn(func() {
				subc.emit(body.First())
			})
			if bc.context() == kSTATEMENT {
				subc.injectBytecode(&bc_PopReturn{})
			}
		} else {
			subc.emit(body.First())
		}
		if binds.Count() > 0 {
			subc.injectBytecode(&bc_PopLocals{binds.Count() / 2})
		}
	}
	bc.closes = subc.closes
}

func (bc *ByteCompiler) compileRecur(form Seq) {
	if vRECUR_PC.Deref(bc.vm) == nil {
		panic(errors.New("recur outside of function or loop"))
	}
	if bc.context() != kRETURN {
		panic(errors.New("recur must be in tail position"))
	}
	recurPc := vRECUR_PC.Deref(bc.vm).(int)
	recurLocalIx := vRECUR_LOCAL_IX.Deref(bc.vm).(int)
	recurArity := vRECUR_ARITY.Deref(bc.vm).(int)
	if RT.Count(form) != recurArity {
		panic(fmt.Errorf("incorrect recur arity, expected: %d, got: %d", recurArity, RT.Count(form)))
	}
	bc.asExprs(func() {
		for ; form != nil; form = form.Next() {
			bc.emit(form.First())
		}
		bc.injectBytecode(&bc_Recur{recurPc, recurLocalIx - bc.baseIndex, recurArity})
	})
	// fmt.Printf("end compile recur: bc.curIndex = %d, recurLocalIx = %d, i = %d\n", bc.curIndex, recurLocalIx, i)
}

func (bc *ByteCompiler) compileDo(form Seq) {
	if form == nil {
		bc.injectBytecode(&bc_Quoted{nil})
		return
	}
	bc.asStatements(func() {
		for ; form.Next() != nil; form = form.Next() {
			bc.emit(form.First())
		}
	})
	bc.emit(form.First())
}

func (bc *ByteCompiler) compileNew(form Seq) {
	if RT.Count(form) != 1 {
		panic(errors.New("new takes one argument"))
	}
	sym, _ := form.First().(*Symbol)
	if sym == nil {
		panic(errors.New("argument to new must be a symbol"))
	}
	if imp, _ := ResolveName(bc.vm.CurrentNS(), sym).(*Import); imp != nil {
		typ, _ := imp.Object().(*Type)
		if typ != nil && typ.Kind() != reflect.Interface {
			bc.injectBytecode(&bc_New{typ})
			return
		}
	} else {
		panic(fmt.Errorf("cannot resolve symbol: %v in this context", sym))
	}
	panic(errors.New("argument to new must be a concrete type"))
}

// func (bc *ByteCompiler) compileBinding(form Seq) {
// 	binds, body := bc.validateLet(form, true)
// 	bindvars := make([]*Var, 0, binds.Count()/2)
// 	bc.asExprs(func() {
// 		for bs := binds.Seq(); bs != nil; bs = bs.Next().Next() {
// 			bindvars = append(bindvars, FindVarOrFail(bc.vm.CurrentNS(), bs.First().(*Symbol)))
// 			bc.emit(bs.Next().First())
// 		}
// 	})
// 	bc.injectBytecode(&bc_PushThreadBindings{bindvars})
// 	if body != nil {
// 		bc.asStatements(func() {
// 			for ; body.Next() != nil; body = body.Next() {
// 				bc.emit(body.First())
// 			}
// 		})
// 		bc.asExprs(func() {
// 			bc.emit(body.First())
// 		})
// 	}
// 	bc.injectBytecode(&bc_PopThreadBindings{})
// }

func (bc *ByteCompiler) compileAssign(form Seq, meta PersistentMap) {
	if RT.Count(form) != 2 {
		panic(errors.New("set! requires a receiver and a value"))
	}
	switch recv := bc.Macroexpand(form.First()).(type) {
	case Seq:
		if RT.Equiv(recv.First(), sDOT) {
			bc.compileAssignField(recv, RT.Second(form), meta)
			return
		}
	case *Symbol:
		bc.compileAssignVar(recv, RT.Second(form), meta)
		return
	}
	panic(errors.New("set! accepts struct field and var symbol receivers"))
}

func (bc *ByteCompiler) compileAssignField(recv Seq, val interface{}, meta PersistentMap) {
	if RT.Count(recv) != 3 {
		panic(errors.New("invalid host field expression in set!"))
	}
	field, _ := RT.Nth(recv, 2, nil).(*Symbol)
	if field == nil {
		panic(errors.New("field must be a symbol"))
	}
	bc.asExprs(func() {
		bc.emit(RT.Second(recv))
		bc.emit(val)
	})
	bc.injectBytecode(&bc_SetField{NewKeyword(field), bc.sourceInfo(meta)})
}

var (
	kSET       *Keyword = ParseKeyword("Set")
	setVarFn   *DirectFn
	setVarOnce sync.Once
)

func (bc *ByteCompiler) compileAssignVar(recv *Symbol, val interface{}, meta PersistentMap) {
	setVarOnce.Do(func() {
		setVarFn = LookupType(vartype).Methods().ValAt(kSET, nil).(*DirectFn)
	})
	bc.asExprs(func() {
		bc.injectBytecode(&bc_Quoted{setVarFn})
		bc.compileTheVar(NewPersistentList(recv))
		bc.emit(val)
	})
	bc.injectBytecode(&bc_Invoke{3, bc.sourceInfo(meta)})
}
