package lang

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"sync"
)

type Evaler struct {
	vmHolder
	code    []interface{}
	pc      int
	params  stack
	closes  []interface{}
	returns stack
}

var (
	evalerpool = sync.Pool{
		New: func() interface{} { return &Evaler{} },
	}
)

var (
	niltype  *Type         = WrapType(reflect.TypeOf((*interface{})(nil)).Elem())
	nilvalue reflect.Value = reflect.Zero(niltype.Type)
)

type invokeError struct {
	err       error
	backtrace []sourceInfo
}

func (e *invokeError) Error() string {
	return fmt.Sprintf("%s: %s", e.backtrace[0], e.err)
}

func invokeErrorAppendBacktrace(e interface{}, info sourceInfo) *invokeError {
	switch e := e.(type) {
	case *invokeError:
		e.backtrace = append(e.backtrace, info)
		return e
	default:
		return &invokeError{toError(e), []sourceInfo{info}}
	}
}

func unpoolEvaler() *Evaler {
	return evalerpool.Get().(*Evaler)
}

func repoolEvaler(ev *Evaler) {
	*ev = Evaler{}
	evalerpool.Put(ev)
}

func newEvaler(vm *VM) *Evaler {
	evaler := unpoolEvaler()
	*evaler = Evaler{vmHolder{vm}, nil, 0, makeStack(), nil, makeStack()}
	return evaler
}

// release re-pools the evaler AND its stacks
// MUST ONLY be called on the top-level evaler
// sub-evalers can simply be re-pooled
func (ev *Evaler) release() {
	ev.params.release()
	ev.returns.release()
	repoolEvaler(ev)
}

func (ev *Evaler) debug(fmtstr string, args ...interface{}) {
	if RT.BoolCast(vEVAL_DEBUG.Deref(ev.vm)) {
		fmt.Fprintf(os.Stderr, fmtstr, args...)
	}
}

func (ev *Evaler) step() (more bool) {
	more = true
	if ev.pc >= len(ev.code) {
		return false
	}
	bytecode := ev.code[ev.pc]
	ev.pc++
	// ev.debug("step: %v (pc: %d)\n", bytecode, ev.pc)

	switch bytecode := bytecode.(type) {
	case *bc_DerefVar:
		ev.returns.push(bytecode.v.Deref(ev.vm))
	case *bc_Def:
		if bytecode.hasValue {
			bytecode.v.BindRoot(ev.returns.pop())
		}
		if bytecode.hasMeta {
			bytecode.v.ResetMeta(ev.returns.pop().(PersistentMap))
		}
		ev.returns.push(bytecode.v)
	case *bc_Param:
		// ev.debug("Param: %v (idx %d), ev.params.len(): %d\n", bytecode.name, bytecode.index, ev.params.len())
		ev.returns.push(ev.params.at(bytecode.index))
	case *bc_ClosedOver:
		// fmt.Printf("ev.closes: %v, bytecode.index: %v\n", ev.closes, bytecode.index)
		ev.returns.push(ev.closes[bytecode.index])
	case *bc_Quoted:
		ev.returns.push(bytecode.value)
	case *bc_EmptyList:
		if bytecode.hasMeta {
			ev.returns.push(EMPTY_LIST.WithMeta(ev.returns.pop().(PersistentMap)))
		} else {
			ev.returns.push(EMPTY_LIST)
		}
	case *bc_Vector:
		vec := NewPersistentVector(ev.returns.peekN(bytecode.count)...)
		ev.returns.popN(bytecode.count)
		if bytecode.hasMeta {
			ev.returns.push(vec.WithMeta(ev.returns.pop().(PersistentMap)))
		} else {
			ev.returns.push(vec)
		}
	case *bc_Map:
		themap := NewPersistentMap(ev.returns.peekN(bytecode.count * 2)...)
		ev.returns.popN(bytecode.count * 2)
		if bytecode.hasMeta {
			ev.returns.push(themap.WithMeta(ev.returns.pop().(PersistentMap)))
		} else {
			ev.returns.push(themap)
		}
	case *bc_Set:
		set := NewPersistentSet(ev.returns.peekN(bytecode.count)...)
		ev.returns.popN(bytecode.count)
		if bytecode.hasMeta {
			ev.returns.push(set.WithMeta(ev.returns.pop().(PersistentMap)))
		} else {
			ev.returns.push(set)
		}
	case *bc_Fn:
		var closes []interface{}
		if bytecode.closes > 0 {
			closes = make([]interface{}, bytecode.closes)
			copy(closes, ev.returns.peekN(bytecode.closes))
			ev.returns.popN(bytecode.closes)
		}
		var meta PersistentMap
		if bytecode.hasMeta {
			meta = ev.returns.pop().(PersistentMap)
		}
		// fmt.Printf("evaledCloses: %v\n", evaledCloses)
		fn := &Fn{
			name:      bytecode.name,
			closes:    closes,
			restArity: bytecode.restArity,
			bodies:    bytecode.bodies,
			metadata:  metadata{meta},
		}
		if bytecode.captureEnv {
			fn.threadBindings = ev.vm.bindings
		}
		ev.returns.push(fn)
	case *bc_Invoke:
		invokeValues := ev.returns.peekN(bytecode.count)
		switch fn := invokeValues[0].(type) {
		case *DirectFn:
			ev.evalInvokeDirect(fn, invokeValues[1:], true)
		case *Fn:
			ev.evalInvokeFn(fn, invokeValues[1:])
		case *ReflectiveFn:
			ev.evalInvokeReflectiveFn(fn, invokeValues[1:])
		case IFn:
			ret := fn.Invoke(ev.vm, RT.Seq(invokeValues[1:]))
			ev.returns.popN(bytecode.count)
			ev.returns.push(ret)
		default:
			panic(fmt.Errorf("cannot invoke %v (%T)", invokeValues[0], invokeValues[0]))
		}
	case *bc_Branch:
		cond := ev.returns.pop()
		// fmt.Printf("branch cond: %v\n", cond)
		if !RT.BoolCast(cond) {
			ev.pc = bytecode.elsePC
		}
	case *bc_PushLocal:
		ev.params.push(ev.returns.pop())
	case *bc_PopLocals:
		ev.params.popN(bytecode.count)
	case *bc_Jump:
		ev.pc = bytecode.pc
	case *bc_PopReturn:
		ev.returns.pop()
	case *bc_Recur:
		ev.moveReturnsToParams(bytecode.firstLocal, bytecode.arity)
		ev.pc = bytecode.pc
	case *bc_PushThreadBindings:
		bmap := ev.returns.pop().(PersistentMap)
		ev.PushThreadBindings(bmap)
	case *bc_PopThreadBindings:
		ev.PopThreadBindings()
	case *bc_Go:
		fn := ev.returns.pop().(IFn)
		go func() {
			defer func() {
				if e := recover(); e != nil {
					vmid := "<nil VM>"
					if vm := fn.VM(nil); vm != nil {
						vmid = fmt.Sprintf("#VM[%d]", vm.tcid)
					}
					vERR.Deref(fn.VM(nil)).(io.StringWriter).WriteString(
						fmt.Sprintf("unhandled error on %s\n\n", vmid),
					)
					ev.vm.printError(e)
				}
			}()
			fn.Invoke(nil, nil)
		}()
		ev.returns.push(nil)
	case *bc_FieldOrMethodCall:
		callArgs := ev.returns.peekN(bytecode.count)
		if ev.tryDirectMethod(callArgs, bytecode) {
			return
		}
		recv := callArgs[0]
		// fmt.Fprintf(os.Stderr, "--- REFLECTION: %v.%v --- ", recv, bytecode.selector.Name())
		recvval := valueOf(recv)
		if recv := reflect.Indirect(recvval); recv.Kind() == reflect.Struct {
			if field := recv.FieldByName(bytecode.selector.Name()); field.IsValid() {
				ev.returns.pop() // recv
				ev.returns.push(field.Interface())
				return
			}
		}
		if method := recvval.MethodByName(bytecode.selector.Name()); method.IsValid() {
			fn := mkReflectiveFn(bytecode.selector.Name(), method)
			ev.evalInvokeReflectiveFn(fn, callArgs[1:])
			return
		}
		panic(fmt.Errorf("no such field or method %s on type %v", bytecode.selector.Name(), recvval.Type()))
	case *bc_SetField:
		val := ev.returns.pop()
		recv := ev.returns.pop()
		recvval := valueOf(recv)
		if recv := reflect.Indirect(recvval); recv.Kind() == reflect.Struct {
			if field := recv.FieldByName(bytecode.field.Name()); field.IsValid() {
				field.Set(massagedValueOf(field.Type(), val))
				ev.returns.push(val)
				return
			}
		}
		panic(fmt.Errorf("no such field %s on type %v", bytecode.field.Name(), recvval.Type()))
	case *bc_Make:
		switch bytecode.typ {
		case tSLICE:
			var len, cap int
			if bytecode.count == 1 {
				len = int(ev.returns.pop().(Int))
				cap = len
			} else {
				cap = int(ev.returns.pop().(Int))
				len = int(ev.returns.pop().(Int))
			}
			slice := reflect.MakeSlice(bytecode.typ.Type, len, cap)
			ev.returns.push(slice.Interface())
		case tCHAN:
			var len int
			if bytecode.count == 1 {
				len = int(ev.returns.pop().(Int))
			}
			thechan := reflect.MakeChan(bytecode.typ.Type, len)
			ev.returns.push(thechan.Interface())
		default:
			panic(fmt.Errorf("INTERNAL ERROR: invalid type argument to make: %v", bytecode.typ))
		}
	case *bc_New:
		if bytecode.typ.Kind() == reflect.Ptr {
			ev.returns.push(reflect.New(bytecode.typ.Elem()).Interface())
		} else {
			ev.returns.push(reflect.New(bytecode.typ).Interface())
		}
	default:
		panic(fmt.Errorf("cannot eval type: %T", bytecode))
	}
	return true
}

func (ev *Evaler) moveReturnsToParams(firstParamIdx, count int) {
	if firstParamIdx == ev.params.len() {
		ev.params.pushN(ev.returns.peekN(count))
	} else {
		ev.params.reset(firstParamIdx, ev.returns.peekN(count))
	}
	ev.returns.popN(count)
}

func (ev *Evaler) makeSubEvaler(fn *Fn, params []interface{}) *Evaler {
	isRestCall := fn.ArityCheck(len(params))

	// ev.debug("top of doInvoke -- len(ev.returns): %d\n", len(ev.returns))
	paramCount := len(params)
	if isRestCall {
		paramCount = fn.restArity
		if len(params) < fn.restArity {
			ev.returns.push(nil)
		} else {
			// Rest params must not alias the param stack, they will be overwritten
			paramSlice := make([]interface{}, len(params)-fn.restArity+1)
			copy(paramSlice, params[fn.restArity-1:])
			ev.returns.popN(len(params) - fn.restArity + 1)
			ev.returns.push(newInterfaceSliceSeq(paramSlice))
		}
	}
	body := fn.bodies[paramCount]

	subev := unpoolEvaler()
	*subev = Evaler{
		vmHolder: vmHolder{fn.VM(ev.vm)},
		code:     body,
		pc:       0,
		params:   ev.returns.newFrame(paramCount + 1), //sub-fn gets returns as params, no copying
		closes:   fn.closes,
		returns:  ev.params.newFrame(0),
	}
	return subev
}

func (ev *Evaler) evalInvokeFn(fn *Fn, params []interface{}) {
	// ev.debug("invoke %v: %v\n", fn, fnev.params)
	fnev := ev.makeSubEvaler(fn, params)
	ret := fnev.run()
	// child's RETURN stack was this fn's PARAM stack
	ev.params.popFrame(fnev.returns)
	// child's PARAM stack was this fn's RETURN stack
	ev.returns.popFrame(fnev.params)
	ev.returns.push(ret)
	repoolEvaler(fnev)
}

func (ev *Evaler) run() interface{} {
	defer func() {
		bytecode := ev.code[ev.pc-1]
		if e := recover(); e != nil {
			if haver, hasinfo := bytecode.(sourceInfoHaver); hasinfo {
				sinfo := haver.SourceInfo()
				panic(invokeErrorAppendBacktrace(e, sinfo))
			} else {
				panic(invokeErrorAppendBacktrace(e, sourceInfo{}))
			}
		}
	}()
	for ev.step() {
	}
	ret := ev.returns.pop()
	if !ev.returns.empty() {
		panic(fmt.Errorf(
			"INTERNAL ERROR: return stack not empty on function return! %v",
			ev.returns.view(),
		))
	}
	return ret
}

func (ev *Evaler) evalInvokeReflectiveFn(fn *ReflectiveFn, bcParams []interface{}) {
	ev.debug("---- REFLECTION ---- (%s)\n", fn.name)
	arityCheck(fn.arity, len(bcParams))
	params := ev.buildReflectParams(fn, bcParams)
	ev.returns.popN(len(bcParams) + 1)
	resultValues := fn.fn.Call(params)
	ev.returns.push(convertReflectResults(resultValues))
}

func convertReflectResults(resultValues []reflect.Value) interface{} {
	if len(resultValues) == 0 {
		return nil
	} else if len(resultValues) == 1 {
		return massagedReturnValueOf(resultValues[0])
	} else {
		results := make([]interface{}, 0, len(resultValues))
		for _, r := range resultValues {
			results = append(results, massagedReturnValueOf(r))
		}
		return PersistentVectorBuilder.CreateOwning(results...)
	}
}

func (ev *Evaler) buildReflectParams(fn *ReflectiveFn, bcParams []interface{}) (params []reflect.Value) {
	if fn.passVM {
		params = make([]reflect.Value, 0, len(bcParams)+1)
		params = append(params, valueOf(ev.vm))
	} else {
		params = make([]reflect.Value, 0, len(bcParams))
	}
	for i, p := range bcParams {
		params = append(params, massagedValueOf(fn.ArgType(i), p))
	}
	return params
}

func (ev *Evaler) evalInvokeDirect(fn *DirectFn, params []interface{}, fnIsOnStack bool) {
	ret := fn.InvokeDirect(ev.vm, params...)
	if fnIsOnStack {
		ev.returns.popN(len(params) + 1)
	} else {
		ev.returns.popN(len(params))
	}
	ev.returns.push(ret)
}

func (ev *Evaler) tryDirectMethod(callArgs []interface{}, bytecode *bc_FieldOrMethodCall) bool {
	recv := callArgs[0]
	if recv == nil {
		return false
	}
	rtype := reflect.TypeOf(recv)
	// fmt.Fprintf(os.Stderr, "scanning cache\n")
	cachedType := LookupType(rtype)
	if cachedType == nil {
		cachedType = CacheType(WrapType(rtype), nil, nil)
	}
	cachedMethod := RT.ValAt(cachedType.Methods(), bytecode.selector, nil)
	if cachedMethod == nil {
		methodImpls := LookupMethod(bytecode.selector)
		for mseq := methodImpls.Values(); mseq != nil; mseq = mseq.Next() {
			dfn := mseq.First().(*DirectFn)
			if dfn.IsMethodFor(recv) {
				cachedMethod = dfn
				cachedType.methods.Swap(func(ms interface{}) interface{} {
					return RT.Assoc(ms, bytecode.selector, dfn)
				})
				break
			}
		}
		if cachedMethod == nil {
			cachedType.methods.Swap(func(ms interface{}) interface{} {
				return RT.Assoc(ms, bytecode.selector, kNO_IMPL)
			})
		}
	}
	directfn, _ := cachedMethod.(*DirectFn)
	if directfn == nil {
		return false
	}
	ev.evalInvokeDirect(directfn, callArgs, false)
	return true
}

// var (
// 	intType   reflect.Type = reflect.TypeOf(int(0))
// 	int8Type  reflect.Type = reflect.TypeOf(int8(0))
// 	int16Type reflect.Type = reflect.TypeOf(int16(0))
// 	int32Type reflect.Type = reflect.TypeOf(int32(0))
// 	int64Type reflect.Type = reflect.TypeOf(int64(0))

// 	uintType   reflect.Type = reflect.TypeOf(uint(0))
// 	uint8Type  reflect.Type = reflect.TypeOf(uint8(0))
// 	uint16Type reflect.Type = reflect.TypeOf(uint16(0))
// 	uint32Type reflect.Type = reflect.TypeOf(uint32(0))
// 	uint64Type reflect.Type = reflect.TypeOf(uint64(0))

// 	float64type reflect.Type = reflect.TypeOf(float64(0))
// )

func massagedReturnValueOf(v reflect.Value) interface{} {
	k := v.Kind()
	switch {
	case k >= reflect.Chan && k <= reflect.Slice:
		if v.IsNil() {
			return nil
		}
		return v.Interface()
	// case kindIsInt(k):
	// 	return v.Int()
	// case kindIsUint(k):
	// 	// TODO: handle overflow?
	// 	return int64(v.Uint())
	// case k == reflect.Float32 || k == reflect.Float64:
	// 	return v.Float()
	case k == reflect.Struct || k == reflect.Array:
		ptr := reflect.New(v.Type())
		ptr.Elem().Set(v)
		return ptr.Interface()
	default:
		if num := valueToNumber(v); num != nil {
			return num
		}
		return v.Interface()
	}
}

func massagedValueOf(t reflect.Type, x interface{}) reflect.Value {
	if reflect.TypeOf(x) == t {
		return reflect.ValueOf(x)
	}
	if x == nil {
		if t.Kind() >= reflect.Chan && t.Kind() <= reflect.Slice {
			return reflect.Zero(t)
		}
		panic(fmt.Errorf("Can't create nil %v", t))
	}
	if t.Kind() == reflect.Struct || t.Kind() == reflect.Array {
		xval := reflect.ValueOf(x)
		if xval.Kind() == reflect.Ptr {
			return reflect.ValueOf(x).Elem()
		}
		panic(fmt.Errorf("Can't convert %v to %v", xval.Type(), t))
	}
	return reflect.ValueOf(x).Convert(t)
}

func valueOf(x interface{}) reflect.Value {
	switch x := x.(type) {
	case nil:
		return nilvalue
	default:
		return reflect.ValueOf(x)
	}
}
