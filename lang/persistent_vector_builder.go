package lang

type PersistentVectorBuilderType struct{}

var (
	PersistentVectorBuilder *PersistentVectorBuilderType = &PersistentVectorBuilderType{}
)

func (_ *PersistentVectorBuilderType) CreateOwning(elems ...interface{}) PersistentVector {
	if len(elems) == 0 {
		return EMPTY_VECTOR
	}
	return &persistentVector{tail: elems}
}

func (_ *PersistentVectorBuilderType) FromSeq(elems Seq) PersistentVector {
	elslice := make([]interface{}, 0)
	for ; elems != nil; elems = elems.Next() {
		elslice = append(elslice, elems.First())
	}
	return PersistentVectorBuilder.CreateOwning(elslice...)
}

func (_ *PersistentVectorBuilderType) PrintNative() string {
	return "lang.PersistentVectorBuilder"
}
