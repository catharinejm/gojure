package lang

import (
	"fmt"
	"io"
	"reflect"
	"strings"
	"unicode"
	"unicode/utf8"
)

type NativeCompiler struct {
	vmHolder
	io.StringWriter
	packagename string
	nsname      *Symbol
	imports     []import_
	vars        []varDef
}

type import_ struct {
	name, path string
}

type varDef struct {
	name    *Symbol
	goname  string
	root    interface{}
	dynamic bool
	meta    PersistentMap
}

func CompileNS(vm *VM, ns *Namespace, output io.StringWriter) {
	nc := &NativeCompiler{vmHolder: vmHolder{vm}, StringWriter: output}
	nc.compileNS(ns)
}

func (nc *NativeCompiler) compileNS(ns *Namespace) {
	nc.packagename = Munge(ns.name.String())
	nc.nsname = ns.name
	nc.imports = []import_{
		{"lang", "gitlab.com/catharinejm/gojure/lang"},
		{"", "reflect"},
	}
	nc.buildVarDefs(ns.GetMappings())
	nc.WriteString("package " + nc.packagename + "\n\n")
	nc.writeImports()
	nc.writeVarDecls()
	nc.writeInit()
}

func (nc *NativeCompiler) writeImports() {
	nc.WriteString("import (\n")
	for _, imp := range nc.imports {
		nc.WriteString(imp.name)
		nc.WriteString(" ")
		nc.WriteString(fmt.Sprintf("%#v", imp.path))
		nc.WriteString("\n")
	}
	nc.WriteString(")\n")
}

func (nc *NativeCompiler) writeVarDecls() {
	nc.WriteString("var (\n")
	nc.WriteString("thisNS *lang.Namespace\n")
	for _, vd := range nc.vars {
		nc.WriteString(vd.goname)
		nc.WriteString(" *lang.Var\n")
	}
	nc.WriteString(")\n")
}

func (nc *NativeCompiler) writeInit() {
	nc.WriteString("func init() {\n")

	nc.WriteString("thisNS = lang.FindOrCreateNamespace(")
	nc.emitConstant(nc.nsname)
	nc.WriteString(")\n")

	for _, vd := range nc.vars {
		nc.WriteString(vd.goname)
		nc.WriteString(" = ")
		nc.WriteString("lang.InternVar(thisNS, ")
		nc.emitConstant(vd.name)
		nc.WriteString(", ")
		nc.emitConstant(vd.root)
		nc.WriteString(")")
		if vd.dynamic {
			nc.WriteString(".SetDynamic()")
		}
		nc.WriteString("\n")
		if vd.meta != nil {
			nc.WriteString(vd.goname + ".ResetMeta(")
			nc.emitConstant(vd.meta)
			nc.WriteString(")\n")
		}
	}

	nc.WriteString("}\n")
}

func (nc *NativeCompiler) buildVarDefs(vars Associative) {
	for vs := RT.Seq(vars); vs != nil; vs = vs.Next() {
		entry := vs.First().(MapEntry)
		name := entry.Key().(*Symbol)
		v := entry.Value().(*Var)
		if v == vIN || v == vOUT || v == vERR {
			continue
		}
		nc.vars = append(nc.vars, varDef{
			name:    name,
			goname:  "Var_" + Munge(name.String()),
			root:    v.Deref(nil),
			dynamic: v.IsDynamic(),
			meta:    v.Meta(),
		})
	}
}

func (nc *NativeCompiler) emitConstant(c interface{}) {
	switch c := c.(type) {
	case nil:
		nc.WriteString("nil")
	case *Symbol:
		nc.WriteString("lang.ParseSymbol(\"" + c.String() + "\")")
	case *Keyword:
		nc.WriteString("lang.ParseKeyword(\"" + c.sym.String() + "\")")
	case *Var:
		nc.WriteString("lang.FindVarOrFail(")
		nc.emitConstant(c.ns)
		nc.WriteString(",")
		nc.emitConstant(c.sym)
		nc.WriteString(")")
	case *Namespace:
		nc.WriteString("lang.FindOrCreateNamespace(")
		nc.emitConstant(c.name)
		nc.WriteString(")")
	case PersistentMap:
		nc.WriteString("lang.NewPersistentMap(")
		nc.emitMapPairConstants(c)
		nc.WriteString(")")
	case PersistentVector:
		nc.WriteString("lang.NewPersistentVector(")
		nc.emitSeqBodyConstants(RT.Seq(c))
		nc.WriteString(")")
	case PersistentSet:
		nc.WriteString("lang.NewPersistentSet(")
		nc.emitSeqBodyConstants(RT.Seq(c))
		nc.WriteString(")")
	case Seq:
		nc.WriteString("lang.NewPersistentList(")
		nc.emitSeqBodyConstants(c)
		nc.WriteString(")")
	case string:
		nc.WriteString(fmt.Sprintf("%#v", c))
	case reflect.Type:
		nc.emitType(c)
	case IFn:
		nc.emitFn(c)
	case NativePrinter:
		nc.WriteString(c.PrintNative())
	default:
		v := reflect.ValueOf(c)
		switch {
		case v.Kind() <= reflect.Float64:
			nc.WriteString(fmt.Sprintf("%v(%v)", v.Type(), v.Interface()))
		default:
			panic(fmt.Errorf("Can't emit constant: %v (%T)\n", c, c))
		}
	}
}

func (nc *NativeCompiler) emitMapPairConstants(m PersistentMap) {
	for kvs := m.Seq(); kvs != nil; kvs = kvs.Next() {
		nc.emitSeqBodyConstants(RT.Seq(kvs.First()))
	}
}

func (nc *NativeCompiler) emitSeqBodyConstants(s Seq) {
	for s = RT.Seq(s); s != nil; s = s.Next() {
		nc.emitConstant(s.First())
		nc.WriteString(", ")
	}
}

func (nc *NativeCompiler) emitType(t reflect.Type) {
	switch {
	case t == nil:
		panic(fmt.Errorf("Can't emit nil type"))
	case t.Kind() == reflect.Interface:
		nc.WriteString("reflect.TypeOf((*" + t.String() + ")(nil)).Elem()")
	case t.Kind() == reflect.Ptr:
		nc.WriteString("reflect.TypeOf((" + t.String() + ")(nil))")
	case t.Kind() == reflect.Struct:
		nc.WriteString("reflect.TypeOf(" + t.String() + "{})")
	case t.Kind() == reflect.String:
		nc.WriteString("reflect.TypeOf(\"\")")
	case t.Kind() == reflect.Bool:
		nc.WriteString("reflect.TypeOf(false)")
	case t.Kind() <= reflect.Float64:
		nc.WriteString("reflect.TypeOf(" + t.String() + "(0))")
	default:
		panic(fmt.Errorf("Can't emit type: %v", t))
	}
}

func (nc *NativeCompiler) registerVar(v *Var) string {
	panic("no registerVar yet")
}

type FnCompiler struct {
	nc        *NativeCompiler
	enclFn    *FnCompiler
	name      *Symbol
	goname    string
	restArity int
	methods   [MAX_ARITY + 1]fcmethod
	closes    []interface{}
	meta      PersistentMap
}

type fcmethod struct {
	scope *fcscope
}

type fcscope struct {
	parent  *fcscope
	baseIdx int
	locals  []fclocal
	code    []interface{}
}

type fclocal struct {
	name    *Symbol
	_goname string
}

func (l fclocal) goname() string {
	if l._goname == "" {
		if l.name == nil {
			l._goname = "_"
		} else {
			l._goname = fmt.Sprintf("%s__%d", Munge(l.name.String()), RT.NextID())
		}
	}
	return l._goname
}

func (nc *NativeCompiler) emitFn(fn IFn) {
	switch fn := fn.(type) {
	case *Fn:
		fc := FnCompiler{nc: nc}
		fc.emit(fn)
	}
}

func (fc *FnCompiler) emit(fn *Fn) {
	fc.name = fn.name
	if fc.name == nil {
		fc.goname = fmt.Sprintf("%s__anonfn__%d", fc.nc.packagename, RT.NextID())
	} else {
		fc.goname = Munge(fc.name.String())
	}
	fc.restArity = fn.restArity
	for i, m := range fn.bodies {
		if m != nil {
			fcm := fc.processMethod(i, m)
			methodname := fmt.Sprintf("%s__method%d", fc.goname, i)
			fc.nc.WriteString(fmt.Sprintf("var %s *lang.Func%d\n", methodname, i))
			fc.nc.WriteString(methodname + " = ")
			fc.emitMethod(i, fcm)
		}
	}
}

func (fc *FnCompiler) emitMethod(arity int, m fcmethod) {
	fc.nc.WriteString("func(__THE_VM__ *lang.VM,")
	for i := 0; i < arity; i++ {
		fc.nc.WriteString(m.scope.locals[i].goname())
		fc.nc.WriteString(" interface{},")
	}
	fc.nc.WriteString(") interface{} {\n")
	for _, bc := range m.scope.code {
		fc.emitBytecode(bc, m.scope)
	}
	fc.nc.WriteString("}\n")
}

func (fc *FnCompiler) emitBytecode(bc interface{}, scope *fcscope) {
	switch bc := bc.(type) {
	case *bc_DerefVar:
		varname := fc.nc.registerVar(bc.v)
		fc.nc.WriteString(varname)
		fc.nc.WriteString(".Deref(__THE_VM__)")
	case *bc_Def:
		panic("can't compile def in native fn")
	case *bc_Param:
		fc.nc.WriteString(scope.locals[bc.index].goname())
	case *bc_ClosedOver:

	}
}

func (fc *FnCompiler) processMethod(arity int, body []interface{}) (m fcmethod) {
	m.scope = &fcscope{}
	m.scope.process(body)
	return m
}

func (scope *fcscope) process(code []interface{}) {
	for bcIx := 0; bcIx < len(code); bcIx++ {
		bc := code[bcIx]
		switch bc := bc.(type) {
		case *bc_Param:
			for i := len(scope.locals); i <= bc.index; i++ {
				scope.locals = append(scope.locals, fclocal{})
			}
			scope.locals[bc.index].name = bc.name
		case *bc_PushLocal:
			i := bcIx + 1
			pushes := 1
		pushloop:
			for ; i < len(code); i++ {
				switch bc0 := code[i].(type) {
				case *bc_PushLocal:
					pushes++
				case *bc_PopLocals:
					pushes -= bc0.count
					if pushes == 0 {
						break pushloop
					}
				}
			}
			subscope := fcscope{scope, len(scope.locals), scope.locals, nil}
			subscope.process(code[bcIx:i])
			bcIx = i - 1
			scope.code = append(scope.code, subscope)
			continue
		}
		scope.code = append(scope.code, bc)
	}
}

func Munge(s string) string {
	var sb strings.Builder
	letterIx := 0
	for i, r := range s {
		if unicode.IsLetter(r) || unicode.IsNumber(r) || r == '_' {
			continue
		}
		sb.WriteString(s[letterIx:i])
		letterIx = i + utf8.RuneLen(r)
		switch r {
		case '~':
			sb.WriteString("_TILDE_")
		case '`':
			sb.WriteString("_BTICK_")
		case '!':
			sb.WriteString("_BANG_")
		case '@':
			sb.WriteString("_AT_")
		case '#':
			sb.WriteString("_HASH_")
		case '$':
			sb.WriteString("_DOLLAR_")
		case '%':
			sb.WriteString("_PCT_")
		case '^':
			sb.WriteString("_CARET_")
		case '&':
			sb.WriteString("_AMP_")
		case '*':
			sb.WriteString("_STAR_")
		case '-':
			sb.WriteString("_MINUS_")
		case '+':
			sb.WriteString("_PLUS_")
		case '=':
			sb.WriteString("_EQUALS_")
		case ':':
			sb.WriteString("_COLON_")
		case '<':
			sb.WriteString("_LT_")
		case '>':
			sb.WriteString("_GT_")
		case '?':
			sb.WriteString("_QMARK_")
		case '/':
			sb.WriteString("_SLASH_")
		case '|':
			sb.WriteString("_PIPE_")
		case '\\':
			sb.WriteString("_BSLASH_")
		case '"':
			sb.WriteString("_DQUOTE_")
		case '\'':
			sb.WriteString("_SQUOTE_")
		case '.':
			sb.WriteString("_DOT_")
		default:
			sb.WriteString(fmt.Sprintf("_U%04X_", r))
		}
	}
	if letterIx == 0 {
		return s
	}
	sb.WriteString(s[letterIx:len(s)])
	return sb.String()
}
