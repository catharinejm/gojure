package lang

import (
	"errors"
	"fmt"
	"sync"
)

const BASE_STACK_SIZE = 32

var (
	stackpool = sync.Pool{
		New: func() interface{} {
			// atomic.AddInt64(&stackGetMisses, 1)
			return make([]interface{}, 0, BASE_STACK_SIZE)
		},
	}

	stackPutHits   int64
	stackPutMisses int64

	stackGets      int64
	stackGetMisses int64
)

func makeStack() stack {
	// atomic.AddInt64(&stackGets, 1)
	root := stackpool.Get().([]interface{})
	return stack{root, 0}
}

type stack struct {
	root []interface{}
	zidx int
}

func (s stack) empty() bool {
	return len(s.root) == s.zidx
}

func (s stack) len() int {
	return len(s.root) - s.zidx
}

func (s *stack) push(el interface{}) {
	s.root = append(s.root, el)
}

func (s *stack) pushN(els []interface{}) {
	s.root = append(s.root, els...)
}

func (s *stack) pop() (el interface{}) {
	if s.empty() {
		panic(errors.New("pop: stack underflow"))
	}
	el = s.root[len(s.root)-1]
	s.root[len(s.root)-1] = nil
	s.root = s.root[:len(s.root)-1]
	return
}

func (s *stack) popN(count int) {
	switch {
	case count == 0:
		return
	case s.len() < count:
		l, c := s.len(), count
		panic(fmt.Errorf("popN: stack underflow -- len = %d, pop = %d", l, c))
	}
	for i := len(s.root) - count; i < len(s.root); i++ {
		s.root[i] = nil
	}
	s.root = s.root[:len(s.root)-count]
}

func (s stack) peek() interface{} {
	if s.empty() {
		panic(errors.New("peek: stack underflow"))
	}
	return s.root[len(s.root)-1]
}

func (s stack) peekN(count int) []interface{} {
	if s.len() < count {
		l, c := s.len(), count
		panic(fmt.Errorf("peekN: stack underflow -- len = %d, peek = %d", l, c))
	}
	return s.root[len(s.root)-count:]
}

func (s stack) view() []interface{} {
	return s.root[s.zidx:]
}

func (s stack) at(i int) interface{} {
	return s.root[s.zidx+i]
}

func (s stack) newFrame(paramCount int) stack {
	if s.len() < paramCount {
		l, c := s.len(), paramCount
		panic(fmt.Errorf("newFrame: requested frame: %d is larger than the stack: %d", c, l))
	}
	return stack{s.root, len(s.root) - paramCount}
}

func (s *stack) reset(base int, els []interface{}) {
	base += s.zidx
	newend := base + len(els)
	if newend > len(s.root) {
		panic(errors.New("reset: too many new elements for frame"))
	}
	copy(s.root[base:], els)
	for i := newend; i < len(s.root); i++ {
		s.root[i] = nil
	}
	s.root = s.root[:newend]
}

func (s *stack) popFrame(child stack) {
	s.root = child.root
	s.popN(len(s.root) - child.zidx)
}

func (s *stack) release() {
	for i := range s.root {
		s.root[i] = nil
	}
	s.root = s.root[:0]
	// TODO: should this threshhold be dynamic? or at least configurable?
	if cap(s.root) <= 8*BASE_STACK_SIZE {
		// atomic.AddInt64(&stackPutHits, 1)
		stackpool.Put(s.root)
	} else {
		// atomic.AddInt64(&stackPutMisses, 1)
	}
	s.root = nil
}

func StackStats() PersistentMap {
	return NewPersistentMap(
		"stackPutHits", stackPutHits,
		"stackPutMisses", stackPutMisses,
		"stackGets", stackGets,
		"stackGetMisses", stackGetMisses,
	)
}
