package lang

import (
	"errors"
	"fmt"
	"sync/atomic"
)

type Var struct {
	ns   *Namespace
	sym  *Symbol
	root atomicBox

	dynamic int32
	resettableMetadata
}

type unboundVar struct {
	v *Var
}

func (uv unboundVar) String() string {
	return fmt.Sprintf("#unboundVar[%v]", uv.v)
}

func newVar(ns *Namespace, sym *Symbol) *Var {
	v := &Var{ns, sym, atomicBox{}, 0, resettableMetadata{}}
	v.root.Store(unboundVar{v})
	return v
}

func DetachedVarWithRoot(name string, root interface{}) *Var {
	v := anonymousVarWithRoot(root)
	v.sym = ParseSymbol(name)
	return v
}

func anonymousVarWithRoot(root interface{}) *Var {
	v := newVar(nil, nil)
	v.BindRoot(root)
	return v
}

func InternVar(ns *Namespace, sym *Symbol, root interface{}) *Var {
	v := ns.Intern(sym)
	v.BindRoot(root)
	return v
}

func FindVar(ns *Namespace, sym *Symbol) *Var {
	if sym.Namespace() != "" {
		ns = FindNamespace(sym.NamespaceSym())
	}
	if ns == nil {
		return nil
	}
	return ns.FindInternedVar(sym.NameSym())
}

func FindVarOrFail(ns *Namespace, sym *Symbol) *Var {
	thevar := FindVar(ns, sym)
	if thevar == nil {
		varNS := sym.ns
		if varNS == "" {
			varNS = ns.name.String()
		}
		panic(fmt.Errorf("undefined var: #'%s/%s", varNS, sym.name))
	}
	return thevar
}

func (v *Var) Deref(vm *VM) interface{} {
	if v.IsDynamic() && vm != nil {
		if tbox := vm.getThreadBinding(v); tbox != nil {
			return tbox.Load()
		}
	}
	return v.root.Load()
}

func (v *Var) Set(vm *VM, value interface{}) interface{} {
	if vm != nil {
		if tbox := vm.getThreadBinding(v); tbox != nil {
			if vm.tcid != tbox.tcid {
				panic(fmt.Errorf("Can't set!: %v from non-binding thread", v))
			}
			tbox.Store(value)
			return value
		}
	}
	panic(fmt.Errorf("Can't change/establish root binding of: %v with set!", v))
}

func (v *Var) SetDynamic() *Var {
	v.setDynamic(1)
	return v
}

func (v *Var) SetDynamicTo(b bool) *Var {
	var d int32
	if b {
		d = 1
	}
	v.setDynamic(d)
	return v
}

func (v *Var) IsDynamic() bool {
	return atomic.LoadInt32(&v.dynamic) != 0
}

func (v *Var) SetMacro() *Var {
	v.ResetMeta(RT.AssocM(v.Meta(), kMACRO, true))
	return v
}

func (v *Var) ResetMeta(newMeta PersistentMap) PersistentMap {
	v.resettableMetadata.mu.Lock()
	defer v.resettableMetadata.mu.Unlock()
	oldtag := RT.ValAt(v.meta, kTAG, nil)
	newtag := RT.ValAt(v.meta, kTAG, nil)
	if oldtag != nil && newtag != nil && !RT.Equiv(oldtag, newtag) {
		panic(errors.New("a var's tag can't be changed once set"))
	}
	if oldtag != nil && newtag == nil {
		newMeta = newMeta.AssocM(kTAG, oldtag)
	}
	v.meta = newMeta
	return v.meta
}

func (v *Var) IsMacro() bool {
	return RT.BoolCast(RT.ValAt(v.Meta(), kMACRO, nil))
}

func (v *Var) IsPublic() bool {
	return !RT.BoolCast(RT.ValAt(v.Meta(), kPRIVATE, false))
}

func (v *Var) setDynamic(d int32) {
	atomic.StoreInt32(&v.dynamic, d)
}

func (v *Var) BindRoot(newroot interface{}) {
	v.root.Store(newroot)
}

func (v *Var) HasRoot() bool {
	_, isunbound := v.root.Load().(unboundVar)
	return !isunbound
}

func (v *Var) IsBound(vm *VM) bool {
	if v.HasRoot() {
		return true
	}
	tbox := vm.getThreadBinding(v)
	return tbox != nil
}

func (v *Var) AlterRoot(vm *VM, fn IFn, args Seq) interface{} {
	newroot := fn.Invoke(vm, args)
	v.root.Store(newroot)
	return newroot
}

func (v *Var) GetThreadBinding(vm *VM) *tbox {
	return vm.getThreadBinding(v)
}

func (v *Var) Symbol() *Symbol {
	if v.ns == nil {
		return v.sym
	}
	return NewSymbol(v.ns.name.String(), v.sym.String())
}

func (v *Var) BoundNS() *Namespace {
	return v.ns
}

func (v *Var) String() string {
	if v.ns != nil {
		return fmt.Sprintf("#'%s/%s", v.ns.name, v.sym.name)
	}
	name := "--unnamed--"
	if v.sym != nil {
		name = v.sym.name
	}
	return fmt.Sprintf("#<Var: %s>", name)
}
