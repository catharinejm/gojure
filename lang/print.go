package lang

type NativePrinter interface {
	PrintNative() string
}
