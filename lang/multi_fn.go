package lang

import (
	"fmt"
	"sync"
)

type MultiFn struct {
	rw                 sync.RWMutex
	name               string
	dispatchFn         IFn
	defaultDispatchVal interface{}
	methodTable        PersistentMap
	methodCache        PersistentMap
	preferTable        PersistentMap
	hierarchy          IRef
	cachedHierarchy    interface{}
}

func NewMultiFn(name string, dispatchFn IFn, defaultDispatchVal interface{}, hierarchy IRef) *MultiFn {
	return &MultiFn{
		name:               name,
		dispatchFn:         dispatchFn,
		defaultDispatchVal: defaultDispatchVal,
		methodTable:        EMPTY_MAP,
		methodCache:        EMPTY_MAP,
		preferTable:        EMPTY_MAP,
		hierarchy:          hierarchy,
	}
}

func (mf *MultiFn) Reset() *MultiFn {
	mf.rw.Lock()
	defer mf.rw.Unlock()
	mf.methodTable = EMPTY_MAP
	mf.methodCache = EMPTY_MAP
	mf.preferTable = EMPTY_MAP
	return mf
}

func (mf *MultiFn) AddMethod(dispatchVal interface{}, method IFn) *MultiFn {
	mf.rw.Lock()
	defer mf.rw.Unlock()
	mf.methodTable = mf.methodTable.AssocM(dispatchVal, method)
	mf.resetCache()
	return mf
}

func (mf *MultiFn) RemoveMethod(dispatchVal interface{}) *MultiFn {
	mf.rw.Lock()
	defer mf.rw.Unlock()
	mf.methodTable = mf.methodTable.Dissoc(dispatchVal)
	mf.resetCache()
	return mf
}

func (mf *MultiFn) PreferMethod(dispatchValX, dispatchValY interface{}) *MultiFn {
	mf.rw.Lock()
	defer mf.rw.Unlock()
	if mf.prefers(dispatchValX, dispatchValY) {
		panic(fmt.Errorf("Preference conflict in multimethod '%s': %s is already preferred to %s",
			mf.name, dispatchValX, dispatchValY))
	}
	mf.preferTable = mf.preferTable.AssocM(
		dispatchValX,
		RT.Conj(
			RT.ValAt(mf.preferTable, dispatchValX, EMPTY_SET).(PersistentCollection),
			dispatchValY))
	mf.resetCache()
	return mf
}

func (mf *MultiFn) prefers(x, y interface{}) bool {
	xprefs := mf.preferTable.ValAt(x, EMPTY_SET).(PersistentSet)
	if xprefs.Contains(y) {
		return true
	}
	// TODO: support parents
	return false
}

func (mf *MultiFn) resetCache() {}
