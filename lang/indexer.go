package lang

type Indexer interface {
	Nth(i int, notFound interface{}) interface{}
}
