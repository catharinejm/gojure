package lang

import (
	"reflect"
	"sync"
)

var (
	typecache   sync.Map
	methodcache atomicBox
)

type CachedType struct {
	typ     *Type
	fields  PersistentMap
	methods atomicBox
}

func CacheType(typ *Type, fields, methods PersistentMap) *CachedType {
	if ct, exists := typecache.Load(typ.Type); exists {
		return ct.(*CachedType)
	}
	if methods == nil {
		methods = EMPTY_MAP
	}
	ct, _ := typecache.LoadOrStore(typ.Type, &CachedType{typ, fields, makeAtomicBox(methods)})
	cacheMethods(ct.(*CachedType))
	return ct.(*CachedType)
}

func LookupType(typ reflect.Type) *CachedType {
	typ = unwrapType(typ)
	iface, _ := typecache.Load(typ)
	cached, _ := iface.(*CachedType)
	return cached
}

func (ct *CachedType) Type() *Type {
	return ct.typ
}

func (ct *CachedType) Fields() PersistentMap {
	return ct.fields
}

func (ct *CachedType) Methods() PersistentMap {
	return ct.methods.Load().(PersistentMap)
}

func cacheMethods(cached *CachedType) {
	for ms := RT.Seq(cached.Methods()); ms != nil; ms = ms.Next() {
		methodEntry := ms.First().(MapEntry)
		methodcache.Swap(func(mc interface{}) interface{} {
			impls := RT.ValAt(mc, methodEntry.Key(), EMPTY_MAP).(PersistentMap)
			for iseq := impls.Seq(); iseq != nil; iseq = iseq.Next() {
				implEntry := iseq.First().(MapEntry)
				implType := implEntry.Key().(*Type)
				if implType.AssignableTo(cached.typ.Type) {
					impls = impls.Dissoc(implType)
				} else if cached.typ.AssignableTo(implType.Type) {
					return RT.Assoc(mc, methodEntry.Key(), impls)
				}
			}
			return RT.Assoc(mc, methodEntry.Key(), impls.Assoc(cached.typ, methodEntry.Value()))
		})
	}
}

func LookupMethod(name *Keyword) PersistentMap {
	return RT.ValAt(methodcache.Load(), name, EMPTY_MAP).(PersistentMap)
}
