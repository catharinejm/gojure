package lang

import (
	"fmt"
	"sync/atomic"
)

var threadContextID uint64 = 0

// nextThreadContextID returns monotonically increasing uint64s for identifying
// "threads" within a gojure program, starting at 1.
func nextThreadContextID() uint64 {
	return atomic.AddUint64(&threadContextID, 1)
}

type tbox struct {
	tcid uint64
	atomicBox
}

func newTbox(tcid uint64, value interface{}) *tbox {
	tb := &tbox{tcid, atomicBox{}}
	tb.Store(value)
	return tb
}

func (t *tbox) String() string {
	return fmt.Sprintf("#tbox[%v]", t.Load())
}
