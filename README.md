## Gojure

Gojure is a dialect of [Clojure](https://clojure.org/) implemented in Go. When
it is completed, it will be a spiritual port of Clojure, retaining Java
Clojure's semantics as closely as is reasonable. It supports host interop,
concurrency via goroutines, and go's channels.

Gojure is early in development, and while it does work, it may change
considerably. Not all Clojure features are yet supported.

### Implementation Notes

Gojure compiles clojure source forms to a series of instructions (mis-dubbed
"bytecodes"), which are executed via a simple stack machine.

Threads of execution are tracked via a VM object which contains thread-local
dynamic var bindings and a unique id. The current VM is passed implicitly to any
native function, called from Gojure, which accepts `*VM` as its first
parameter. Any Go code which interacts with Gojure must take care not to pass a
VM reference to across goroutine boundaries. Using the `(go* ...)` special form
automatically creates a new VM for the new goroutine and shuttles the dynamic
var bindings into that new context. `nil` is a valid `*VM` value, but it will
not have access to any dynamic bindings. Fresh bindings can still be made,
however.

Interpreted gojure fns contain their bytecode. Invoking them implicitly launches
an evaluator. Execution will remain within a single evaluator until a native
host call occurs. After the native call returns, execution on the evaluator
continues. If native code calls into another interpreted function it will run on
a new evaluator, but it should still have the same thread-context, provided the
native code is well-behaved.

Because Go does not (portably) support runtime code loading, all Go packages
that are referenced from Gojure code must be pre-processed by the
[importer](./importer) tool. The tool scans all public names in the specified
packages and generates a package with `init()` functions which bind each name
into a `GoPackage` object.

Since Go does not have classes, native names are accessed via regular namespaced
symbols, where the package name is the namespace. E.g.

```
(ns my-ns)
(import fmt "fmt")
(import strings "strings")

(let [sb (new strings/Builder)]
  (.WriteString sb "Hello,")
  (.WriteRune sb \space)
  (.WriteString sb "World!")
  (fmt/Println (.String sb)))

;;=> "Hello, World!"
;;=> [14 nil]
```

As an optimization, functions and methods in processed packages are wrapped in
`DirectFn` objects, which accept the same number of parameters as the wrapped
func, each typed `interface{}` (or `...interface{}` for a variadic argument). It
then attempts to cast each `interface{}` parameter to the expected parameter
type for the enclosed function and invokes it. All functions in Gojure return a
single `interface{}` value. Void Go functions will return `nil` and multiple
return values are wrapped in a vector.

When a receiver's type in a method call is unknown (and not hinted) at Gojure
compile time, or no cached DirectFn exists (e.g. the type is private), it will
fall back to some form of reflection. At eval time, the evaler again attempts to
look up a `DirectFn` for the given type/method combination. If a value's type is
not cached, but it implements a public interface which contains the requested
method, the `DirectFn` for the interface will be used, and that method will be
cached under the private type for future use. If no `DirectFn` can be found, the
evaler falls back to standard Go reflection.

Values passed into Gojure must follow these rules. They are automatically
handled in host calls from within Gojure.

- All int types are wrapped in Gojure's `Int`, which is a wrapper around `int64`.
- All float types are wrapped in Gojure's `Float`, which is a wrapper around
`float64`.
- All rune types are wrapped in Gojure's `Rune`, which is a wrapper around
`rune`. Note that while `rune` is an alias for `int32` in Go, in Gojure they are
distinct types. Any call to a go function which uses the literal `rune` type in
its signature will assume the input argument is `Rune`, or will return a `Rune`
in its result.
- All struct or array value types must be passed as pointers.
- Everything else is passed as is, (interfaces, strings, bools, channels,
slices, maps).

Note that while the fallback Go reflection is more lenient in enforcing these
rules, they are strictly followed for the optimized `DirectFn` objects.

As an additional optimization, `Type` objects are generated for each public type
in a preprocessed Go package. Each `Type` wraps a `reflect.Type` object, and has
an `Accepts(interface{}) bool` method as well as `SliceOf(xs interface{})` and
`ValueSliceOf(xs interface{})`. `Accepts` is roughly equivalent to
`(reflect.Type).AssignableTo`, but it is always false for `nil` and it assumes
the above casting rules have been followed. `SliceOf` takes a seqable argument
and returns a slice of the `Type`'s type containing that collection's
elements. All elements in the collection must be assignable to the given
type. `ValueSliceOf` is like `SliceOf`, but in the case of structs and arrays
(which are always pointer types in Gojure) it returns a slice of the pointed-to
values. For all other kinds it returns the same value as `SliceOf`.

### License

```
Copyright (c) Catharine Manning. All rights reserved.
The use and distribution terms for this software are covered by the
Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
which can be found in the LICENSE file.
```

Gojure contains Clojure code from the original `clojure.core` namespace. Clojure
is licensed as follows:

```
Copyright (c) Rich Hickey. All rights reserved.
The use and distribution terms for this software are covered by the
Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
which can be found in the file epl-v10.html at the root of this distribution.
By using this software in any fashion, you are agreeing to be bound by
the terms of this license.
You must not remove this notice, or any other, from this software.
```
